#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <FS.h>
#include <Hash.h>
#include <ArduinoJson.h>
#include <ESP8266mDNS.h>
#include <ESPAsyncWiFiManager.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>

WiFiClient wifiClient;



const char* ssid = "sample";
const char* pass = "sample2"; 
const char* http_username = "admin";
const char* http_password = "admin";

// declaring the storing variables 

int a_stat;
int s_stat;
String output;
String auto_mode;
String setpoints ;
String units;
String sp_rt;
String debug;
String roaster;
String manual;
AsyncWebServer server(80);
DNSServer dns;
static float a;
void Send_db(){
    
  if (WiFi.status() == WL_CONNECTED) { //Check WiFi connection status
    HTTPClient http;    //Declare object of class HTTPClient
 
    http.begin(wifiClient , "http://192.168.0.173:5000/postjson");      //Specify request destination
    http.addHeader("Content-Type", "application/json");  //Specify content-type header
 
    int httpCode = http.POST(output);   //Send the request
    String payload = http.getString();                                        //Get the response payload
 
//    Serial.println(httpCode);   //Print HTTP return code
//    Serial.println(payload);    //Print request response payload
// 
    http.end();  //Close connection
 
  } 
  
  
}



String Mode_status()
{
  String m;
  if(a_stat == 1 && s_stat == 0)
    m = "auto";
  
  else if(a_stat == 0 && s_stat == 1)
    m = "shed";
  
  else if(a_stat == 0 && s_stat == 0)
    m = "no dose"; 
  return m;
}
// ------------------- End of the Reading Functions -----------------

void setup() 
{
  Serial.begin(115200);
  delay(100);
  
// Initialize the file system(SPIFFS)
  if(!SPIFFS.begin())
  {
    Serial.println("An Error has Occured while mounting  File system");
    return ; 
   }
  AsyncWiFiManager wifiManager(&server,&dns);
  //-----------------------------------connecting to wifi ------------------
//  Serial.println("connecting to ");
//  Serial.println(ssid);
//
//  // begin the wifi connection
//  WiFi.begin(ssid,pass);
//  
//
//  // checking the connection for the wifi connection with 
//  while(WiFi.status()!= WL_CONNECTED)
//  {
//    delay(500);
//    Serial.print(".");  
//  }
//  if (!MDNS.begin("esp8266")) {
//    Serial.println("Error setting up MDNS responder!");
//    while (1) {
//      delay(1000);
//    }
//  }
//  Serial.println("mDNS responder started");
//  Serial.println("Connected to ");
//  Serial.println(ssid);
//  Serial.print("Ip Address is ::");
//  Serial.println(WiFi.localIP());
// 
  wifiManager.autoConnect("Doser Controller");
//  wifiManager.startConfigPortalModeless("Doser Controller", "12345678");
  Serial.println("connected...yeey :)");
  Serial.print("Ip Address is ::");
  Serial.println(WiFi.localIP());
  StaticJsonDocument<200> network;
  network["menu"] = "ipaddr";
  network["ssid"] = WiFi.SSID();
  network["ip"] = WiFi.localIP().toString();
  serializeJson(network, Serial);
  

//  -------------------------------------### Requesting WEb PAGES ###--------------------------

 
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    if(!request->authenticate(http_username, http_password))
      return request->requestAuthentication();
    request->send(SPIFFS, "/dash.html","text/html");
  });
  server.on("/sets", HTTP_GET, [](AsyncWebServerRequest *request){
    if(!request->authenticate(http_username, http_password))
      return request->requestAuthentication();
    request->send(SPIFFS, "/SETTINGS.html","text/html");
  });
  server.on("/config", HTTP_GET, [](AsyncWebServerRequest *request){
    if(!request->authenticate(http_username, http_password))
      return request->requestAuthentication();
    request->send(SPIFFS, "/configurations.html","text/html");
  });
  server.on("/about", HTTP_GET, [](AsyncWebServerRequest *request){
    if(!request->authenticate(http_username, http_password))
      return request->requestAuthentication();
    request->send(SPIFFS, "/about.html","text/html");
  });

  // ----------------------------------### End of requesting Pages ###------------------------

  // ----------------------------------### REQUESTING FOR THE STATUS ###-----------------------
 server.on("/curr_stats", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200,"text/plain",output.c_str());
  });
  server.on("/dosing_stats", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200,"text/plain",Mode_status().c_str());
  });
  server.on("/logout", HTTP_GET, [](AsyncWebServerRequest *request){
  request->send(401);
  });
  server.on("/ref_auto", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200,"text/plain",auto_mode.c_str());
  });
  server.on("/ref_setpo", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200,"text/plain",setpoints.c_str());
  });
  server.on("/ref_units", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200,"text/plain",units.c_str());
  });
  server.on("/ref_sp_rt", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200,"text/plain",sp_rt.c_str());
  });
  server.on("/ref_debug", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200,"text/plain",debug.c_str());
  });
  server.on("/ref_roaster", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200,"text/plain",roaster.c_str());
  });
  server.on("/ref_manual", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200,"text/plain",manual.c_str());
  });
  //------------------------------------### End requesting for the status ###-----------------


// ----------------------------------------------### RECEVING THE DATA FROM THE WEB BROWSER (POST METHODS) from THE (mode page) ###---------------------
  server.on(
    "/post_shed",
    HTTP_POST,
    [](AsyncWebServerRequest * request){
        },
    NULL,
    [](AsyncWebServerRequest * request, uint8_t *data, size_t len, size_t index, size_t total) {
      
      for (size_t i = 0; i < len; i++) {
        Serial.write(data[i]);
      }
      request->send(200,"text/plain","Saved Parameters !");
      
  });

// server side for the Auto mode ----------------------
  server.on(
    "/post_auto",
    HTTP_POST,
    [](AsyncWebServerRequest * request){
        },
    NULL,
    [](AsyncWebServerRequest * request, uint8_t *data, size_t len, size_t index, size_t total) {

      for (size_t i = 0; i < len; i++) {
        Serial.write(data[i]);
      }
      request->send(200,"text/plain","Saved Parameters !");
  });
// Dosing for the Auto and Schedule Modes  ----------------------
  server.on(
    "/auto_dosing",
    HTTP_POST,
    [](AsyncWebServerRequest * request){
        },
    NULL,
    [](AsyncWebServerRequest * request, uint8_t *data, size_t len, size_t index, size_t total) {

      for (size_t i = 0; i < len; i++) {
        Serial.write(data[i]);
      }
      request->send(200,"text/plain",Mode_status().c_str());
  });

// Dosing for the Auto and Schedule Modes  ----------------------
  server.on(
    "/shed_dosing",
    HTTP_POST,
    [](AsyncWebServerRequest * request){
        },
    NULL,
    [](AsyncWebServerRequest * request, uint8_t *data, size_t len, size_t index, size_t total) {

      for (size_t i = 0; i < len; i++) {
        Serial.write(data[i]);
      }
      request->send(200,"text/plain",Mode_status().c_str());
  });
//------------------------------------------### END OF THE RECEIVING THE DATA FROM (MODE PAGE) ###-------------------

// ..........................................### RECEVING THE DATA FROM THE (SETTINGS PAGE) ###................. 
// receiving the setting points
  server.on(
      "/post_setpoints",
      HTTP_POST,
      [](AsyncWebServerRequest * request){
          },
      NULL,
      [](AsyncWebServerRequest * request, uint8_t *data, size_t len, size_t index, size_t total) {
       
        for (size_t i = 0; i < len; i++) {
          Serial.write(data[i]);
          
        }
        request->send(200,"text/plain","Saved Parameters !");
    });

// RECEIVING THE data from the UNITS PAGE
  server.on(
      "/post_units",
      HTTP_POST,
      [](AsyncWebServerRequest * request){
          },
      NULL,
      [](AsyncWebServerRequest * request, uint8_t *data, size_t len, size_t index, size_t total) {
       
        for (size_t i = 0; i < len; i++) {
          Serial.write(data[i]);
        }
        request->send(200,"text/plain","Saved Parameters !");
    });


// RECEIVING THE DATA FROM THE DATE TIME
  server.on(
      "/post_date_time",
      HTTP_POST,
      [](AsyncWebServerRequest * request){
          },
      NULL,
      [](AsyncWebServerRequest * request, uint8_t *data, size_t len, size_t index, size_t total) {
       
        for (size_t i = 0; i < len; i++) {
          Serial.write(data[i]);
           
        }
        request->send(200,"text/plain","Saved Parameters !");
        
    });


// RECEIVIING THE json DATA FROM THE speed&ratio MENU
 server.on(
      "/post_speed_ratio",
      HTTP_POST,
      [](AsyncWebServerRequest * request){
          },
      NULL,
      [](AsyncWebServerRequest * request, uint8_t *data, size_t len, size_t index, size_t total) {
        
        for (size_t i = 0; i < len; i++) {
          Serial.write(data[i]);
         
        }
        request->send(200,"text/plain","Saved Parameters !");
 
    });

// RECEIVING THE DATA FROM THE DEBUG MENU
  server.on(
      "/post_debug",
      HTTP_POST,
      [](AsyncWebServerRequest * request){
          },
      NULL,
      [](AsyncWebServerRequest * request, uint8_t *data, size_t len, size_t index, size_t total) {
        for (size_t i = 0; i < len; i++) {
          Serial.write(data[i]);
        }
        request->send(200,"text/plain","Saved Parameters !");
    });


// RECEIVING THE DATA FROM THE DEBUG MENU
  server.on(
      "/post_manual",
      HTTP_POST,
      [](AsyncWebServerRequest * request){
          },
      NULL,
      [](AsyncWebServerRequest * request, uint8_t *data, size_t len, size_t index, size_t total) {
        for (size_t i = 0; i < len; i++) {
          Serial.write(data[i]);
        }
        request->send(200,"text/plain","Saved Parameters !");
    });




    
  // Server starting .....
  server.begin();
  Serial.println("HTTP Server Started ....");
//  MDNS.addService("http", "tcp", 80);
}


// -------------------------------------------------------------### END OF RECEVING THE DATA FROM THE (SETTINGS PAGE) ###--------------------------------------------------


void loop() {

//  wifiManager.loop();
  if(Serial.available()>0)
  {
    StaticJsonDocument<300> rev;
    String sen_msg = Serial.readString();
    DeserializationError err = deserializeJson(rev, sen_msg);
    if(err == DeserializationError::Ok)
    {
      // receving the sensor data from the mega board
      if(rev["pack"] == "sensor")
      {
       a_stat = rev["Status"]["auto"];
       s_stat = rev["Status"]["shed"];
//       serializeJson(rev, output);
       output = sen_msg;
       Send_db();
       
      }
      else if(rev["pack"] == "auto_m")
      {
//        serializeJson(rev, auto_mode);
          auto_mode = sen_msg;
      }
      else if(rev["pack"] == "setp")
      {
//        serializeJson(rev , setpoints);  
          setpoints = sen_msg;
      }
      else if(rev["pack"] == "units")
      {
//        serializeJson(rev , units);
          units = sen_msg;
      }
      else if(rev["pack"] == "sp_rt")
      {
//        serializeJson(rev , sp_rt);
          sp_rt = sen_msg;
      }
      else if(rev["pack"] == "debug")
      {
//        serializeJson(rev , debug); 
          debug = sen_msg; 
      }
       else if(rev["pack"] == "roaster")
      {
//        serializeJson(rev , debug); 
          roaster = sen_msg; 
      }
      else if(rev["pack"] == "manual")
      {
//        serializeJson(rev , debug); 
          manual = sen_msg; 
      }
    }
   }
}
