//Master Node Main Code
#include <SPI.h>
#include <LoRa.h>
#include <Wire.h>
#include <RTClib.h>
#define TINY_GSM_MODEM_SIM800   
#include <TinyGsmClient.h>
#include "ThingsBoard.h"
#include "esp_system.h"
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x3F,20,4);  // set the LCD address to 0x27 for a 16 chars and 2 line display
//int init();
const char apn[]      = "airtelgprs.com"; 
const char gprsUser[] = ""; 
const char gprsPass[] = ""; 
const char simPIN[]   = ""; 

#define TOKEN               "PVl8xBIN077PNtgGNhus"
#define THINGSBOARD_SERVER  "demo.thingsboard.io"

// TTGO T-Call pins
#define MODEM_RST            5
#define MODEM_PWKEY          4
#define MODEM_POWER_ON       23
#define MODEM_TX             27
#define MODEM_RX             26

//define the pins used by the LoRa transceiver module
#define SCK 15
#define MISO 19
#define MOSI 32
#define SS 18
#define RST 14
#define DIO0 33

#define SerialAT Serial1
#ifdef DUMP_AT_COMMANDS
  #include <StreamDebugger.h>
  StreamDebugger debugger(serialGsm, Serial);
  TinyGsm modem(debugger);
#else
  // Initialize GSM modem
  TinyGsm modem(SerialAT);
#endif
bool modemConnected = false;

RTC_DS3231 rtc;
//char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

byte msgCount = 0;         
int interval = 1000;         
long lastSendTime = 0;    
const int ledChannel = 0;
int Light = 25;
String M_status = "0", M_bulb_status;
String current_amp, bulb_status, pole_status;
const int current_sensor = 34;
int currentValueM = 0;
int mVperAmp = 100; // use 100 for 20A Module and 66 for 30A Module
double Voltage = 0;
double VRMS = 0;

void setup() {
  Serial.begin(115200);
  SerialAT.begin(115200, SERIAL_8N1, MODEM_RX, MODEM_TX);
    lcd.init();
//    lcd.init();
  lcd.backlight();
  Wire.begin();
  pinMode(Light, OUTPUT);
  ledcAttachPin(Light, ledChannel);
  // Set modem reset, enable, power pins
  pinMode(MODEM_PWKEY, OUTPUT);
  pinMode(MODEM_RST, OUTPUT);
  pinMode(MODEM_POWER_ON, OUTPUT);
  digitalWrite(MODEM_PWKEY, LOW);
  digitalWrite(MODEM_RST, HIGH);
  digitalWrite(MODEM_POWER_ON, HIGH);

  // Restart SIM800 module, it takes quite some time
  Serial.println("Initializing modem...");
  modem.restart();
  String modemInfo = modem.getModemInfo();
  Serial.print(F("Modem: "));
  Serial.println(modemInfo);
  
  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    while (1);
  }

  rtc.adjust(DateTime(__DATE__, __TIME__));
  delay(1000);
  
  //SPI LoRa pins
  SPI.begin(SCK, MISO, MOSI, SS);
  LoRa.setPins(SS, RST, DIO0);
  if (!LoRa.begin(433E6)) {
    Serial.println("Starting LoRa failed!");
    esp_restart();
    while (1);
  }
  LoRa.setSyncWord(0xF3);    
  LoRa.setSpreadingFactor(8);
  Serial.println("LoRa Initializing OK!");
}

void loop() {
  Voltage = getVPP();
  VRMS = (Voltage/2.0) *0.707; 
  currentValueM = (VRMS * 1000)/mVperAmp;
  if(currentValueM <= 10){
    currentValueM = 0;
  }
  if (millis() - lastSendTime > interval) {
    sendMessage();
    lastSendTime = millis();           
    interval = random(2000) + 1000;  
  }
  onReceive(LoRa.parsePacket());
  if(M_status == "25" && M_status == "50" && M_status == "75" && M_status == "100" &&  currentValueM <= 0.10){
    M_bulb_status = "Bulb Fault";
  }
  else{
    M_bulb_status = "No Fault";
  }
  sendData();
}

float getVPP()
{
  float result;
  int readValue;             
  int maxValue = 0;          
  int minValue = 4096;       
  uint32_t start_time = millis();
  while((millis()-start_time) < 1000) //sample for 1 Sec
  {
      readValue = analogRead(current_sensor);
      if (readValue > maxValue) 
      {
        maxValue = readValue;
      }
   }
   if (readValue < minValue) 
   {
    minValue = readValue;
   }
   result = ((maxValue - minValue) * 5.0)/4096.0; 
   return result;
}

void sendMessage(){
  DateTime now = rtc.now();
  //Serial.print("Hour :" + now.hour());
  //Serial.println("Minute :" + now.minute());
  //25% Brightness Level
  if((now.hour()>=19 && now.hour()<=19) && (now.minute()>=49 && now.minute()<=50)){
    ledcWrite(ledChannel,256);
    M_status = "25";
    LoRa.beginPacket();
    LoRa.print(M_status);
    lcd.setCursor(3,0);
  lcd.print(M_status);
    LoRa.endPacket();
    //Serial.println("Street Light ON");
    //delay(500);
  }
  //50% Brightness Level
  else if((now.hour()>=19 && now.hour()<=19) && (now.minute()>=49 && now.minute()<=50)){
    ledcWrite(ledChannel,512);
    M_status = "50";
    LoRa.beginPacket();
    LoRa.print(M_status);
    LoRa.endPacket();
    //Serial.println("Street Light ON");
    //delay(500);
  }
  //75% Brightness Level
  else if ((now.hour()>=19 && now.hour()<=19) && (now.minute()>=50 && now.minute()<=51)){
    ledcWrite(ledChannel,768);
    M_status = "75";
    LoRa.beginPacket();
    LoRa.print(M_status);
    LoRa.endPacket();
    //Serial.println("Street Light OFF");
    //delay(500);
  }
  //100% Brightness Level
  else if ((now.hour()>=19 && now.hour()<=19) && (now.minute()>=50 && now.minute()<=51)){
    ledcWrite(ledChannel,1024);
    M_status = "100";
    LoRa.beginPacket();
    LoRa.print(M_status);
    LoRa.endPacket();
    //Serial.println("Street Light OFF");
    //delay(500);
  }
  else{
    ledcWrite(ledChannel,0);
    M_status = "Out of Time";
    //Send LoRa packet to receiver
    //LoRa.beginPacket();
    //LoRa.print(M_status);
    //LoRa.endPacket();
  }
}

void onReceive(int packetSize) {
  if (packetSize == 0) return;          // if there's no packet, return

  // read packet header bytes:
  String incoming = "";

  while (LoRa.available()) {
    incoming = LoRa.readStringUntil('\r');
    current_amp = getValue(incoming,',',0);
    pole_status = getValue(incoming,',',1);
    bulb_status = getValue(incoming,',',2);
  }
}
String getValue(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length()-1;

  for(int i=0; i<=maxIndex && found<=index; i++){
    if(data.charAt(i)==separator || i==maxIndex){
        found++;
        strIndex[0] = strIndex[1]+1;
        strIndex[1] = (i == maxIndex) ? i+1 : i;
    }
  }

  return found>index ? data.substring(strIndex[0], strIndex[1]) : "";
}

void sendData(){
  TinyGsmClient client(modem);
  ThingsBoard tb(client);
  if (!modemConnected) {
    Serial.print(F("Waiting for network..."));
    if (!modem.waitForNetwork()) {
        Serial.println(" fail");
        delay(3000);
        return;
    }
  Serial.println(" OK");
  
  Serial.print("Connecting to APN: ");
  Serial.print(apn);
  if (!modem.gprsConnect(apn, gprsUser, gprsPass)) {
    Serial.println(" fail");
    delay(2000);
    return;
  }
   modemConnected = true;
   Serial.println("..OK");
  }

  if (!tb.connected()) {
    // Connect to the ThingsBoard
    //Serial.print("Connecting to: ");
    //Serial.print(THINGSBOARD_SERVER);
    //Serial.print(" with token ");
    //Serial.println(TOKEN);
    if (!tb.connect(THINGSBOARD_SERVER, TOKEN)) {
      Serial.println("Failed to connect");
      return;
    }
  }
  Serial.println("Sending data...");
  tb.sendTelemetryFloat("currentValueM", currentValueM);
  tb.sendTelemetryString("M_status", M_status.c_str());
  tb.sendTelemetryString("M_bulb_status", M_bulb_status.c_str());
  tb.sendTelemetryString("current_amp", current_amp.c_str());
  tb.sendTelemetryString("bulb_status", bulb_status.c_str());
  tb.sendTelemetryString("pole_status", pole_status.c_str());
  tb.loop();
}
