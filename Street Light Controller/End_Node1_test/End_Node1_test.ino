#include <SPI.h>
#include <LoRa.h>
//#include <avr/wdt.h>

const int csPin = 10;          
const int resetPin = 9;       
const int irqPin = 2;      

int interval = 500;          
long lastSendTime = 0;        

const int acsPin = A5;
String N1_status = "N1_0", bulb_status ;
float Idig, Imax, Ipeak, Irms, Isum;
float Iarray[60];
float currentValue1 = 0;
int Light = 3;
String LoRaMessage = "";

void setup() {
  Serial.begin(115200);
  while (!Serial);
  pinMode(Light, OUTPUT);
  analogWrite(Light, 0);
  //wdt_enable(WDTO_4S);
  
  LoRa.setPins(csPin, resetPin, irqPin);
  if (!LoRa.begin(433E6)) {
    Serial.println("Starting LoRa failed!");
    //wdt_reset();
    while (1);
  }
  LoRa.setSyncWord(0xF3);           // ranges from 0-0xFF, default 0x34, see API docs
  LoRa.setSpreadingFactor(8);
  Serial.println("LoRa Initializing OK!");
}

void loop() {
  int i;
  for(i=0;i<60;i++){
    Idig = analogRead(acsPin);
    if(Idig>511){
      Ipeak = ((Idig-511)*5)/(0.100*1023); // Peak value of positive
      Iarray[i] = Ipeak;
    }
    else Iarray[i] = 0;
    delay(10);
  }
  Ipeak = findMax();
  currentValue1 = Ipeak*0.707;
  if(currentValue1 <= 0.16){
    currentValue1 = 0;
  }
  onReceive(LoRa.parsePacket());
  delay(200);
  if(N1_status == "N1_25" && N1_status == "N1_50" && N1_status == "N1_75" && N1_status == "N1_100" && currentValue1 <= 0.16){
    bulb_status = "Bulb Fault";
  }
  else{
    bulb_status = "No Fault";
  }
  if (millis() - lastSendTime > interval) {
    sendMessage();
    lastSendTime = millis();           
    interval = random(2000) + 1000; 
  }
 //delay(100);
}
float findMax(){
  Imax = Iarray[0];
  int j;
  for(j=0;j<=60;j++){
  if(Iarray[j] > Imax){ 
  Imax = Iarray[j];
}
}
return Imax;
}
void onReceive(int packetSize) {
  if (packetSize == 0) return;          // if there's no packet, return

  while (LoRa.available()) {
   String incoming = LoRa.readStringUntil('\r');
    Serial.println("Message: " + incoming);
    if (incoming == "25"){
        analogWrite(Light, 68);
        N1_status = "N1_25";
        sendMessage();
        //Serial.println("Light ON");
        //delay(500);
      }
      else if (incoming == "50"){
        analogWrite(Light, 127);
        N1_status = "N1_50";
        sendMessage();
        //Serial.println("Light OFF");
        //delay(500);
      }
      else if (incoming == "75"){
        analogWrite(Light, 191);
        N1_status = "N1_75";
        sendMessage();
        //Serial.println("Light OFF");
        //delay(500);
      }
      else if (incoming == "100"){
        analogWrite(Light, 255);
        N1_status = "N1_100";
        sendMessage();
        //Serial.println("Light OFF");
        //delay(500);
       }
       else{
        analogWrite(Light, 0);
        N1_status = "N1_0";
        sendMessage();
     }
  }
}

void sendMessage() {
  String LoRaMessage = String(currentValue1) +","+ N1_status +","+ bulb_status;
  LoRa.beginPacket();                
  LoRa.print(LoRaMessage);              
  LoRa.endPacket();                     
     
}
