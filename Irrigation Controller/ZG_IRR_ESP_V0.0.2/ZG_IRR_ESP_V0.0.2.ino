
   
/*
 * Thingsboard
 * wifiauto Connect
 * 01/06 Update
 * 
*/

#include <ArduinoJson.h>
#include <TaskScheduler.h>
#include <ESP8266WiFi.h>
#include <ThingsBoard.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>   
#include <ESP8266mDNS.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include "WiFiManager.h"
const long utcOffsetInSeconds = 19800;
char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

// Define NTP Client to get time
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org", utcOffsetInSeconds);

WiFiServer server(80);
#define TOKEN "g2Zm4CEu9t6ngFJvqTIP" //"VT4XtjB2lB5e6FPknlBW"  
int LED_Pin =13;

int status = WL_IDLE_STATUS;
unsigned long lastSend;
char thingsboardServer[] = "35.240.193.182";
WiFiClient wifiClient;
ThingsBoard tb(wifiClient);


// *********************************************************************
// Task Scheduler
// *********************************************************************
// objects
//Scheduler r;
//
//void JsonDe();
//void JsonDeone();
//Task t0(40000,  TASK_FOREVER, &JsonDe, &r);
//Task t1(80000,  TASK_FOREVER, &JsonDeone, &r);
//*********************************************************************

void configModeCallback (WiFiManager *myWiFiManager) {
  Serial.println("Entered config mode");
  Serial.println(WiFi.softAPIP());
  //if you used auto generated SSID, print it
  Serial.println(myWiFiManager->getConfigPortalSSID());
}
void setup() {
  // Initialize Serial port
  Serial.begin(115200);
//  Serial3.begin(115200);
    timeClient.begin();

//  InitWiFi();
  lastSend = 0; 
  while (!Serial) continue;
  // WiFiManager
      WiFiManager wifiManager;

      //wifiManager.resetSettings();
      wifiManager.autoConnect("ZG IRRIGATION","");
      wifiManager.setAPStaticIPConfig(IPAddress(10, 0, 1, 1), IPAddress(10, 0, 1, 1), IPAddress(255, 255, 255, 0));
      wifiManager.setAPCallback(configModeCallback);
      
      if (!wifiManager.autoConnect()) {
    Serial.println("failed to connect and hit timeout");
    //reset and try again, or maybe put it to deep sleep
    ESP.reset();
    delay(1000);
  }
      
      // if you get here you have connected to the WiFi
      Serial.println("Connected.");
      // start a server
      server.begin();
      Serial.println("Server started");
}
 
void loop() {
  timeClient.update();
//  r.execute();
// Check if a client has connected
    WiFiClient client = server.available();
    //if (!client)  {  return;  }
    //MEGA DATA EXCHANGER
//    if (Serial.available()) {
//    Serial.write(Serial.read());
//    }
//    
//  if ( !tb.connected() ) {
    reconnect();
//  }

  if ( millis() - lastSend > 1000 ) { // Update and send only after 1 seconds
    getAndSendTemperatureAndHumidityData();
    lastSend = millis();
  }

  tb.loop();
  }

char CID;
char DT;  
//float phvalue;
//float ecvalue;
//float tempvalue;
//float dovalue;

float pumpAvalue;
float pumpBvalue;
float DOpumpvalue;
float Solenoidvalue;
float Waterflowinletvalue;
float waterflowoutletvalue;
float waterlevelvalue;
float radationvalue;

float pHASP;
float ECASP;
float PHHYS;
float ECHYS;
void getAndSendTemperatureAndHumidityData()
{
  if (Serial.available()) 
  {
    // Allocate the JSON document
    // This one must be bigger than for the sender because it must store the strings
    StaticJsonDocument<500> doc;

    // Read the JSON document from the "link" serial port
    DeserializationError err = deserializeJson(doc, Serial);
    if (err == DeserializationError::Ok) 
    {
      // Print the values
      // (we must use as<T>() to resolve the ambiguity)
//      Serial.print("temperature = ");
//      Serial.println(doc["ESP8266"]["DHT11"]["temperature"].as<long>());
//      Serial.print("humidity = ");
//     Serial.println(doc["ESP8266"]["DHT11"]["humidity"].as<int>());
//      temp = doc["ESP8266"]["DHT11"]["temperature"].as<long>();
//      humi = doc["ESP8266"]["DHT11"]["humidity"].as<int>();
//      Serial.print(temp);
//      Serial.print(humi);
 //     CID=doc["ESP8266"]["CID"].as<long>();
      DT=doc["ESP8266"]["Date/Time"].as<float>();
//      phvalue= doc["Sensor"]["pH"].as<float>();
//      ecvalue=doc["Sensor"]["EC"].as<float>();
//      tempvalue=doc["Sensor"]["temp C"].as<float>();
//      dovalue=doc["Sensor"]["DO"].as<float>();
      
      pumpAvalue= doc["Zone1"]["Pump A"].as<float>();
      pumpBvalue=doc["Zone1"]["Pump B"].as<float>();
      DOpumpvalue=doc["Zone1"]["AIR PUMP"].as<float>();
      Solenoidvalue=doc["Zone1"]["SOLENOID"].as<float>();
      Waterflowinletvalue= doc["Zone1"]["WO"].as<float>();
      waterflowoutletvalue=doc["Zone1"]["WI"].as<float>();
      waterlevelvalue=doc["Zone1"]["WATERLEVEL"].as<float>();
      radationvalue=doc["Zone1"]["RADIATION"].as<float>();
      
      Serial.print(CID);
      
      Serial.print("date");
      Serial.println(DT);
//      Serial.print(phvalue);
//      Serial.print(ecvalue);
//      Serial.print(tempvalue);
//      Serial.print(dovalue);

      Serial.print(pumpAvalue);
      Serial.print(pumpBvalue);
      Serial.print(DOpumpvalue);
      Serial.print(Solenoidvalue);
      Serial.print(Waterflowinletvalue);
      Serial.print(waterflowoutletvalue);
      Serial.print(waterlevelvalue);
      Serial.print(radationvalue);
      
      Serial.print(pHASP);
      Serial.print(ECASP);
      Serial.print(PHHYS);
      Serial.print(ECHYS);
      
    } 
    else 
    {
      // Print error to the "debug" serial port
      Serial.print("deserializeJson() returned ");
      Serial.println(err.c_str());
  
      // Flush all bytes in the "link" serial port buffer
      while (Serial.available() > 0)
        Serial.read();
    }
  }
  
//  tb.sendTelemetryFloat("temperature", temp);
//  tb.sendTelemetryFloat("humidity", humi);
//  tb.sendTelemetryiint("CID", CID);
  tb.sendTelemetryFloat(" Doser Date/Time", DT);

 
  Serial.print(daysOfTheWeek[timeClient.getDay()]);

  String Day = daysOfTheWeek[timeClient.getDay()];
  Serial.print(", ");
  Serial.print(timeClient.getHours());
  Serial.print(":");
  Serial.print(timeClient.getMinutes());
  Serial.print(":");
  Serial.println(timeClient.getSeconds());
  tb.sendTelemetryString("Customer ID", "BHF ");
  tb.sendTelemetryInt("Date/time", 'DT');
  tb.sendTelemetryFloat("Hour", timeClient.getHours());
  tb.sendTelemetryFloat("Min", timeClient.getMinutes());
  tb.sendTelemetryFloat("Sec", timeClient.getSeconds());
  
//  tb.sendTelemetryString("Day", Day);
  tb.sendTelemetryFloat("Date", DT);
  
//  tb.sendTelemetryFloat("pH", phvalue);
//  tb.sendTelemetryFloat("EC", ecvalue);
//  tb.sendTelemetryFloat("temp C", tempvalue);
//  tb.sendTelemetryFloat("DO", dovalue);

  tb.sendTelemetryFloat("pH", pumpAvalue);
  tb.sendTelemetryFloat("EC", pumpBvalue);
  tb.sendTelemetryFloat("temp C", DOpumpvalue);
  tb.sendTelemetryFloat("DO", Solenoidvalue);
  tb.sendTelemetryFloat("L/min", Waterflowinletvalue);
  tb.sendTelemetryFloat("L/min", waterflowoutletvalue);
  tb.sendTelemetryFloat("cm", waterlevelvalue);
  tb.sendTelemetryFloat("nm", radationvalue);

 

  Serial.print(pumpAvalue);
  Serial.print(pumpBvalue);
  Serial.print(DOpumpvalue);
  Serial.print(Solenoidvalue);
  Serial.print(Waterflowinletvalue);
  Serial.print(waterflowoutletvalue);
   Serial.print(waterlevelvalue);
  Serial.print(radationvalue);
}

//void InitWiFi()
//{
//  Serial.println("Connecting to AP ...");
//  // attempt to connect to WiFi network
//
//  WiFi.begin(WIFI_AP, WIFI_PASSWORD);
//  while (WiFi.status() != WL_CONNECTED) {
//    delay(500);
//    Serial.print(".");
//  }
//  Serial.println("Connected to AP");
//}


void reconnect() {
  // Loop until we're reconnected
  while (!tb.connected()) {
    status = WiFi.status();
    if ( status != WL_CONNECTED) {
//      WiFi.begin(WIFI_AP, WIFI_PASSWORD);
      while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
      }
      Serial.println("Connected to AP");
    }
    Serial.print("Connecting to ThingsBoard node Server...");
    if ( tb.connect(thingsboardServer, TOKEN) ) {
      Serial.println( "[DONE]" );
    } else {
      Serial.print( "[FAILED]" );
      Serial.println( " : retrying in 5 seconds]" );
      // Wait 5 seconds before retrying
      delay( 3000 );
    }
  }
}  
