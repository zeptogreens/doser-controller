#include <Arduino.h>
#include <menu.h>
#include <menuIO/keypadIn.h>
#include <menuIO/serialOut.h>
#include <menuIO/u8g2Out.h>

#include <menuIO/chainStream.h>
#include <menuIO/serialOut.h>
#include <menuIO/serialIn.h>
#include <menuIO/U8GLibOut.h>

#include <TimerOne.h>
#include <ClickEncoder.h>

#include <MemoryFree.h>
#include <SD.h>
#include <TaskScheduler.h>
#include <ArduinoJson.h>
#include "DHT.h"
#include <RTClib.h>
#include <U8g2lib.h>

#include <RTClib.h>
#include <Time.h>
#include <TimeLib.h>
char t[32];
#include "Countimer.h"
#include "Timer.h"
Timer timer;

////Hardware pin definitions
int UVOUT = A2; //Output from the sensor
int REF_3V3 = A1; //3.3V power on the Arduino board

#define DHTPIN A0  // Digital pin connected to the DHT sensor

#define DHTTYPE DHT11   // DHT 11

DHT dht(DHTPIN, DHTTYPE);
using namespace Menu;

#define LEDPIN LED_BUILTIN
//#define USE_SSD1306

//EPROM
#include <EEPROMex.h>

//WatchDogTimer
//#include <avr/wdt.h>

//Define
#define BUZZER 37
#define LEDRED 42
#define LEDGREEN 44
#define LEDBLUE 58

//RELAY
#define PUMPA 51
#define PUMPB 50
#define AIRPUMP  49 //14
#define SOLVALVE 48 //15
int FirstState = 0;
int SecondState = 0;
int ThirdState = 0;
int MANUAL = 0;
int NC = 0;

int led = 8;
int led1 = 9;
int led2 = 10;
int led3 = 11;

unsigned long previousMillis = 0; 
const long interval = 1000;  

Countimer PUMPAON;
Countimer PUMPAOFF;
Countimer PUMPANone;

Countimer PUMPBON;
Countimer PUMPBOFF;
Countimer PUMPBNone;

Countimer AIRSTONEON;
Countimer AIRSTONEOFF;
Countimer AIRSTONEone;

int nhur=436,nmin=439;
int nhur1,nmin1;
int ledRED = LOW;
int ledGREEN = LOW;
int ledBLUE = LOW;

#define DEBUG 1                                // change value to 1 to enable debuging using serial monitor  
#define SERIAL_PORT_SPEED 115200                //9600, 19200, 38400, 57600, 115200

//****************************************** Water Level US Sensor **************************************************************
#define RORelay 19

const int trigPin = 52;
const int echoPin = 53;
long duration;
long distance;
float waterHeight;
int tankHeight ; //enter your water tank height here in [cm]
int totalVolume ; // volume here is in [cm^3], change depending on tank volume
float waterQuantity;
float volume;
float pi = 3.14159265359;
float offset = 245;
//Water distance
float Wtime=0,Wdistance=0;
int Wtemp=0;
bool TankM;
int tanksetpoint;

//***********************************************************************************************************************

//Tasks
void getNutrientTL();

Task t1(3000, TASK_FOREVER, &getNutrientTL);
//Task t3(5000, TASK_FOREVER, &t3Callback);

Scheduler runner;

//********************************** ENCODER


const byte ROWS = 4; //four rows
const byte COLS = 4; //four columns
//define the cymbols on the buttons of the keypads
char hexaKeys[ROWS][COLS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','-'},
  {'/','0','*','+'},
};
byte rowPins[ROWS] = {25,24,2,3}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {4,5,6,7}; //connect to the column pinouts of the keypad

//initialize an instance of class NewKeypad
Keypad customKeypad( makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS);


//********************Display Configuration **************************
//#include <SPI.h>
//#define USE_SWSPI
//#define U8_DC 17
//#define U8_CS 16
//#define U8_RST 23
//#define fontName  u8g2_font_5x7_tf // u8g2_font_synchronizer_nbp_tf           
////  #define fontX 5
////  #define fontY 9
//#define offsetX 0
//#define offsetY 0
//#define U8_Width 128
//#define U8_Height 64
//
////#include <Arduino.h>
////#include <U8g2lib.h>
////
////#ifdef U8X8_HAVE_HW_SPI
////#include <SPI.h>
////#endif
////#ifdef U8X8_HAVE_HW_I2C
////#include <Wire.h>
////#endif
//
//int x = 0;
//int scroll_direction = 1;    
#include <Wire.h>
  #define fontName u8g2_font_5x7_tf// u8g2_font_blipfest_07_tr  
#define fontX 7
#define fontY 11
#define offsetX 0
#define offsetY 0
#define U8_Width 128
#define U8_Height 64
#define USE_HWI2C

U8G2_KS0108_128X64_F u8g2(U8G2_R0, 37, 38, 39, 40, 41, 42, 43, 44, /*enable=*/36, /*dc=*/ 34, /*cs0=*/ 45, /*cs1=*/ 46, /*cs2=*/ 35, /* reset=*/  47);   // Set R/W to low!//Direction of title scroll, 1 right, -1 left//Offset postion of title

#define LEDPIN 13

//************SD Card ********///
#define SD_DETECT_PIN   49
#define SDSS            53
File sdcard_file;
    

RTC_DS3231 rtc; //for date time
char daysOfTheWeek[7][12] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};  

int hourupg;
int minupg;
uint8_t secslive;
int Monthlive;
int Datelive;
int Yearlive;


// define menu colors --------------------------------------------------------
//each color is in the format:
//  {{disabled normal,disabled selected},{enabled normal,enabled selected, enabled editing}}
// this is a monochromatic color table
const colorDef<uint8_t> colors[6] MEMMODE = {
  {{0, 0}, {0, 1, 1}}, //bgColor
  {{1, 1}, {1, 0, 0}}, //fgColor
  {{1, 1}, {1, 0, 0}}, //valColor
  {{1, 1}, {1, 0, 0}}, //unitColor
  {{0, 1}, {0, 0, 1}}, //cursorColor
  {{1, 1}, {1, 0, 0}}, //titleColor
};

result doAlert(eventMask e, prompt &item);

result showEvent(eventMask e, navNode& nav, prompt& item) {
  Serial.print("event: ");
  Serial.println(e);
  return proceed;
}


result action1(eventMask e, navNode& nav, prompt &item) {
  Serial.print(e);
  Serial.println(" action1 executed, proceed menu");
  Serial.flush();
  return proceed;
}

result action2(eventMask e, navNode& nav, prompt &item) {
  Serial.print(e);
  Serial.print(" action2 executed, quiting menu");
  return quit;
}

result home(eventMask e, navNode& nav, prompt &item) {
  Serial.print(e);
  Serial.print(" home");
  return quit;
}


int ledCtrl = LOW;

result myLedOn() {
  ledCtrl = HIGH;
  digitalWrite (BUZZER, HIGH);
  return proceed;
}
result myLedOff() {
  
  ledCtrl = LOW;
  digitalWrite (BUZZER, LOW);
  return proceed;
}
//*******************************TANK**********************************
TOGGLE(TankM,Tankcontrol,"TANK MANAGEMENT : ",doNothing,noEvent,noStyle//,doExit,enterEvent,noStyle
  ,VALUE("STOP",true,doNothing,enterEvent)
  ,VALUE("START",false,doExit,noEvent)
);

result TankMgStart() {
  TankM = true;
  return proceed;
}
result TankMgStop() {
  TankM = false;
  return proceed;
}
///**********************Relay Control******************************

bool relayPA ;
bool relayPB ;
bool relayAir ;
bool relaySol ;

TOGGLE(relayPA, pumpa, "     PUMP A : ", doNothing, noEvent, noStyle
       , VALUE("ON", HIGH, doNothing, noEvent)
       , VALUE("OFF", LOW, doNothing, noEvent)
      );


TOGGLE(relayPB, pumpb, "     PUMP B : ", doNothing, noEvent, noStyle
       , VALUE("ON", HIGH, doNothing, noEvent)
       , VALUE("OFF", LOW, doNothing, noEvent)
      );
      
TOGGLE(relayAir, air, "     DO PUMP : ", doNothing, noEvent, noStyle
       , VALUE("ON", HIGH, doNothing, noEvent)
       , VALUE("OFF", LOW, doNothing, noEvent)
      );

TOGGLE(relaySol, sol, "     SOLENOID : ", doNothing, noEvent, noStyle
       , VALUE("ON", HIGH, doNothing, noEvent)
       , VALUE("OFF", LOW, doNothing, noEvent)
      );
      
void switching(){
  
  if(relayPA==HIGH ){
    
    relaySol==LOW;
    }
    else 
    {
      relaySol==HIGH;
      };
  if(relayPB==HIGH) {
    relaySol==LOW;
    
    }
    else{
    
      relaySol==HIGH;
      }
  
  }
//******************************************************************
  bool Autocnttt;
TOGGLE(Autocnttt, Manuall, "MANUAL : ", doNothing, noEvent, noStyle
       , VALUE("ON", true,  doNothing, enterEvent)
       , VALUE("OFF",false, doNothing, enterEvent)
      );
MENU(MANmenu, " MANUAL MODE", showEvent, enterEvent, noStyle
 ,SUBMENU(Manuall)
//, OP(" ACTUATOR'S", doNothing, noEvent)     
     , SUBMENU(pumpa)
     , SUBMENU(pumpb)
     , SUBMENU(air)
     , SUBMENU(sol)
, EXIT("<BACK")
    );      

///****************************************************************    
//customizing a prompt look!
//by extending the prompt class
class altPrompt:public prompt {
public:
  altPrompt(constMEM promptShadow& p):prompt(p) {}
  Used printTo(navRoot &root,bool sel,menuOut& out, idx_t idx,idx_t len,idx_t) override {
    return out.printRaw(F("special prompt!"),len);;
  }
};
//*****************************ZONE ONE AUTO ************************************
int Z1PASThr;
int Z1PASTmin;
int Z1PASPhr;
int Z1PASPmin;

int Z1ZPASThr;
int Z1ZPASTmin;
int Z1ZPASPhr;
int Z1ZPASPmin;


int Z1PBSThr;
int Z1PBSTmin;
int Z1PBSPhr;
int Z1PBSPmin;

int Z1ZPBSThr;
int Z1ZPBSTmin;
int Z1ZPBSPhr;
int Z1ZPBSPmin;

int Z1APSThr;
int Z1APSTmin;
int Z1APSPhr;
int Z1APSPmin;

int Z1ZAPSThr;
int Z1ZAPSTmin;
int Z1ZAPSPhr;
int Z1ZAPSPmin;


int EepromZ1PASThr = 200;
int EepromZ1PASTmin =202;
int EepromZ1PASPhr =204;
int EepromZ1PASPmin= 206;

int EepromZ1ZPASThr = 300;
int EepromZ1ZPASTmin =302;
int EepromZ1ZPASPhr =304;
int EepromZ1ZPASPmin= 306;


int EepromZ1PBSThr = 208;
int EepromZ1PBSTmin =210;
int EepromZ1PBSPhr =212;
int EepromZ1PBSPmin= 214;

int EepromZ1ZPBSThr = 308;
int EepromZ1ZPBSTmin =310;
int EepromZ1ZPBSPhr =312;
int EepromZ1ZPBSPmin= 314;


int EepromZ1APSThr = 216;
int EepromZ1APSTmin =214;
int EepromZ1APSPhr =218;
int EepromZ1APSPmin= 220;

int EepromZ1ZAPSThr = 316;
int EepromZ1ZAPSTmin =314;
int EepromZ1ZAPSPhr =318;
int EepromZ1ZAPSPmin= 320;

char PUMPAONtime;
char PUMPAOFFtime;
//***SAVE TIME********************************************************///
result Z1PAsavetime() {
  EEPROM.write(EepromZ1PASThr, Z1PASThr);
  EEPROM.write(EepromZ1PASTmin, Z1PASTmin);

  return proceed;
}
result Z1PAsavedue() {

  EEPROM.write(EepromZ1PASPhr, Z1PASPhr);
  EEPROM.write(EepromZ1PASPmin, Z1PASPmin);
  return proceed;
}
void EepromReadZ1AP()
{
  Z1PASThr = EEPROM.read(EepromZ1PASThr);
  Z1PASTmin = EEPROM.read(EepromZ1PASTmin);
  Z1PASPhr = EEPROM.read(EepromZ1PASPhr);
  Z1PASPmin = EEPROM.read(EepromZ1PASPmin);
  return proceed;
}
//*****************************************************************
result Z1ZPAsavetime() {
  EEPROM.write(EepromZ1ZPASThr, Z1ZPASThr);
  EEPROM.write(EepromZ1ZPASTmin, Z1ZPASTmin);

  return proceed;
}
result Z1ZPAsavedue() {

  EEPROM.write(EepromZ1ZPASPhr, Z1ZPASPhr);
  EEPROM.write(EepromZ1ZPASPmin, Z1ZPASPmin);
  return proceed;
}
void EepromReadZ1ZAP()
{
  Z1ZPASThr = EEPROM.read(EepromZ1ZPASThr);
  Z1ZPASTmin = EEPROM.read(EepromZ1ZPASTmin);
  Z1ZPASPhr = EEPROM.read(EepromZ1ZPASPhr);
  Z1ZPASPmin = EEPROM.read(EepromZ1ZPASPmin);
  return proceed;
}  
//**********************************************************************************
result Z1PBsavetime() {
  EEPROM.write(EepromZ1PBSThr, Z1PBSThr);
  EEPROM.write(EepromZ1PBSTmin, Z1PBSTmin);

  return proceed;
}
result Z1PBsavedue() {

  EEPROM.write(EepromZ1PBSPhr, Z1PBSPhr);
  EEPROM.write(EepromZ1PBSPmin, Z1PBSPmin);
  return proceed;
}
void EepromReadZ1PB()
{
  Z1PBSThr = EEPROM.read(EepromZ1PBSThr);
  Z1PBSTmin = EEPROM.read(EepromZ1PBSTmin);
  Z1PBSPhr = EEPROM.read(EepromZ1PBSPhr);
  Z1PBSPmin = EEPROM.read(EepromZ1PBSPmin);
  return proceed;
}
//*************************************************************************
result Z1ZPBsavetime() {
  EEPROM.write(EepromZ1ZPBSThr, Z1ZPBSThr);
  EEPROM.write(EepromZ1ZPBSTmin, Z1ZPBSTmin);

  return proceed;
}
result Z1ZPBsavedue() {

  EEPROM.write(EepromZ1ZPBSPhr, Z1ZPBSPhr);
  EEPROM.write(EepromZ1ZPBSPmin, Z1ZPBSPmin);
  return proceed;
}
void EepromReadZ1ZPB()
{
  Z1ZPBSThr = EEPROM.read(EepromZ1ZPBSThr);
  Z1ZPBSTmin = EEPROM.read(EepromZ1ZPBSTmin);
  Z1ZPBSPhr = EEPROM.read(EepromZ1ZPBSPhr);
  Z1ZPBSPmin = EEPROM.read(EepromZ1ZPBSPmin);
  return proceed;
}
//*****************************************************************
result Z1APsavetime() {
  EEPROM.write(EepromZ1APSThr, Z1APSThr);
  EEPROM.write(EepromZ1APSTmin, Z1APSTmin);

  return proceed;
}
result Z1APsavedue() {

  EEPROM.write(EepromZ1APSPhr, Z1APSPhr);
  EEPROM.write(EepromZ1APSPmin, Z1APSPmin);
  return proceed;
}
void EepromReadZ1Air()
{
  Z1APSThr = EEPROM.read(EepromZ1APSThr);
  Z1APSTmin = EEPROM.read(EepromZ1APSTmin);
  Z1APSPhr = EEPROM.read(EepromZ1APSPhr);
  Z1APSPmin = EEPROM.read(EepromZ1APSPmin);
  return proceed;
}
//*****************************************************************************
result Z1ZAPsavetime() {
  EEPROM.write(EepromZ1ZAPSThr, Z1ZAPSThr);
  EEPROM.write(EepromZ1ZAPSTmin, Z1ZAPSTmin);

  return proceed;
}
result Z1ZAPsavedue() {

  EEPROM.write(EepromZ1ZAPSPhr, Z1ZAPSPhr);
  EEPROM.write(EepromZ1ZAPSPmin, Z1ZAPSPmin);
  return proceed;
}
void EepromReadZ1ZAir()
{
  Z1ZAPSThr = EEPROM.read(EepromZ1ZAPSThr);
  Z1ZAPSTmin = EEPROM.read(EepromZ1ZAPSTmin);
  Z1ZAPSPhr = EEPROM.read(EepromZ1ZAPSPhr);
  Z1ZAPSPmin = EEPROM.read(EepromZ1ZAPSPmin);
  return proceed;
}
//******************************************************************
// A PUMP
altMENU(menu, Z1PASET1, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z1PASThr, ":", "  HH", 0, 23, 10, 1, Z1PAsavetime, enterEvent, wrapStyle)
        , FIELD(Z1PASTmin, "", "  MIN", 0, 59, 10, 1, Z1PAsavetime, enterEvent, wrapStyle) 
       );
       
altMENU(menu, Z1PASET2, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z1PASPhr, ":", "  HH", 0, 23, 10, 1, Z1PAsavedue, enterEvent, wrapStyle)
        , FIELD(Z1PASPmin, "", "  MIN", 0, 59, 10, 1, Z1PAsavedue, enterEvent, wrapStyle)
              );
//***************************************************************************

//  B PUMP 
altMENU(menu, Z1PBSET1, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z1PBSThr, ":", "  HH", 0, 23, 10, 1, Z1PBsavetime, enterEvent, wrapStyle)
        , FIELD(Z1PBSTmin, "", "  MIN", 0, 59, 10, 1, Z1PBsavetime, enterEvent, wrapStyle) 
       );
       
altMENU(menu, Z1PBSET2, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z1PBSPhr, ":", "  HH", 0, 23, 10, 1, Z1PBsavedue, enterEvent, wrapStyle)
        , FIELD(Z1PBSPmin, "", "  MIN", 0, 59, 10, 1, Z1PBsavedue, enterEvent, wrapStyle)
              );
//****************************************************************

////AIRSTONE PUMP
altMENU(menu, Z1APSET1, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z1APSThr, ":", "  HH", 0, 23, 10, 1, Z1APsavetime, enterEvent, wrapStyle)
        , FIELD(Z1APSTmin, "", "  MIN", 0, 59, 10, 1, Z1APsavetime, enterEvent, wrapStyle) 
       );
       
altMENU(menu, Z1APSET2, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z1APSPhr, ":", "  HH", 0, 23, 10, 1, Z1APsavedue, enterEvent, wrapStyle)
        , FIELD(Z1APSPmin, "", "  MIN", 0, 59, 10, 1, Z1APsavedue, enterEvent, wrapStyle)
              );
//****************************************************************

                           //****************************************************************



////**************************************************************************************
 ;
MENU(Z1PA, "PUMP A", showEvent, enterEvent, noStyle
     
//    , FIELD(PUMPAONtime, " ON TIME", " ", 0, 0, 0, doNothing, enterEvent, wrapStyle)
//      , FIELD(PUMPAOFFtime, " OFF TIME", " ", 0, 0, 0, doNothing, enterEvent, wrapStyle)

      , SUBMENU(Z1PASET1)
      , SUBMENU(Z1PASET2)
     , OP("       SAVE"  , EepromReadZ1AP, enterEvent)
     
     

     , EXIT("<BACK")
    );

MENU(Z1PB, "PUMP B", showEvent, enterEvent, noStyle
      , SUBMENU(Z1PBSET1)
      , SUBMENU(Z1PBSET2)
     , OP("       SAVE"  ,EepromReadZ1PB , enterEvent)

     , EXIT("<BACK")
    );  

MENU(Z1AP, "DO PUMP", showEvent, enterEvent, noStyle
//    , FIELD(airon, " ON TIME", " ", 0, 0, 0, doNothing, enterEvent, wrapStyle)

      , SUBMENU(Z1APSET1)
      , SUBMENU(Z1APSET2)
      
     , OP("       SAVE"  ,EepromReadZ1Air , enterEvent)

     , EXIT("<BACK")
    ); 

//MENU(Z1WL, "NT SOL", showEvent, enterEvent, noStyle
//      , SUBMENU(Z1SOLSET1)
//      , SUBMENU(Z1SOLSET2)
//     , OP("       CONFIRM"  , EepromReadZ1SOL, enterEvent)
//
//     , EXIT("<BACK")
//    ); 
//*************************************************************              


TOGGLE(ledRED, redLED, "       RED : ", doNothing, noEvent, noStyle
       , VALUE("On", HIGH, doNothing, noEvent)
       , VALUE("Off", LOW, doNothing, noEvent)
      );


TOGGLE(ledGREEN, greenLED, "       GREEN : ", doNothing, noEvent, noStyle
       , VALUE("On", HIGH, doNothing, noEvent)
       , VALUE("Off", LOW, doNothing, noEvent)
      );
      
TOGGLE(ledBLUE, blueLED, "       BLUE : ", doNothing, noEvent, noStyle
       , VALUE("On", HIGH, doNothing, noEvent)
       , VALUE("Off", LOW, doNothing, noEvent)
      );
      
//*****************************************************************
int selTest21=0;
SELECT(selTest21,selMenu21,"Pump A",doNothing,noEvent,noStyle
  ,VALUE("ON",0,doNothing,noEvent)
  ,VALUE("OFF",1,doNothing,noEvent)
);

int selTest31=0;
SELECT(selTest31,selMenu31,"Pump B",doNothing,noEvent,noStyle
  ,VALUE("ON",0,doNothing,noEvent)
  ,VALUE("OFF",1,doNothing,noEvent)
);

int selTest41=0;
SELECT(selTest41,selMenu41,"DO Pump",doNothing,noEvent,noStyle
  ,VALUE("ON",0,doNothing,noEvent)
  ,VALUE("OFF",1,doNothing,noEvent)
);

MENU(selMenu01,"ZONE 1",showEvent,anyEvent,noStyle
 ,SUBMENU(Z1PA)
 ,SUBMENU(Z1PB)
 ,SUBMENU(Z1AP)
 //,SUBMENU(Z1WL)
  ,EXIT("<BACK>")

  );

MENU(selMenu02,"ZONE 2",showEvent,anyEvent,noStyle
 ,SUBMENU(Z1PA)
 ,SUBMENU(Z1PB)
 ,SUBMENU(Z1AP)
 //,SUBMENU(Z1WL)
  ,EXIT("<BACK>")

  );
  MENU(selMenu03,"ZONE 3",showEvent,anyEvent,noStyle
 ,SUBMENU(Z1PA)
 ,SUBMENU(Z1PB)
 ,SUBMENU(Z1AP)
 //,SUBMENU(Z1WL)
  ,EXIT("<BACK>")

  );
  MENU(selMenu04,"ZONE 4",showEvent,anyEvent,noStyle
 ,SUBMENU(Z1PA)
 ,SUBMENU(Z1PB)
 ,SUBMENU(Z1AP)
 //,SUBMENU(Z1WL)
  ,EXIT("<BACK>")

  );
bool Autocnt;
void runer(){
if(Autocnt==true)
{
  PUMPAON.start();
  PUMPBON.start();
  AIRSTONEON.start();
   // Serial.print ("auto start");
}
}
void stoper(){
if(Autocnt==false)
{
 digitalWrite(PUMPA,LOW);
  digitalWrite(PUMPB,LOW);
  digitalWrite(AIRPUMP,LOW);
  digitalWrite(SOLVALVE,LOW);
}
}
TOGGLE(Autocnt, autoOn, "AUTOMATIC : ", doNothing, noEvent, noStyle
       , VALUE("ON", true, runer, enterEvent)
       , VALUE("OFF",false,stoper, enterEvent)
      );
        
MENU(Automatic," AUTO MODE",showEvent,anyEvent,noStyle
 ,SUBMENU(autoOn)
 ,SUBMENU(selMenu01)
 ,SUBMENU(selMenu02)
 ,SUBMENU(selMenu03)
 ,SUBMENU(selMenu04)
  ,EXIT("<BACK>")

  );

// A PUMP
altMENU(menu, Z1ZPASET1 , " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z1ZPASThr, ":", "  HH", 0, 23, 10, 1, Z1ZPAsavetime, enterEvent, wrapStyle)
        , FIELD(Z1ZPASTmin, "", "  MIN", 0, 59, 10, 1, Z1ZPAsavetime, enterEvent, wrapStyle) 
       );
       
altMENU(menu, Z1ZPASET2, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z1ZPASPhr, ":", "  HH", 0, 23, 10, 1, Z1ZPAsavedue, enterEvent, wrapStyle)
        , FIELD(Z1ZPASPmin, "", "  MIN", 0, 59, 10, 1, Z1ZPAsavedue, enterEvent, wrapStyle)
              );
//***************************************************************************

////  B PUMP 
//altMENU(menu, Z1ZPBSET1, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
//        , FIELD(Z1ZPBSThr, ":", "  HH", 0, 23, 10, 1, Z1PBsavetime, enterEvent, wrapStyle)
//        , FIELD(Z1ZPBSTmin, "", "  MIN", 0, 59, 10, 1, Z1PBsavetime, enterEvent, wrapStyle) 
//       );
//       
//altMENU(menu, Z1ZPBSET2, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
//        , FIELD(Z1ZPBSPhr, ":", "  HH", 0, 23, 10, 1, Z1ZPBsavedue, enterEvent, wrapStyle)
//        , FIELD(Z1ZPBSPmin, "", "  MIN", 0, 59, 10, 1, Z1ZPBsavedue, enterEvent, wrapStyle)
//              );
////****************************************************************
//
//////AIRSTONE PUMP
//altMENU(menu, Z1ZAPSET1, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
//        , FIELD(Z1ZAPSThr, ":", "  HH", 0, 23, 10, 1, Z1ZAPsavetime, enterEvent, wrapStyle)
//        , FIELD(Z1ZAPSTmin, "", "  MIN", 0, 59, 10, 1, Z1ZAPsavetime, enterEvent, wrapStyle) 
//       );
//       
//altMENU(menu, Z1APSET2, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
//        , FIELD(Z1APSPhr, ":", "  HH", 0, 23, 10, 1, Z1ZAPsavedue, enterEvent, wrapStyle)
//        , FIELD(Z1APSPmin, "", "  MIN", 0, 59, 10, 1, Z1ZAPsavedue, enterEvent, wrapStyle)
//              );
MENU(UNIT_1, "SCHEDULE 1", showEvent, enterEvent, wrapStyle 
   , SUBMENU(Z1ZPASET1)  
   , SUBMENU(Z1ZPASET2)
//   ,OP("Conform",unit1read,enterEvent)
   , EXIT("<Back")
);
MENU(UNIT_2, "SCHEDULE 2", showEvent,enterEvent, wrapStyle
   , SUBMENU(Z1ZPASET1)  
   , SUBMENU(Z1ZPASET2)
//   ,OP("Conform",unit2read,enterEvent)
   , EXIT("<Back")
);
MENU(UNIT_3, "SCHEDULE 3", showEvent, enterEvent, wrapStyle 
   , SUBMENU(Z1ZPASET1)  
   , SUBMENU(Z1ZPASET2)
//   ,OP("Conform",unit3read,enterEvent)
   , EXIT("<Back")
);
MENU(UNIT_4, "SCHEDULE 4", showEvent,enterEvent, wrapStyle
   , SUBMENU(Z1ZPASET1)  
   , SUBMENU(Z1ZPASET2)
//   ,OP("Conform",unit4read,enterEvent)
   , EXIT("<Back")
);
MENU(UNIT_5, "SCHEDULE 5", showEvent,enterEvent, wrapStyle 
   , SUBMENU(Z1ZPASET1)  
   , SUBMENU(Z1ZPASET2)
//   ,OP("Conform",unit5read,enterEvent)
   , EXIT("<Back")
);
MENU(UNIT_6, "SCHEDULE 6", showEvent,enterEvent, wrapStyle 
   , SUBMENU(Z1ZPASET1)  
   , SUBMENU(Z1ZPASET2)
//   ,OP("Conform",unit6read,enterEvent)
   , EXIT("<Back")
);
MENU(UNIT_7, "SCHEDULE 7", showEvent,enterEvent, wrapStyle
   , SUBMENU(Z1ZPASET1)  
   , SUBMENU(Z1ZPASET2)
//   ,OP("Conform",unit7read,enterEvent)
   , EXIT("<Back")
);
MENU(UNIT_8,"SCHEDULE 8", showEvent,enterEvent, wrapStyle 
   , SUBMENU(Z1ZPASET1)  
   , SUBMENU(Z1ZPASET2)
//   ,OP("Conform",unit8read,enterEvent)
   , EXIT("<Back")
);

MENU(UNIT_9, "SCHEDULE 9", showEvent,enterEvent, wrapStyle 
   , SUBMENU(Z1ZPASET1)  
   , SUBMENU(Z1ZPASET2)
//   ,OP("Conform",unit5read,enterEvent)
   , EXIT("<Back")
);
MENU(UNIT_10, "SCHEDULE 10", showEvent,enterEvent, wrapStyle 
   , SUBMENU(Z1ZPASET1)  
   , SUBMENU(Z1ZPASET2)
//   ,OP("Conform",unit6read,enterEvent)
   , EXIT("<Back")
);
MENU(UNIT_11, "SCHEDULE 11", showEvent,enterEvent, wrapStyle
   , SUBMENU(Z1ZPASET1)  
   , SUBMENU(Z1ZPASET2)
//   ,OP("Conform",unit7read,enterEvent)
   , EXIT("<Back")
);
MENU(UNIT_12,"SCHEDULE 12", showEvent,enterEvent, wrapStyle 
   , SUBMENU(Z1ZPASET1)  
   , SUBMENU(Z1ZPASET2)
//   ,OP("Conform",unit8read,enterEvent)
   , EXIT("<Back")
);
MENU(Z1ZPA,"PUMP A",showEvent,anyEvent,noStyle
 ,SUBMENU(UNIT_1)
  ,SUBMENU(UNIT_2)
  ,SUBMENU(UNIT_3)
  ,SUBMENU(UNIT_4)
  ,SUBMENU(UNIT_5)
  ,SUBMENU(UNIT_6)
  ,SUBMENU(UNIT_7)
  ,SUBMENU(UNIT_8)
  ,SUBMENU(UNIT_9)
  ,SUBMENU(UNIT_10)
  ,SUBMENU(UNIT_11)
  ,SUBMENU(UNIT_12)
  //,SUBMENU(START)
//  ,OP("START",scheduling,enterEvent)
  ,EXIT("<Back") 
);


MENU(Z1ZPB,"PUMP B",showEvent,anyEvent,noStyle
 ,SUBMENU(UNIT_1)
  ,SUBMENU(UNIT_2)
  ,SUBMENU(UNIT_3)
  ,SUBMENU(UNIT_4)
  ,SUBMENU(UNIT_5)
  ,SUBMENU(UNIT_6)
  ,SUBMENU(UNIT_7)
  ,SUBMENU(UNIT_8)
   ,SUBMENU(UNIT_9)
  ,SUBMENU(UNIT_10)
  ,SUBMENU(UNIT_11)
  ,SUBMENU(UNIT_12)
  //,SUBMENU(START)
//  ,OP("START",scheduling,enterEvent)
  ,EXIT("<Back") 
);
MENU(Z1ZAP,"DO PUMP",showEvent,anyEvent,noStyle
 ,SUBMENU(UNIT_1)
  ,SUBMENU(UNIT_2)
  ,SUBMENU(UNIT_3)
  ,SUBMENU(UNIT_4)
  ,SUBMENU(UNIT_5)
  ,SUBMENU(UNIT_6)
  ,SUBMENU(UNIT_7)
  ,SUBMENU(UNIT_8)
   ,SUBMENU(UNIT_9)
  ,SUBMENU(UNIT_10)
  ,SUBMENU(UNIT_11)
  ,SUBMENU(UNIT_12)
  //,SUBMENU(START)
//  ,OP("START",scheduling,enterEvent)
  ,EXIT("<Back") 
);

MENU(sellMenu01,"ZONE 1",showEvent,anyEvent,noStyle
 ,SUBMENU(Z1ZPA)
 ,SUBMENU(Z1ZPB)
 ,SUBMENU(Z1ZAP)
 
// ,SUBMENU(Z1WL)
  ,EXIT("<BACK>")

  );

MENU(sel2Menu02,"ZONE 2",showEvent,anyEvent,noStyle
 ,SUBMENU(Z1ZPA)
 ,SUBMENU(Z1ZPB)
 ,SUBMENU(Z1ZAP)
// ,SUBMENU(Z1WL)
  ,EXIT("<BACK>")

  );
  MENU(sel3Menu03,"ZONE 3",showEvent,anyEvent,noStyle
 ,SUBMENU(Z1ZPA)
 ,SUBMENU(Z1ZPB)
 ,SUBMENU(Z1ZAP)
// ,SUBMENU(Z1WL)
  ,EXIT("<BACK>")

  );
  MENU(sel4Menu04,"ZONE 4",showEvent,anyEvent,noStyle
 ,SUBMENU(Z1ZPA)
 ,SUBMENU(Z1ZPB)
 ,SUBMENU(Z1ZAP)
// ,SUBMENU(Z1WL)
  ,EXIT("<BACK>")

  );
  bool Autocntt;
  TOGGLE(Autocntt, sche, "SCHEDULE : ", doNothing, noEvent, noStyle
       , VALUE("ON", true, doNothing, enterEvent)
       , VALUE("OFF",false,doNothing, enterEvent)
      );
  MENU(SCHEDULE," SCHEDULE MODE",showEvent,anyEvent,noStyle
 ,SUBMENU(sche)
 ,SUBMENU(sellMenu01)
 ,SUBMENU(sel2Menu02)
 ,SUBMENU(sel3Menu03)
 ,SUBMENU(sel4Menu04)
  ,EXIT("<BACK>")
);
//int selTest35=0;
//TOGGLE(selTest35, autoOn, "Auto Mode   : ", doNothing, noEvent, noStyle
//       , VALUE("ON", HIGH, doNothing, noEvent)
//       , VALUE("OFF", LOW, doNothing, noEvent)
//      );
//MENU(SCHEDULE," SCHEDULE",showEvent,anyEvent,noStyle
//// ,SUBMENU(autoOn1)
//// ,SUBMENU(selMenu01)
//// ,SUBMENU(selMenu02)
//// ,SUBMENU(selMenu03)
//// ,SUBMENU(selMenu04)
//  ,EXIT("<BACK>")
//*********************************************************************************************************
 
int totalVolume1;
int EepromNTHset=300;
int EepromNTVSet=310;
///*********EPROM********************///
result NTHset() {
  EEPROM.write(EepromNTHset, tankHeight);  
  
//   EEPROM.read(EepromNTHset, tankHeight);  

  
  return proceed;
}
result NTVset() {
  
  EEPROM.write(EepromNTVSet, totalVolume1);
// EEPROM.read(EepromNTVSet, totalVolume);
  return proceed;
}
int EepromTSLSet = 350;
result TSLset() {
  
  EEPROM.write(EepromTSLSet, tanksetpoint);

  return proceed;
}
result EepromReadNT()
{
  tankHeight = EEPROM.read(EepromNTHset);
  totalVolume1 = EEPROM.read(EepromNTVSet);
  tanksetpoint = EEPROM.read(EepromTSLSet);

  
} 
int distanceincm;
int distanceinInch; 
MENU(tanksetting," TANK ",showEvent,anyEvent,noStyle
  ,SUBMENU(Tankcontrol)
  ,FIELD(distanceinInch,"    LEVEL : "," inch",0,0,0,0,doNothing,noEvent,noStyle)
  ,FIELD(tankHeight,"    HEIGHT : ","cm",0,1000,10,1,NTHset,enterEvent,wrapStyle)
  //,FIELD(totalVolume1,"    VOLUME : ","cm^3",0,5000,100,1,NTVset,enterEvent,wrapStyle)
  ,FIELD(tanksetpoint,"    SETLEVEL : ","cm",0,500,100,1,TSLset,enterEvent,wrapStyle)
  ,OP("          SAVE",EepromReadNT,enterEvent)
    
  ,EXIT("<BACK")
);

MENU(WLmenu," TANK LEVEL",showEvent,noEvent,noStyle
// ,FIELD(distanceincm,"LEVEL  "," cm",0,0,0,0,doNothing,noEvent,noStyle)
 ,FIELD(distanceinInch," LEVEL : "," inch",0,0,0,0,doNothing,noEvent,noStyle)
  ,FIELD(waterHeight,"     HEIGHT  :"," cm",0,0,0,0,doNothing,noEvent,noStyle)
  ,FIELD(waterQuantity,"   QUANTITY  :"," ml",0,0,0,0,doNothing,noEvent,noStyle)
  ,SUBMENU(tanksetting)
  ,SUBMENU(sol)
  
  
  ,EXIT("<BACK")
);

MENU(Settingsmenu," SETTINGS",showEvent,noEvent,noStyle
  , OP(" INFO", Cinfo, enterEvent)
  
  ,EXIT("<BACK")
);
MENU(mainMenu, "  IRRIGATION CONTROLLER", doNothing, noEvent, wrapStyle
     , OP(" MONITOR", Home, enterEvent)
     
     ,SUBMENU(Automatic)
     ,SUBMENU(SCHEDULE)
     ,SUBMENU(MANmenu)
     ,SUBMENU(tanksetting)
     ,SUBMENU(Settingsmenu)
//    , OP(" ABOUT", goHome, enterEvent)

     //  ,EXIT("<BACK")
    );




serialIn serial(Serial);

////input from the encoder + encoder button + serial
//menuIn* inputsList[]={&encStream,&encButton,&serial};
//chainStream<3> in(inputsList);//3 is the number of inputs

//fontY should now account for fontMarginY
#define fontX 3
#define fontY 10
#define MAX_DEPTH 10

//this macro replaces all the above commented lines
MENU_OUTPUTS(out, MAX_DEPTH
             // ,U8GLIB_OUT(u8g2,colors,fontX,fontY,{0,0,128/fontX,64/fontY})
             , U8G2_OUT(u8g2, colors, fontX, fontY, offsetX, offsetY, {0, 0, U8_Width / fontX, U8_Height / fontY})
             , SERIAL_OUT(Serial)
             , NONE
            );
keypadIn kpad(customKeypad);
NAVROOT(nav,mainMenu,MAX_DEPTH,kpad,out);

result doAlert(eventMask e, prompt &item) {
  nav.idleOn(alert);
  return proceed;
}

result Home(eventMask e, prompt &item) {
  nav.idleOn(Time);

}
result Time (menuOut& o,idleEvent e) {
  DateTime now = rtc.now();
  {
//

//
//
//   nhur1=now.hour();
//   nmin1=now.minute();
//    Serial.print("Time: ");
//     Serial.print(nhur1);
//     Serial.print(":");
//      
////      u8g2.setCursor(, 1);
//      u8g2.drawFrame( nhur1);
//      u8g2.drawFrame(":");
//      Serial.print( now.minute());
////      u8g2.setCursor(5,1);
//      u8g2.drawFrame( nmin1);
//      u8g2.setCursor(10,1);
////      lcd.print("Date: ");
//      //u8g2.drawStr(58, 52, "Date:")
//      u8g2.drawFrame( now.day());
//       u8g2.drawFrame("/");
//      
//      u8g2.drawFrame( now.month());
//       u8g2.drawFrame("/");
//     u8g2.drawFrame( now.year());
//    
//    }
//
 u8g2.drawFrame(0, 0, 128, 64);
   u8g2.drawFrame(0, 0, 128, 12);
     u8g2.drawFrame(0, 0, 128, 30);


   u8g2.drawFrame(0, 0, 128, 48);

    u8g2.drawStr(16, 10, "      MONITOR    ");

      

 u8g2.drawStr(1, 24, " D/T:");
    u8g2.setCursor(28, 24);
    u8g2.print( now.day());
    u8g2.drawStr(38, 24, "/");
    u8g2.setCursor(44, 24);
    u8g2.print( now.month());
    u8g2.drawStr(49, 24, "/");
    u8g2.setCursor(55, 24);
    u8g2.print( now.year());
    //u8g2.print( ndaysOfTheWeek[now.dayOfTheWeek()]);
    
    //u8g2.drawStr(58, 52, "Date:");
    u8g2.setCursor(82 , 24); 
    u8g2.print( now.hour());
    u8g2.drawStr(92, 24, ":"); 
    u8g2.setCursor(97, 24);
    u8g2.print( now.minute());
    u8g2.drawStr(108, 24, ":"); 
    u8g2.setCursor(113, 24);
    u8g2.print( now.second());

      float temperature, humidity;
  humidity = dht.readHumidity();
  temperature = dht.readTemperature(); 
// u8g2.clear();
 char tempF[6]; 
 char humF[6];
 dtostrf(temperature, 5, 1, tempF);
 dtostrf(humidity, 2, 0, humF);
 u8g2.drawStr(0, 41, " TEMP:");
// u8g2.setCursor(30, 41);
    if (dtostrf(temperature, 5, 1, tempF)){
      u8g2.drawStr(24, 41, tempF);
}
else{
    u8g2.drawStr(26, 41, NC);
}
 u8g2.drawStr(52, 41, "C");
 u8g2.drawStr(68, 41, "HUM: ");
 
 if (dtostrf(humidity, 2, 0, humF)){
    u8g2.drawStr(88, 41, humF);
 }
 else{
      u8g2.drawStr(85, 41, NC);
 }
  u8g2.drawStr(105, 41, "%");


}
uva();
  return proceed;
}
result alert(menuOut& o, idleEvent e) {
  if (e == idling) {
    o.setCursor(0, 0);
    o.print("alert test");
    o.setCursor(0, 1);
    o.print("press [select]");
    o.setCursor(0, 2);
    o.print("to continue...");
  }
  return proceed;
}

result home(menuOut& o, idleEvent e) {
  {
    //u8g2.setFont(fontName);
    //updateScreen();

  }
  return proceed;
}
//result Home(eventMask e, prompt &item) {
//  //nav.idleOn(Time);
//
//}
result goHome(eventMask e, prompt &item) {
  nav.idleOn(home);
  return proceed;
}
//result doAlert(eventMask e, prompt &item) {
//  nav.idleOn(alert);
//  return proceed;
//}

result info(eventMask e, prompt &item) {
  nav.idleOn(onioninfo);
  return proceed;
}
result Cinfo(eventMask e, prompt &item) {
  nav.idleOn(controllerinfo);
  return proceed;
}
result controllerinfo(eventMask e, navNode& nav, prompt &item) {
  DateTime now = rtc.now();
  {
    u8g2.drawFrame(0, 0, 128, 64);
//    u8g2.drawFrame(5, 5, 118, 54);
    u8g2.drawStr(7, 12, "  CHIP ID : OOCCC6C7");
     u8g2.drawStr(12, 25,"FLASH ID : 0016301C");
    u8g2.drawStr(5, 40,"MAC : 84:F3:EB:CC:C6:C7");
    u8g2.drawStr(5, 55,"MANUFACTURE YEAR : 2021");
//    u8g2.setCursor(40, 51);

  }
  return proceed;
}

result onioninfo(eventMask e, navNode& nav, prompt &item) {

  {

    u8g2.drawStr(45, 51, "none");
  }
  return proceed;
}

///***************Read all data from memoery******************************
void readalldata() {
 EepromReadZ1AP();
  EepromReadZ1PB();
  EepromReadZ1Air();
  EepromReadNT();
      
  return proceed;
}
//when menu is suspended
result idle(menuOut& o, idleEvent e) {
  o.clear();
  switch (e) {
    case idleStart: o.println("suspending menu!"); break;
    case idling: o.println("suspended..."); break;
    case idleEnd: o.println("resuming menu."); break;
  }
  return proceed;
}



void sensorjson(){
DynamicJsonDocument doc(500);

doc["CID"] = "ZEIRRC21001";
doc["Date/Time:"] = t;

JsonObject relaypos = doc.createNestedObject("Switch");
  relaypos["Pump A "] = 00; 
relaypos["PUMP B"] =00 ;
relaypos["AIR PUMP"] = 0;
relaypos["SOLENOID"] = 0;

serializeJsonPretty(doc, Serial3);
serializeJsonPretty(doc, Serial);
//
delay(100);
}
void dateandtime(void) {
  DateTime now = rtc.now();
  hourupg = now.hour();
  minupg = now.minute();
  secslive = now.second();
  Datelive = now.day();
  Monthlive = now.month();
  Yearlive = now.year();

}

long waterconlevel;
void getNutrientTL()
{
digitalWrite(trigPin, LOW);
delayMicroseconds (10);
digitalWrite(trigPin, HIGH);
delayMicroseconds (20);
digitalWrite(trigPin, LOW);


duration = pulseIn(echoPin, HIGH);
distanceincm = duration*0.034/2;
distanceinInch = distanceincm/2.54;
volume = pi*16*distanceincm; //change based on the volume for the tank
//waterHeight = tankHeight-distanceincm;
waterQuantity = totalVolume-volume-offset;
waterconlevel = tankHeight*2.54-waterHeight;

Serial.print("Tank is: ");
Serial.print(distanceincm);
Serial.println(" Cm");
Serial.print(distanceinInch);
//Serial.println(" Intch");
//Serial.print("Water level [cm]: ");
//Serial.println(waterHeight);
//Serial.print("Water Quantity [ml]: ");
//Serial.println(waterQuantity);

int temp;
 if(distanceincm<tanksetpoint && temp==0)
 {
     digitalWrite(SOLVALVE, LOW);
     Serial.println(" PUMP OFF");
     
     temp=1;
 }
 
  else if(distanceincm<tanksetpoint && temp==1)
 {
     digitalWrite(SOLVALVE, LOW);
     Serial.println(" PUMP OFF");
 }
 
 else if(distanceincm<tankHeight)
 {
   digitalWrite(SOLVALVE, HIGH);
   Serial.println(" PUMP ON");
   
   temp=0;
 }

 
//delay(100);
//if(distanceincm<=200 ) 
// {
//     digitalWrite(SOLVALVE, LOW);
//    // digitalWrite(BUZZER, HIGH);
//     
//     Serial.print("Water Tank Full Motor Turned off");
////     delay(1000);
//    // digitalWrite(BUZZER, LOW);
////     delay(3000);
//     Wtemp=1;
// }
// 
//   if(distanceincm>=100 )
// { 
//     digitalWrite(SOLVALVE, HIGH); 
//     Serial.println("Water Tank Full Motor Turned ON");
////     delay(2000);
// }
 
// else if(distanceincm=<tankHeight)
// {
//   digitalWrite(SOLVALVE, HIGH);
//   Serial.println("LOW Water Level Motor Turned ON");
//   //delay(2000);
//   Wtemp=0;
// }
 return distance;

////delay(100);
//if(distanceincm==waterconlevel && duration==0)
// {
//     digitalWrite(SOLVALVE, LOW);
//    // digitalWrite(BUZZER, HIGH);
//     
//     Serial.print("Water Tank Full Motor Turned OFF");
////     delay(1000);
//    // digitalWrite(BUZZER, LOW);
//    // delay(3000);
//     Wtemp=1;
// }
// 
//  else if(distance<12 && duration==1)
// { 
//     digitalWrite(SOLVALVE, LOW); 
//     Serial.println("Water Tank Full Motor Turned OFF ");
////     delay(2000);
// }
// 
// else if(distance=<tankHeight)
// {
//   digitalWrite(SOLVALVE, HIGH);
//   Serial.println("LOW Water Level Motor Turned ON");
//   //delay(2000);
//   Wtemp=0;
// }
// return distance;
 
}
//************************************DHT11*****************************

void getDht11(){
  
  float temperature, humidity;
  humidity = dht.readHumidity();
  temperature = dht.readTemperature(); 

 char tempF[6]; 
 char humF[6];
 dtostrf(temperature, 5, 1, tempF);
 dtostrf(humidity, 2, 0, humF);

 Serial.print("T:");
 Serial.print(tempF);
 Serial.print((char)223);
 Serial.print("C ");
 Serial.println("H: ");
 Serial.print(humF);  
 Serial.println("%");
 }


//***********************************************************************


void setup() {
  Serial.begin(115200);
  pinMode(LEDPIN, OUTPUT);
  pinMode(led, OUTPUT);
   pinMode(led1, OUTPUT);
    pinMode(led2, OUTPUT);
    pinMode(led3, OUTPUT);
   while(!Serial);
  Serial.println("menu 4.x test");Serial.flush(); 
  Serial3.begin(115200);
  while (!Serial);
  Serial.println("ZEEPTOGREENS...........");
  Serial.flush();
Wire.begin();
    u8g2.begin();
  dht.begin();
   pinMode(A3 , OUTPUT);
  //WD reset
 // wdt_enable(WDTO_8S);
 
   
   
   DateTime now = rtc.now();
  rtc.begin();
   rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
//  sprintf(t, "%02d:%02d:%02d %02d/%02d/%02d",  now.hour(), now.minute(), now.second(), now.day(), now.month(), now.year());
//  Serial.print(F("Date/Time: "));
//  Serial.println(t);

  
  //mainMenu[0].enabled=disabledStatus;

  
  //change input burst for slow output devices
  //this is the number of max. processed inputs before drawing
  //nav.inputBurst=10;

  //BOOT
  nav.showTitle = true;

//  u8g2.drawStr(0, 0, "WELCOME TO ZEEPTOGREENS");

  u8g2.drawStr(10, 10, "BOOTING.....");
//  Timer1.initialize(1000);
// / Timer1.attachInterrupt(timerIsr);
//  SPI.begin();
//  u8g2.begin();
  //  noScreenSetup();

  readalldata();
  //TASK
  runner.init();
  Serial.println("Initialized scheduler");
  
  runner.addTask(t1);
  Serial.println("added t1");
  t1.enable();


//  pinMode(encBtn, INPUT_PULLUP);

  pinMode(BUZZER, OUTPUT);

  //***Indicator
  pinMode(LEDRED, OUTPUT);
  pinMode(LEDGREEN, OUTPUT);
  pinMode(LEDBLUE, OUTPUT);

  pinMode(PUMPA, OUTPUT);
  pinMode(PUMPB, OUTPUT);
  pinMode(AIRPUMP, OUTPUT);
  pinMode(SOLVALVE, OUTPUT);
  digitalWrite(PUMPA,LOW);
  digitalWrite(PUMPB,LOW);
  digitalWrite(AIRPUMP,LOW);
  digitalWrite(SOLVALVE,LOW);

  pinMode(UVOUT, INPUT);
  pinMode(REF_3V3, INPUT);

  ///Waterlevel 
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);


    nav.idleTask = Home;
  //***********COUNTER ****************************************
   PUMPAON.setCounter(Z1PASThr, Z1PASTmin, Z1PASTmin, PUMPAON.COUNT_UP, PUMPAONComplete);
   PUMPAON.setInterval(print_timePUMPA1, 1000);
   PUMPAOFF.setCounter(Z1PASPhr, Z1PASPmin, Z1PASPmin, PUMPAOFF.COUNT_DOWN, PUMPAOFFComplete);
   PUMPAOFF.setInterval(print_timePUMPA2, 1000);
// PUMPANone.setInterval(print_nonePUMPA, 2000);
// 
   PUMPBON.setCounter(Z1PBSThr, Z1PBSTmin, Z1PBSTmin, PUMPBON.COUNT_UP, PUMPBONComplete);
   PUMPBON.setInterval(print_timePUMPB1, 1000);
   PUMPBOFF.setCounter(Z1PBSPhr, Z1PBSPmin, Z1PBSPmin, PUMPBOFF.COUNT_DOWN, PUMPBOFFComplete);
   PUMPBOFF.setInterval(print_timePUMPB2, 1000);
 //PUMPBNone.setInterval(print_nonePUMPB, 2000);
//
   AIRSTONEON.setCounter(Z1APSThr, Z1APSTmin, Z1APSTmin, AIRSTONEON.COUNT_UP, AIRSTONEONComplete);
   AIRSTONEON.setInterval(print_timeAIRSTONE1, 1000);
   AIRSTONEOFF.setCounter(Z1APSPhr, Z1APSPmin, Z1APSPmin, AIRSTONEOFF.COUNT_DOWN, AIRSTONEOFFComplete);
   AIRSTONEOFF.setInterval(print_timeAIRSTONE2, 1000);
//   AIRSTONEone.setInterval(print_noneAIRSTONE, 2000);

   WLmenu[0].enabled=disabledStatus;
   tanksetting[1].enabled=disabledStatus;
   WLmenu[2].enabled=disabledStatus;
  //u8g.setFont(u8g_font_helvR08);
  u8g2.setFont(fontName);
  //u8g.setFont(u8g_font_04b_03r);
//  u8g2.firstPage();
  do {
    //    u8g2.setColorIndex(1);
    //    nav.out[0].setCursor(0,0);
    //    u8g2.drawStr( 2+x, 10, "RRD GLCD TEST");
//    nav.out[0].setCursor(0, 2);
//    nav.out[0].print(F("Welcome To ZEEPTOGREENS"));
    nav.out[0].setCursor(0, 3);
    nav.out[0].print(F("BOOTING..."));

  } while (u8g2.nextPage());
  delay(2000);
    // No counter
    // Just call print_none() method every 2s.
//  tNone.setInterval(print_none, 2000);
}


void loop() { 
    
//   getNutrientTL();
   runner.execute();
  //  testfun();
//  wdt_reset();// make sure this gets called at least once every 8 seconds!
if (nav.changed(0)) {//only draw if menu changed for gfx device
    //change checking leaves more time for other tasks
    u8g2.firstPage();
    do nav.doOutput(); while(u8g2.nextPage());
  }
  DateTime now = rtc.now();
//  rtc.begin();
  rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    sprintf(t, "%02d:%02d:%02d %02d/%02d/%02d",  now.hour(), now.minute(), now.second(), now.day(), now.month(), now.year());
    Serial.print(F("Date/Time: "));
    Serial.println(t);
  ///Switching funtion

digitalWrite(led, HIGH);


//digitalWrite(led1, HIGH);

  digitalWrite(LEDPIN, ledCtrl);
  digitalWrite (BUZZER, ledCtrl);
  digitalWrite (LEDRED, ledRED);
  digitalWrite (LEDGREEN, ledGREEN);
  digitalWrite (LEDBLUE, ledBLUE);
if(Autocnt==true){
  (Autocntt==false);
  (Autocnttt==false);
  digitalWrite(led3, HIGH);
  digitalWrite(led1, LOW);
  digitalWrite(led2, LOW);
    PUMPAON.run();
    PUMPAOFF.run();
    PUMPANone.run();

    PUMPBON.run();
    PUMPBOFF.run();
    PUMPBNone.run();

    AIRSTONEON.run();
    AIRSTONEOFF.run();
    AIRSTONEone.run(); 
}
if(Autocnt==false){
  digitalWrite(led3, LOW);

  digitalWrite (PUMPA, relayPA);
  digitalWrite (PUMPB, relayPB);
  digitalWrite (AIRPUMP, relayAir);
  digitalWrite (SOLVALVE, relaySol);
    }
  if (Autocnttt==true) {
    (Autocntt==false);
  (Autocnt==false);
    digitalWrite(led2, HIGH);
    digitalWrite(led1, LOW);
    digitalWrite(led3, LOW);
  } else {
   digitalWrite(led2, LOW);
  }

    if (Autocntt==true) {
       (Autocnt==false);
       (Autocnttt==false);
    digitalWrite(led1, HIGH);
    digitalWrite(led2, LOW);
    digitalWrite(led3, LOW );
  } else {
   digitalWrite(led1, LOW);
  }

//     }
 // sensorjson();
  
  nav.doInput();
  //nav.poll();
  if (nav.changed(1)) {//only draw if menu changed for gfx device
    //because this code clears the screen, if always called then screen will blink
//    u8g2.firstPage();
//    do nav.doOutput(); while (u8g2.nextPage());
  }
//  (100);//simulate other tasks 
}


///**********Counter Timer****************
void print_timePUMPA1()
{
  digitalWrite(PUMPA,HIGH);
  Serial.print("PUMP A ON TIME: ");
  Serial.println(PUMPAON.getCurrentTime()); 
}
void print_timePUMPA2()
{
  PUMPAON.pause();
 digitalWrite(PUMPA,LOW);
Serial.print("PUMP A OFF TIME: ");
Serial.println(PUMPAOFF.getCurrentTime());
}
void PUMPAONComplete()
{
  unsigned long currentMillis = millis();
 if (currentMillis - previousMillis >= interval) { 
  previousMillis = currentMillis;
//PUMPAOFF.restart();
PUMPAOFF.start();
}
}
void PUMPAOFFComplete()
{
  unsigned long currentMillis = millis();
 if (currentMillis - previousMillis >= interval) { 
  previousMillis = currentMillis;
PUMPAOFF.stop(); 
//PUMPAON.restart();
PUMPAON.start();
}
}
////**********************PUMP B**********
void print_timePUMPB1()
{
digitalWrite(PUMPB,HIGH);  
Serial.print("PUMP B ON TIME: ");
Serial.println(PUMPAON.getCurrentTime());
}
void print_timePUMPB2()
{
  digitalWrite(PUMPB, LOW); 
Serial.print("PUMP B OFF TIME: ");
Serial.println(PUMPAOFF.getCurrentTime());
}
void PUMPBONComplete()
{
//PUMPBON.stop();
//PUMPBOFF.restart();
PUMPBOFF.start();
}
void PUMPBOFFComplete()
{
PUMPBOFF.stop();
//PUMPBON.restart();
PUMPBON.start();
}
//////****AIR STONE PUMP**********
//char airon;
void print_timeAIRSTONE1()
{
 digitalWrite(AIRPUMP,HIGH);
Serial.print("AIRSTONE ON TIME: ");
Serial.println(AIRSTONEON.getCurrentTime());
////  airon =AIRSTONEON.getCurrentTime();
}
void print_timeAIRSTONE2()
{
  digitalWrite(AIRPUMP, LOW);
Serial.print("AIRSTONE OFF TIME: ");
Serial.println(AIRSTONEOFF.getCurrentTime());
}
void AIRSTONEONComplete()
{
 AIRSTONEON.stop();
 //AIRSTONEOFF.restart();
 AIRSTONEOFF.start();
}
void AIRSTONEOFFComplete()
{
 AIRSTONEOFF.stop();
 //AIRSTONEON.restart();
 AIRSTONEON.start();
}
//****************************************************************

void uva()
  {
  int uvLevel = averageAnalogRead(UVOUT);
  int refLevel = averageAnalogRead(REF_3V3);
  
  //Use the 3.3V power pin as a reference to get a very accurate output value from sensor
  float outputVoltage = 3.3 / refLevel * uvLevel;
  
  float uvIntensity = mapfloat(outputVoltage, 0.0, 31.0, 0.0, 99.0)*10; //Convert the voltage to a UV intensity level
 
  Serial.print("output: ");
  Serial.print(refLevel);
 
  Serial.print("ML8511 output: ");
  Serial.print(uvLevel);
 
  Serial.print(" / ML8511 voltage: ");
  Serial.print(outputVoltage);
 
  Serial.print(" / UV Intensity (mW/cm^2): ");
//  Serial.print(uvIntensity);
// u8g2.drawStr(1, 24, "D/T:");
//    u8g2.setCursor(24, 24);
//    u8g2.print( now.day());
// lcd.setCursor(2, 2);
 u8g2.drawStr(1, 58, " RADATION :");
u8g2.setCursor(58, 58);
 u8g2.print(uvIntensity);
u8g2.setCursor(84, 58);
 u8g2.print(" mW/cm^2");
  
  Serial.println();
  
//  delay(3000);
}
 
//Takes an average of readings on a given pin
//Returns the average
int averageAnalogRead(int pinToRead)
{
  byte numberOfReadings = 8;
  unsigned int runningValue = 0; 
 
  for(int x = 0 ; x < numberOfReadings ; x++)
    runningValue += analogRead(pinToRead);
  runningValue /= numberOfReadings;
 
  return(runningValue);
}
 
float mapfloat(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
