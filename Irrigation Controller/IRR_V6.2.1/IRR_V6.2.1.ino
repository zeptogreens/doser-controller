/*/
 * irrigation Arduino MEGA with ESP8266 Board Code... 
 * Serial communication to esp8266
 * ArduinoJson
 * Auto Save
 * Mode Save
 * Audo Sheduler delay
 
 * 
 */


#include <Arduino.h>
#include <menu.h>
#include <menuIO/keypadIn.h>
#include <menuIO/serialOut.h>
#include <menuIO/u8g2Out.h>

#include <menuIO/chainStream.h>
#include <menuIO/serialOut.h>
#include <menuIO/serialIn.h>
#include <menuIO/U8GLibOut.h>

#include <TimerOne.h>
#include <ClickEncoder.h>
//Data
#include <ArduinoJson.h>
#include <TaskScheduler.h>

#include <MemoryFree.h>
#include <SD.h>
#include <TaskScheduler.h>
#include <ArduinoJson.h>
#include "DHT.h"
#include <RTClib.h>
#include <U8g2lib.h>

#include <RTClib.h>
#include <Time.h>
#include <TimeLib.h>
char t[32];
#include "Countimer.h"
#include "Timer.h"
Timer timer;

////Hardware pin definitions
int UVOUT = A2; //Output from the sensor
int REF_3V3 = A1; //3.3V power on the Arduino board

#define DHTPIN A0  // Digital pin connected to the DHT sensor

#define DHTTYPE DHT11   // DHT 11

DHT dht(DHTPIN, DHTTYPE);
using namespace Menu;

#define LEDPIN LED_BUILTIN
//#define USE_SSD1306

//EPROM
#include <EEPROMex.h>

//WatchDogTimer
//#include <avr/wdt.h>

//Define
#define BUZZER 37
#define LEDRED 42
#define LEDGREEN 44
#define LEDBLUE 58

//RELAY
#define PUMPA 51
#define PUMPB 50
#define AIRPUMP  49 //14
#define SOLVALVE 48 //15
int FirstState = 0;
int SecondState = 0;
int ThirdState = 0;
int MANUAL = 0;
int NC = 0;

int led = 0;
int led1 = 2;
int led2 = 3;
int led3 = 4;

unsigned long previousMillis = 0; 
const long interval = 1000;  

Countimer PUMPAON;
Countimer PUMPAOFF;
Countimer PUMPANone;

Countimer PUMPBON;
Countimer PUMPBOFF;
Countimer PUMPBNone;

Countimer AIRSTONEON;
Countimer AIRSTONEOFF;
Countimer AIRSTONEone;

int nhur=436,nmin=439;
int nhur1,nmin1;
int ledRED = LOW;
int ledGREEN = LOW;
int ledBLUE = LOW;

#define DEBUG 1                                // change value to 1 to enable debuging using serial monitor  
#define SERIAL_PORT_SPEED 115200                //9600, 19200, 38400, 57600, 115200

//****************************************** Water Level US Sensor **************************************************************
#define RORelay 19

const int trigPin = 52;
const int echoPin = 53;
long duration;
long distance;
float waterHeight;
int tankHeight ; //enter your water tank height here in [cm]
int totalVolume ; // volume here is in [cm^3], change depending on tank volume
float waterQuantity;
float volume;
float pi = 3.14159265359;
float offset = 245;
//Water distance
float Wtime=0,Wdistance=0;
int Wtemp=0;
bool TankM;
int tanksetpoint;

//***********************************************************************************************************************
Scheduler r;
//Tasks
void getNutrientTL();
void sensorjson();
Task t1(3000, TASK_FOREVER, &getNutrientTL);
//Task t3(5000, TASK_FOREVER, &t3Callback);

Task t4(5000,  TASK_FOREVER, &sensorjson, &r, true);


//********************************** ENCODER


const byte ROWS = 4; //four rows
const byte COLS = 4; //four columns
//define the cymbols on the buttons of the keypads
char hexaKeys[ROWS][COLS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','-'},
  {'/','0','*','+'},
};
byte rowPins[ROWS] = {26, 27, 28, 29}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {30, 31, 32, 33}; //connect to the column pinouts of the keypad

//initialize an instance of class NewKeypad
Keypad customKeypad( makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS);


//********************Display Configuration **************************
//#include <SPI.h>
//#define USE_SWSPI
//#define U8_DC 17
//#define U8_CS 16
//#define U8_RST 23
//#define fontName  u8g2_font_5x7_tf // u8g2_font_synchronizer_nbp_tf           
////  #define fontX 5
////  #define fontY 9
//#define offsetX 0
//#define offsetY 0
//#define U8_Width 128
//#define U8_Height 64
//
////#include <Arduino.h>
////#include <U8g2lib.h>
////
////#ifdef U8X8_HAVE_HW_SPI
////#include <SPI.h>
////#endif
////#ifdef U8X8_HAVE_HW_I2C
////#include <Wire.h>
////#endif
//
//int x = 0;
//int scroll_direction = 1;    
#include <Wire.h>
  #define fontName u8g2_font_5x7_tf// u8g2_font_blipfest_07_tr  
#define fontX 5
#define fontY 8
#define offsetX 0
#define offsetY 0
#define U8_Width 128
#define U8_Height 64
#define USE_HWI2C

U8G2_KS0108_128X64_F u8g2(U8G2_R0, 37, 38, 39, 40, 41, 42, 43, 44, /*enable=*/36, /*dc=*/ 34, /*cs0=*/ 45, /*cs1=*/ 46, /*cs2=*/ 35, /* reset=*/  47);   // Set R/W to low!//Direction of title scroll, 1 right, -1 left//Offset postion of title

#define LEDPIN 13

//************SD Card ********///
#define SD_DETECT_PIN   49
#define SDSS            53
File sdcard_file;
    

RTC_DS3231 rtc; //for date time
char daysOfTheWeek[7][12] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};  

int hourupg;
int minupg;
uint8_t secslive;
int Monthlive;
int Datelive;
int Yearlive;


// define menu colors --------------------------------------------------------
//each color is in the format:
//  {{disabled normal,disabled selected},{enabled normal,enabled selected, enabled editing}}
// this is a monochromatic color table
const colorDef<uint8_t> colors[6] MEMMODE = {
  {{0, 0}, {0, 1, 1}}, //bgColor
  {{1, 1}, {1, 0, 0}}, //fgColor
  {{1, 1}, {1, 0, 0}}, //valColor
  {{1, 1}, {1, 0, 0}}, //unitColor
  {{0, 1}, {0, 0, 1}}, //cursorColor
  {{1, 1}, {1, 0, 0}}, //titleColor
};

result doAlert(eventMask e, prompt &item);

result showEvent(eventMask e, navNode& nav, prompt& item) {
  Serial.print("event: ");
  Serial.println(e);
  return proceed;
}


result action1(eventMask e, navNode& nav, prompt &item) {
  Serial.print(e);
  Serial.println(" action1 executed, proceed menu");
  Serial.flush();
  return proceed;
}

result action2(eventMask e, navNode& nav, prompt &item) {
  Serial.print(e);
  Serial.print(" action2 executed, quiting menu");
  return quit;
}

result home(eventMask e, navNode& nav, prompt &item) {
  Serial.print(e);
  Serial.print(" home");
  return quit;
}


int ledCtrl = LOW;

result myLedOn() {
  ledCtrl = HIGH;
  digitalWrite (BUZZER, HIGH);
  return proceed;
}
result myLedOff() {
  
  ledCtrl = LOW;
  digitalWrite (BUZZER, LOW);
  return proceed;
}
//*******************************TANK**********************************
TOGGLE(TankM,Tankcontrol,"TANK MANAGEMENT : ",doNothing,noEvent,noStyle//,doExit,enterEvent,noStyle
  ,VALUE("STOP",true,doNothing,enterEvent)
  ,VALUE("START",false,doExit,noEvent)
);

result TankMgStart() {
  TankM = true;
  return proceed;
}
result TankMgStop() {
  TankM = false;
  return proceed;
}
///**********************Relay Control******************************

bool relayPA ;
bool relayPB ;
bool relayAir ;
bool relaySol ;

TOGGLE(relayPA, pumpa, "     PUMP A : ", doNothing, noEvent, noStyle
       , VALUE("ON", HIGH, doNothing, noEvent)
       , VALUE("OFF", LOW, doNothing, noEvent)
      );


TOGGLE(relayPB, pumpb, "     PUMP B : ", doNothing, noEvent, noStyle
       , VALUE("ON", HIGH, doNothing, noEvent)
       , VALUE("OFF", LOW, doNothing, noEvent)
      );
      
TOGGLE(relayAir, air, "     DO PUMP : ", doNothing, noEvent, noStyle
       , VALUE("ON", HIGH, doNothing, noEvent)
       , VALUE("OFF", LOW, doNothing, noEvent)
      );

TOGGLE(relaySol, sol, "     SOLENOID : ", doNothing, noEvent, noStyle
       , VALUE("ON", HIGH, doNothing, noEvent)
       , VALUE("OFF", LOW, doNothing, noEvent)
      );
      
void switching(){
  
  if(relayPA==HIGH ){
    
    relaySol==LOW;
    }
    else 
    {
      relaySol==HIGH;
      };
  if(relayPB==HIGH) {
    relaySol==LOW;
    
    }
    else{
    
      relaySol==HIGH;
      }
  
  }
//******************************************************************
  bool Autocnttt;
TOGGLE(Autocnttt, Manuall, "MANUAL : ", doNothing, noEvent, noStyle
       , VALUE("ON", true,  doNothing, enterEvent)
       , VALUE("OFF",false, doNothing, enterEvent)
      );
MENU(MANmenu, " MANUAL MODE", showEvent, enterEvent, noStyle
 ,SUBMENU(Manuall)
//, OP(" ACTUATOR'S", doNothing, noEvent)     
     , SUBMENU(pumpa)
     , SUBMENU(pumpb)
     , SUBMENU(air)
     , SUBMENU(sol)
, EXIT("<BACK")
    );      

///****************************************************************    
//customizing a prompt look!
//by extending the prompt class
class altPrompt:public prompt {
public:
  altPrompt(constMEM promptShadow& p):prompt(p) {}
  Used printTo(navRoot &root,bool sel,menuOut& out, idx_t idx,idx_t len,idx_t) override {
    return out.printRaw(F("special prompt!"),len);;
  }
};
//*****************************ZONE ONE AUTO ************************************
int Z1PASThr;
int Z1PASTmin;
int Z1PASPhr;
int Z1PASPmin;

int Z1ZPASThr;
int Z1ZPASTmin;
int Z1ZPASPhr;
int Z1ZPASPmin;


int Z1PBSThr;
int Z1PBSTmin;
int Z1PBSPhr;
int Z1PBSPmin;

int Z1ZPBSThr;
int Z1ZPBSTmin;
int Z1ZPBSPhr;
int Z1ZPBSPmin;

int Z1APSThr;
int Z1APSTmin;
int Z1APSPhr;
int Z1APSPmin;

int Z1ZAPSThr;
int Z1ZAPSTmin;
int Z1ZAPSPhr;
int Z1ZAPSPmin;

int Z2PASThr;
int Z2PASTmin;
int Z2PASPhr;
int Z2PASPmin;

int Z2ZPASThr;
int Z2ZPASTmin;
int Z2ZPASPhr;
int Z2ZPASPmin;


int Z2PBSThr;
int Z2PBSTmin;
int Z2PBSPhr;
int Z2PBSPmin;

int Z2ZPBSThr;
int Z2ZPBSTmin;
int Z2ZPBSPhr;
int Z2ZPBSPmin;

int Z2APSThr;
int Z2APSTmin;
int Z2APSPhr;
int Z2APSPmin;

int Z2ZAPSThr;
int Z2ZAPSTmin;
int Z2ZAPSPhr;
int Z2ZAPSPmin;

int Z3PASThr;
int Z3PASTmin;
int Z3PASPhr;
int Z3PASPmin;

int Z3ZPASThr;
int Z3ZPASTmin;
int Z3ZPASPhr;
int Z3ZPASPmin;


int Z3PBSThr;
int Z3PBSTmin;
int Z3PBSPhr;
int Z3PBSPmin;

int Z3ZPBSThr;
int Z3ZPBSTmin;
int Z3ZPBSPhr;
int Z3ZPBSPmin;

int Z3APSThr;
int Z3APSTmin;
int Z3APSPhr;
int Z3APSPmin;

int Z3ZAPSThr;
int Z3ZAPSTmin;
int Z3ZAPSPhr;
int Z3ZAPSPmin;

int Z4PASThr;
int Z4PASTmin;
int Z4PASPhr;
int Z4PASPmin;

int Z4ZPASThr;
int Z4ZPASTmin;
int Z4ZPASPhr;
int Z4ZPASPmin;


int Z4PBSThr;
int Z4PBSTmin;
int Z4PBSPhr;
int Z4PBSPmin;

int Z4ZPBSThr;
int Z4ZPBSTmin;
int Z4ZPBSPhr;
int Z4ZPBSPmin;

int Z4APSThr;
int Z4APSTmin;
int Z4APSPhr;
int Z4APSPmin;

int Z4ZAPSThr;
int Z4ZAPSTmin;
int Z4ZAPSPhr;
int Z4ZAPSPmin;

//////////////////////////////////////////////////////////////////////////////////////////////////
int EepromZ1PASThr = 200;
int EepromZ1PASTmin =202;
int EepromZ1PASPhr =204;
int EepromZ1PASPmin= 206;

int EepromZ1ZPASThr = 300;
int EepromZ1ZPASTmin =302;
int EepromZ1ZPASPhr =304;
int EepromZ1ZPASPmin= 306;


int EepromZ1PBSThr = 208;
int EepromZ1PBSTmin =210;
int EepromZ1PBSPhr =212;
int EepromZ1PBSPmin= 214;

int EepromZ1ZPBSThr = 308;
int EepromZ1ZPBSTmin =310;
int EepromZ1ZPBSPhr =312;
int EepromZ1ZPBSPmin= 314;


int EepromZ1APSThr = 216;
int EepromZ1APSTmin =214;
int EepromZ1APSPhr =218;
int EepromZ1APSPmin= 220;

int EepromZ1ZAPSThr = 316;
int EepromZ1ZAPSTmin =314;
int EepromZ1ZAPSPhr =318;
int EepromZ1ZAPSPmin= 320;

int EepromZ2PASThr = 340;
int EepromZ2PASTmin =342;
int EepromZ2PASPhr =344;
int EepromZ2PASPmin=346;

int EepromZ2ZPASThr = 348;
int EepromZ2ZPASTmin =350;
int EepromZ2ZPASPhr =352;
int EepromZ2ZPASPmin= 354;


int EepromZ2PBSThr = 356;
int EepromZ2PBSTmin =358;
int EepromZ2PBSPhr =360;
int EepromZ2PBSPmin= 362;

int EepromZ2ZPBSThr = 364;
int EepromZ2ZPBSTmin =366;
int EepromZ2ZPBSPhr =368;
int EepromZ2ZPBSPmin= 370;


int EepromZ2APSThr = 372;
int EepromZ2APSTmin =374;
int EepromZ2APSPhr =376;
int EepromZ2APSPmin= 378;

int EepromZ2ZAPSThr = 380;
int EepromZ2ZAPSTmin =382;
int EepromZ2ZAPSPhr =384;
int EepromZ2ZAPSPmin= 386;


int EepromZ3PASThr = 396;
int EepromZ3PASTmin =398;
int EepromZ3PASPhr =400;
int EepromZ3PASPmin= 402;


int EepromZ3ZPASThr = 484;
int EepromZ3ZPASTmin =490;
int EepromZ3ZPASPhr =492;
int EepromZ3ZPASPmin= 494;

int EepromZ3PBSThr = 404;
int EepromZ3PBSTmin =406;
int EepromZ3PBSPhr =408;
int EepromZ3PBSPmin= 410;

int EepromZ3ZPBSThr =412;
int EepromZ3ZPBSTmin =414;
int EepromZ3ZPBSPhr =416;
int EepromZ3ZPBSPmin= 418;


int EepromZ3APSThr = 420;
int EepromZ3APSTmin =422;
int EepromZ3APSPhr =424;
int EepromZ3APSPmin= 426;

int EepromZ3ZAPSThr = 428;
int EepromZ3ZAPSTmin =430;
int EepromZ3ZAPSPhr =432;
int EepromZ3ZAPSPmin= 434;



int EepromZ4PASThr = 436;
int EepromZ4PASTmin =438;
int EepromZ4PASPhr =440;
int EepromZ4PASPmin=442;

int EepromZ4ZPASThr = 444;
int EepromZ4ZPASTmin =446;
int EepromZ4ZPASPhr =448;
int EepromZ4ZPASPmin= 450;


int EepromZ4PBSThr = 452;
int EepromZ4PBSTmin =454;
int EepromZ4PBSPhr =456;
int EepromZ4PBSPmin= 458;

int EepromZ4ZPBSThr = 460;
int EepromZ4ZPBSTmin = 462;
int EepromZ4ZPBSPhr = 464;
int EepromZ4ZPBSPmin= 466;


int EepromZ4APSThr = 468;
int EepromZ4APSTmin = 470;
int EepromZ4APSPhr = 472;
int EepromZ4APSPmin= 474;

int EepromZ4ZAPSThr = 476;
int EepromZ4ZAPSTmin =478;
int EepromZ4ZAPSPhr = 480;
int EepromZ4ZAPSPmin= 482;

char PUMPAONtime;
char PUMPAOFFtime;
//***SAVE TIME********************************************************///
result Z1PAsavetime() {
  EEPROM.write(EepromZ1PASThr, Z1PASThr);
  EEPROM.write(EepromZ1PASTmin, Z1PASTmin);

  return proceed;
}
result Z1PAsavedue() {

  EEPROM.write(EepromZ1PASPhr, Z1PASPhr);
  EEPROM.write(EepromZ1PASPmin, Z1PASPmin);
  return proceed;
}
void EepromReadZ1AP()
{
  Z1PASThr = EEPROM.read(EepromZ1PASThr);
  Z1PASTmin = EEPROM.read(EepromZ1PASTmin);
  Z1PASPhr = EEPROM.read(EepromZ1PASPhr);
  Z1PASPmin = EEPROM.read(EepromZ1PASPmin);
  return proceed;
}
//*****************************************************************
result Z1ZPAsavetime() {
  EEPROM.write(EepromZ1ZPASThr, Z1ZPASThr);
  EEPROM.write(EepromZ1ZPASTmin, Z1ZPASTmin);

  return proceed;
}
result Z1ZPAsavedue() {

  EEPROM.write(EepromZ1ZPASPhr, Z1ZPASPhr);
  EEPROM.write(EepromZ1ZPASPmin, Z1ZPASPmin);
  return proceed;
}
void EepromReadZ1ZAP()
{
  Z1ZPASThr = EEPROM.read(EepromZ1ZPASThr);
  Z1ZPASTmin = EEPROM.read(EepromZ1ZPASTmin);
  Z1ZPASPhr = EEPROM.read(EepromZ1ZPASPhr);
  Z1ZPASPmin = EEPROM.read(EepromZ1ZPASPmin);
  return proceed;
}  
//**********************************************************************************
result Z1PBsavetime() {
  EEPROM.write(EepromZ1PBSThr, Z1PBSThr);
  EEPROM.write(EepromZ1PBSTmin, Z1PBSTmin);

  return proceed;
}
result Z1PBsavedue() {

  EEPROM.write(EepromZ1PBSPhr, Z1PBSPhr);
  EEPROM.write(EepromZ1PBSPmin, Z1PBSPmin);
  return proceed;
}
void EepromReadZ1PB()
{
  Z1PBSThr = EEPROM.read(EepromZ1PBSThr);
  Z1PBSTmin = EEPROM.read(EepromZ1PBSTmin);
  Z1PBSPhr = EEPROM.read(EepromZ1PBSPhr);
  Z1PBSPmin = EEPROM.read(EepromZ1PBSPmin);
  return proceed;
}
//*************************************************************************
result Z1ZPBsavetime() {
  EEPROM.write(EepromZ1ZPBSThr, Z1ZPBSThr);
  EEPROM.write(EepromZ1ZPBSTmin, Z1ZPBSTmin);

  return proceed;
}
result Z1ZPBsavedue() {

  EEPROM.write(EepromZ1ZPBSPhr, Z1ZPBSPhr);
  EEPROM.write(EepromZ1ZPBSPmin, Z1ZPBSPmin);
  return proceed;
}
void EepromReadZ1ZPB()
{
  Z1ZPBSThr = EEPROM.read(EepromZ1ZPBSThr);
  Z1ZPBSTmin = EEPROM.read(EepromZ1ZPBSTmin);
  Z1ZPBSPhr = EEPROM.read(EepromZ1ZPBSPhr);
  Z1ZPBSPmin = EEPROM.read(EepromZ1ZPBSPmin);
  return proceed;
}
//*****************************************************************
result Z1APsavetime() {
  EEPROM.write(EepromZ1APSThr, Z1APSThr);
  EEPROM.write(EepromZ1APSTmin, Z1APSTmin);

  return proceed;
}
result Z1APsavedue() {

  EEPROM.write(EepromZ1APSPhr, Z1APSPhr);
  EEPROM.write(EepromZ1APSPmin, Z1APSPmin);
  return proceed;
}
void EepromReadZ1Air()
{
  Z1APSThr = EEPROM.read(EepromZ1APSThr);
  Z1APSTmin = EEPROM.read(EepromZ1APSTmin);
  Z1APSPhr = EEPROM.read(EepromZ1APSPhr);
  Z1APSPmin = EEPROM.read(EepromZ1APSPmin);
  return proceed;
}
//*****************************************************************************
result Z1ZAPsavetime() {
  EEPROM.write(EepromZ1ZAPSThr, Z1ZAPSThr);
  EEPROM.write(EepromZ1ZAPSTmin, Z1ZAPSTmin);

  return proceed;
}
result Z1ZAPsavedue() {

  EEPROM.write(EepromZ1ZAPSPhr, Z1ZAPSPhr);
  EEPROM.write(EepromZ1ZAPSPmin, Z1ZAPSPmin);
  return proceed;
}
void EepromReadZ1ZAir()
{
  Z1ZAPSThr = EEPROM.read(EepromZ1ZAPSThr);
  Z1ZAPSTmin = EEPROM.read(EepromZ1ZAPSTmin);
  Z1ZAPSPhr = EEPROM.read(EepromZ1ZAPSPhr);
  Z1ZAPSPmin = EEPROM.read(EepromZ1ZAPSPmin);
  return proceed;
}

///************************************************************************************************
result Z2PAsavetime() {
  EEPROM.write(EepromZ2PASThr, Z2PASThr);
  EEPROM.write(EepromZ2PASTmin, Z2PASTmin);

  return proceed;
}
result Z2PAsavedue() {

  EEPROM.write(EepromZ2PASPhr, Z2PASPhr);
  EEPROM.write(EepromZ2PASPmin, Z2PASPmin);
  return proceed;
}
void EepromReadZ2AP()
{
  Z2PASThr = EEPROM.read(EepromZ2PASThr);
  Z2PASTmin = EEPROM.read(EepromZ2PASTmin);
  Z2PASPhr = EEPROM.read(EepromZ2PASPhr);
  Z2PASPmin = EEPROM.read(EepromZ2PASPmin);
  return proceed;
}
//*****************************************************************
result Z2ZPAsavetime() {
  EEPROM.write(EepromZ2ZPASThr, Z2ZPASThr);
  EEPROM.write(EepromZ2ZPASTmin, Z2ZPASTmin);

  return proceed;
}
result Z2ZPAsavedue() {

  EEPROM.write(EepromZ2ZPASPhr, Z2ZPASPhr);
  EEPROM.write(EepromZ2ZPASPmin, Z2ZPASPmin);
  return proceed;
}
void EepromReadZ2ZAP()
{
  Z2ZPASThr = EEPROM.read(EepromZ2ZPASThr);
  Z2ZPASTmin = EEPROM.read(EepromZ2ZPASTmin);
  Z2ZPASPhr = EEPROM.read(EepromZ2ZPASPhr);
  Z2ZPASPmin = EEPROM.read(EepromZ2ZPASPmin);
  return proceed;
}  
//**********************************************************************************
result Z2PBsavetime() {
  EEPROM.write(EepromZ2PBSThr, Z2PBSThr);
  EEPROM.write(EepromZ2PBSTmin, Z2PBSTmin);

  return proceed;
}
result Z2PBsavedue() {

  EEPROM.write(EepromZ2PBSPhr, Z2PBSPhr);
  EEPROM.write(EepromZ2PBSPmin, Z2PBSPmin);
  return proceed;
}
void EepromReadZ2PB()
{
  Z2PBSThr = EEPROM.read(EepromZ2PBSThr);
  Z2PBSTmin = EEPROM.read(EepromZ2PBSTmin);
  Z2PBSPhr = EEPROM.read(EepromZ2PBSPhr);
  Z2PBSPmin = EEPROM.read(EepromZ2PBSPmin);
  return proceed;
}
//*************************************************************************
result Z2ZPBsavetime() {
  EEPROM.write(EepromZ2ZPBSThr, Z2ZPBSThr);
  EEPROM.write(EepromZ2ZPBSTmin, Z2ZPBSTmin);

  return proceed;
}
result Z2ZPBsavedue() {

  EEPROM.write(EepromZ2ZPBSPhr, Z2ZPBSPhr);
  EEPROM.write(EepromZ2ZPBSPmin, Z2ZPBSPmin);
  return proceed;
}
void EepromReadZ2ZPB()
{
  Z2ZPBSThr = EEPROM.read(EepromZ2ZPBSThr);
  Z2ZPBSTmin = EEPROM.read(EepromZ2ZPBSTmin);
  Z2ZPBSPhr = EEPROM.read(EepromZ2ZPBSPhr);
  Z2ZPBSPmin = EEPROM.read(EepromZ2ZPBSPmin);
  return proceed;
}
//*****************************************************************
result Z2APsavetime() {
  EEPROM.write(EepromZ2APSThr, Z2APSThr);
  EEPROM.write(EepromZ2APSTmin, Z2APSTmin);

  return proceed;
}
result Z2APsavedue() {

  EEPROM.write(EepromZ2APSPhr, Z2APSPhr);
  EEPROM.write(EepromZ2APSPmin, Z2APSPmin);
  return proceed;
}
void EepromReadZ2Air()
{
  Z2APSThr = EEPROM.read(EepromZ2APSThr);
  Z2APSTmin = EEPROM.read(EepromZ2APSTmin);
  Z2APSPhr = EEPROM.read(EepromZ2APSPhr);
  Z2APSPmin = EEPROM.read(EepromZ2APSPmin);
  return proceed;
}
//*****************************************************************************
result Z2ZAPsavetime() {
  EEPROM.write(EepromZ2ZAPSThr, Z2ZAPSThr);
  EEPROM.write(EepromZ2ZAPSTmin, Z2ZAPSTmin);

  return proceed;
}
result Z2ZAPsavedue() {

  EEPROM.write(EepromZ2ZAPSPhr, Z2ZAPSPhr);
  EEPROM.write(EepromZ2ZAPSPmin, Z2ZAPSPmin);
  return proceed;
}
void EepromReadZ2ZAir()
{
  Z2ZAPSThr = EEPROM.read(EepromZ2ZAPSThr);
  Z2ZAPSTmin = EEPROM.read(EepromZ2ZAPSTmin);
  Z2ZAPSPhr = EEPROM.read(EepromZ2ZAPSPhr);
  Z2ZAPSPmin = EEPROM.read(EepromZ2ZAPSPmin);
  return proceed;
}
////********************************************************************************************************
///********************
result Z3PAsavetime() {
  EEPROM.write(EepromZ3PASThr, Z3PASThr);
  EEPROM.write(EepromZ3PASTmin, Z3PASTmin);

  return proceed;
}
result Z3PAsavedue() {

  EEPROM.write(EepromZ3PASPhr, Z3PASPhr);
  EEPROM.write(EepromZ3PASPmin, Z3PASPmin);
  return proceed;
}
void EepromReadZ3AP()
{
  Z3PASThr = EEPROM.read(EepromZ3PASThr);
  Z3PASTmin = EEPROM.read(EepromZ3PASTmin);
  Z3PASPhr = EEPROM.read(EepromZ3PASPhr);
  Z3PASPmin = EEPROM.read(EepromZ3PASPmin);
  return proceed;
}
//*****************************************************************
result Z3ZPAsavetime() {
  EEPROM.write(EepromZ3ZPASThr, Z3ZPASThr);
  EEPROM.write(EepromZ3ZPASTmin, Z3ZPASTmin);

  return proceed;
}
result Z3ZPAsavedue() {

  EEPROM.write(EepromZ3ZPASPhr, Z3ZPASPhr);
  EEPROM.write(EepromZ3ZPASPmin, Z3ZPASPmin);
  return proceed;
}
void EepromReadZ3ZAP()
{
  Z3ZPASThr = EEPROM.read(EepromZ3ZPASThr);
  Z3ZPASTmin = EEPROM.read(EepromZ3ZPASTmin);
  Z3ZPASPhr = EEPROM.read(EepromZ3ZPASPhr);
  Z3ZPASPmin = EEPROM.read(EepromZ3ZPASPmin);
  return proceed;
}  
//**********************************************************************************
result Z3PBsavetime() {
  EEPROM.write(EepromZ3PBSThr, Z3PBSThr);
  EEPROM.write(EepromZ3PBSTmin, Z3PBSTmin);

  return proceed;
}
result Z3PBsavedue() {

  EEPROM.write(EepromZ3PBSPhr, Z3PBSPhr);
  EEPROM.write(EepromZ3PBSPmin, Z3PBSPmin);
  return proceed;
}
void EepromReadZ3PB()
{
  Z3PBSThr = EEPROM.read(EepromZ3PBSThr);
  Z3PBSTmin = EEPROM.read(EepromZ3PBSTmin);
  Z3PBSPhr = EEPROM.read(EepromZ3PBSPhr);
  Z3PBSPmin = EEPROM.read(EepromZ3PBSPmin);
  return proceed;
}
//*************************************************************************
result Z3ZPBsavetime() {
  EEPROM.write(EepromZ3ZPBSThr, Z3ZPBSThr);
  EEPROM.write(EepromZ3ZPBSTmin, Z3ZPBSTmin);

  return proceed;
}
result Z3ZPBsavedue() {

  EEPROM.write(EepromZ3ZPBSPhr, Z3ZPBSPhr);
  EEPROM.write(EepromZ3ZPBSPmin, Z3ZPBSPmin);
  return proceed;
}
void EepromReadZ3ZPB()
{
  Z3ZPBSThr = EEPROM.read(EepromZ3ZPBSThr);
  Z3ZPBSTmin = EEPROM.read(EepromZ3ZPBSTmin);
  Z3ZPBSPhr = EEPROM.read(EepromZ3ZPBSPhr);
  Z3ZPBSPmin = EEPROM.read(EepromZ3ZPBSPmin);
  return proceed;
}
//*****************************************************************
result Z3APsavetime() {
  EEPROM.write(EepromZ3APSThr, Z3APSThr);
  EEPROM.write(EepromZ3APSTmin, Z3APSTmin);

  return proceed;
}
result Z3APsavedue() {

  EEPROM.write(EepromZ3APSPhr, Z3APSPhr);
  EEPROM.write(EepromZ3APSPmin, Z3APSPmin);
  return proceed;
}
void EepromReadZ3Air()
{
  Z3APSThr = EEPROM.read(EepromZ2APSThr);
  Z3APSTmin = EEPROM.read(EepromZ2APSTmin);
  Z3APSPhr = EEPROM.read(EepromZ2APSPhr);
  Z3APSPmin = EEPROM.read(EepromZ3APSPmin);
  return proceed;
}
//*****************************************************************************
result Z3ZAPsavetime() {
  EEPROM.write(EepromZ3ZAPSThr, Z3ZAPSThr);
  EEPROM.write(EepromZ3ZAPSTmin, Z3ZAPSTmin);

  return proceed;
}
result Z3ZAPsavedue() {

  EEPROM.write(EepromZ3ZAPSPhr, Z3ZAPSPhr);
  EEPROM.write(EepromZ3APSPmin, Z3ZAPSPmin);
  return proceed;
}
void EepromReadZ3ZAir()
{
  Z3ZAPSThr = EEPROM.read(EepromZ3APSThr);
  Z3APSTmin = EEPROM.read(EepromZ3APSTmin);
  Z3ZAPSPhr = EEPROM.read(EepromZ3ZAPSPhr);
  Z3ZAPSPmin = EEPROM.read(EepromZ3ZAPSPmin);
  return proceed;
}
///**********************
//***********************************************************************************************
result Z4PAsavetime() {
  EEPROM.write(EepromZ4PASThr, Z4PASThr);
  EEPROM.write(EepromZ4PASTmin, Z4PASTmin);

  return proceed;
}
result Z4PAsavedue() {

  EEPROM.write(EepromZ4PASPhr, Z4PASPhr);
  EEPROM.write(EepromZ4PASPmin, Z4PASPmin);
  return proceed;
}
void EepromReadZ4AP()
{
  Z4PASThr = EEPROM.read(EepromZ4PASThr);
  Z4PASTmin = EEPROM.read(EepromZ4PASTmin);
  Z4PASPhr = EEPROM.read(EepromZ4PASPhr);
  Z4PASPmin = EEPROM.read(EepromZ4PASPmin);
  return proceed;
}
//*****************************************************************
result Z4ZPAsavetime() {
  EEPROM.write(EepromZ4ZPASThr, Z4ZPASThr);
  EEPROM.write(EepromZ4ZPASTmin, Z4ZPASTmin);

  return proceed;
}
result Z4ZPAsavedue() {

  EEPROM.write(EepromZ4ZPASPhr, Z4ZPASPhr);
  EEPROM.write(EepromZ4ZPASPmin, Z4ZPASPmin);
  return proceed;
}
void EepromReadZ4ZAP()
{
  Z4ZPASThr = EEPROM.read(EepromZ4ZPASThr);
  Z4ZPASTmin = EEPROM.read(EepromZ4ZPASTmin);
  Z4ZPASPhr = EEPROM.read(EepromZ4ZPASPhr);
  Z4ZPASPmin = EEPROM.read(EepromZ4ZPASPmin);
  return proceed;
}  
//**********************************************************************************
result Z4PBsavetime() {
  EEPROM.write(EepromZ4PBSThr, Z4PBSThr);
  EEPROM.write(EepromZ4PBSTmin, Z4PBSTmin);

  return proceed;
}
result Z4PBsavedue() {

  EEPROM.write(EepromZ4PBSPhr, Z4PBSPhr);
  EEPROM.write(EepromZ4PBSPmin, Z4PBSPmin);
  return proceed;
}
void EepromReadZ4PB()
{
  Z4PBSThr = EEPROM.read(EepromZ4PBSThr);
  Z4PBSTmin = EEPROM.read(EepromZ4PBSTmin);
  Z4PBSPhr = EEPROM.read(EepromZ4PBSPhr);
  Z4PBSPmin = EEPROM.read(EepromZ4PBSPmin);
  return proceed;
}
//*************************************************************************
result Z4ZPBsavetime() {
  EEPROM.write(EepromZ4ZPBSThr, Z4ZPBSThr);
  EEPROM.write(EepromZ4ZPBSTmin, Z4ZPBSTmin);

  return proceed;
}
result Z4ZPBsavedue() {

  EEPROM.write(EepromZ4ZPBSPhr, Z4ZPBSPhr);
  EEPROM.write(EepromZ4ZPBSPmin, Z4ZPBSPmin);
  return proceed;
}
void EepromReadZ4ZPB()
{
  Z4ZPBSThr = EEPROM.read(EepromZ4ZPBSThr);
  Z4ZPBSTmin = EEPROM.read(EepromZ4ZPBSTmin);
  Z4ZPBSPhr = EEPROM.read(EepromZ4ZPBSPhr);
  Z4ZPBSPmin = EEPROM.read(EepromZ4ZPBSPmin);
  return proceed;
}
//*****************************************************************
result Z4APsavetime() {
  EEPROM.write(EepromZ4APSThr, Z4APSThr);
  EEPROM.write(EepromZ4APSTmin, Z4APSTmin);

  return proceed;
}
result Z4APsavedue() {

  EEPROM.write(EepromZ4APSPhr, Z4APSPhr);
  EEPROM.write(EepromZ4APSPmin, Z4APSPmin);
  return proceed;
}
void EepromReadZ4Air()
{
  Z4APSThr = EEPROM.read(EepromZ4APSThr);
  Z4APSTmin = EEPROM.read(EepromZ4APSTmin);
  Z4APSPhr = EEPROM.read(EepromZ4APSPhr);
  Z4APSPmin = EEPROM.read(EepromZ4APSPmin);
  return proceed;
}
//*****************************************************************************
result Z4ZAPsavetime() {
  EEPROM.write(EepromZ4ZAPSThr, Z4ZAPSThr);
  EEPROM.write(EepromZ4ZAPSTmin, Z4ZAPSTmin);

  return proceed;
}
result Z4ZAPsavedue() {

  EEPROM.write(EepromZ4ZAPSPhr, Z4ZAPSPhr);
  EEPROM.write(EepromZ4ZAPSPmin, Z4ZAPSPmin);
  return proceed;
}
void EepromReadZ4ZAir()
{
  Z4ZAPSThr = EEPROM.read(EepromZ4ZAPSThr);
  Z4ZAPSTmin = EEPROM.read(EepromZ4ZAPSTmin);
  Z4ZAPSPhr = EEPROM.read(EepromZ4ZAPSPhr);
  Z4ZAPSPmin = EEPROM.read(EepromZ4ZAPSPmin);
  return proceed;
}
//******************************************************************
// A PUMP
altMENU(menu, Z1PASET1, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z1PASThr, ":", "  HH", 0, 23, 10, 1, Z1PAsavetime, enterEvent, wrapStyle)
        , FIELD(Z1PASTmin, "", "  MIN", 0, 59, 10, 1, Z1PAsavetime, enterEvent, wrapStyle) 
       );
       
altMENU(menu, Z1PASET2, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z1PASPhr, ":", "  HH", 0, 23, 10, 1, Z1PAsavedue, enterEvent, wrapStyle)
        , FIELD(Z1PASPmin, "", "  MIN", 0, 59, 10, 1, Z1PAsavedue, enterEvent, wrapStyle)
              );
//***************************************************************************

//  B PUMP 
altMENU(menu, Z1PBSET3, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z1PBSThr, ":", "  HH", 0, 23, 10, 1, Z1PBsavetime, enterEvent, wrapStyle)
        , FIELD(Z1PBSTmin, "", "  MIN", 0, 59, 10, 1, Z1PBsavetime, enterEvent, wrapStyle) 
       );
       
altMENU(menu, Z1PBSET4, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z1PBSPhr, ":", "  HH", 0, 23, 10, 1, Z1PBsavedue, enterEvent, wrapStyle)
        , FIELD(Z1PBSPmin, "", "  MIN", 0, 59, 10, 1, Z1PBsavedue, enterEvent, wrapStyle)
              );
//****************************************************************

////AIRSTONE PUMP
altMENU(menu, Z1APSET5, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z1APSThr, ":", "  HH", 0, 23, 10, 1, Z1APsavetime, enterEvent, wrapStyle)
        , FIELD(Z1APSTmin, "", "  MIN", 0, 59, 10, 1, Z1APsavetime, enterEvent, wrapStyle) 
       );
       
altMENU(menu, Z1APSET6, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z1APSPhr, ":", "  HH", 0, 23, 10, 1, Z1APsavedue, enterEvent, wrapStyle)
        , FIELD(Z1APSPmin, "", "  MIN", 0, 59, 10, 1, Z1APsavedue, enterEvent, wrapStyle)
              );
//****************************************************************
// A PUMP
altMENU(menu, Z2PASET7, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z2PASThr, ":", "  HH", 0, 23, 20, 1, Z2PAsavetime, enterEvent, wrapStyle)
        , FIELD(Z2PASTmin, "", "  MIN", 0, 59, 20, 1, Z2PAsavetime, enterEvent, wrapStyle) 
       );
       
altMENU(menu, Z2PASET8, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z2PASPhr, ":", "  HH", 0, 23, 20, 1, Z2PAsavedue, enterEvent, wrapStyle)
        , FIELD(Z2PASPmin, "", "  MIN", 0, 59, 20, 1, Z2PAsavedue, enterEvent, wrapStyle)
              );
//***************************************************************************

//  B PUMP 
altMENU(menu, Z2PBSET9, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z2PBSThr, ":", "  HH", 0, 23, 20, 1, Z2PBsavetime, enterEvent, wrapStyle)
        , FIELD(Z2PBSTmin, "", "  MIN", 0, 59, 20, 1, Z2PBsavetime, enterEvent, wrapStyle) 
       );
       
altMENU(menu, Z2PBSET10, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z2PBSPhr, ":", "  HH", 0, 23, 20, 1, Z2PBsavedue, enterEvent, wrapStyle)
        , FIELD(Z2PBSPmin, "", "  MIN", 0, 59, 20, 1, Z2PBsavedue, enterEvent, wrapStyle)
              );
//****************************************************************

////AIRSTONE PUMP
altMENU(menu, Z2APSET11, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z2APSThr, ":", "  HH", 0, 23, 20, 1, Z2APsavetime, enterEvent, wrapStyle)
        , FIELD(Z2APSTmin, "", "  MIN", 0, 59, 20, 1, Z2APsavetime, enterEvent, wrapStyle) 
       );
       
altMENU(menu, Z2APSET12, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z2APSPhr, ":", "  HH", 0, 23, 20, 1, Z2APsavedue, enterEvent, wrapStyle)
        , FIELD(Z2APSPmin, "", "  MIN", 0, 59, 20, 1, Z2APsavedue, enterEvent, wrapStyle)
              );
//****************************************************************
// A PUMP
altMENU(menu, Z3PASET13, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z3PASThr, ":", "  HH", 0, 23, 30, 1, Z3PAsavetime, enterEvent, wrapStyle)
        , FIELD(Z3PASTmin, "", "  MIN", 0, 59, 30, 1, Z3PAsavetime, enterEvent, wrapStyle) 
       );
       
altMENU(menu, Z3PASET14, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z3PASPhr, ":", "  HH", 0, 23, 30, 1, Z3PAsavedue, enterEvent, wrapStyle)
        , FIELD(Z3PASPmin, "", "  MIN", 0, 59, 30, 1, Z3PAsavedue, enterEvent, wrapStyle)
              );
//***************************************************************************

//  B PUMP 
altMENU(menu, Z3PBSET15, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z3PBSThr, ":", "  HH", 0, 23, 30, 1, Z3PBsavetime, enterEvent, wrapStyle)
        , FIELD(Z3PBSTmin, "", "  MIN", 0, 59, 30, 1, Z3PBsavetime, enterEvent, wrapStyle) 
       );
       
altMENU(menu, Z3PBSET16, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z3PBSPhr, ":", "  HH", 0, 23, 30, 1, Z3PBsavedue, enterEvent, wrapStyle)
        , FIELD(Z3PBSPmin, "", "  MIN", 0, 59, 30, 1, Z3PBsavedue, enterEvent, wrapStyle)
              );
//****************************************************************

////AIRSTONE PUMP
altMENU(menu, Z3APSET17, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z3APSThr, ":", "  HH", 0, 23, 30, 1, Z3APsavetime, enterEvent, wrapStyle)
        , FIELD(Z3APSTmin, "", "  MIN", 0, 59, 30, 1, Z3APsavetime, enterEvent, wrapStyle) 
       );
       
altMENU(menu, Z3APSET18, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z3APSPhr, ":", "  HH", 0, 23, 30, 1, Z3APsavedue, enterEvent, wrapStyle)
        , FIELD(Z3APSPmin, "", "  MIN", 0, 59, 30, 1, Z3APsavedue, enterEvent, wrapStyle)
              );

// A PUMP
altMENU(menu, Z4PASET19, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z4PASThr, ":", "  HH", 0, 23, 10, 1, Z4PAsavetime, enterEvent, wrapStyle)
        , FIELD(Z4PASTmin, "", "  MIN", 0, 59, 10, 1, Z4PAsavetime, enterEvent, wrapStyle) 
       );
       
altMENU(menu, Z4PASET20, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z4PASPhr, ":", "  HH", 0, 23, 10, 1, Z4PAsavedue, enterEvent, wrapStyle)
        , FIELD(Z4PASPmin, "", "  MIN", 0, 59, 10, 1, Z4PAsavedue, enterEvent, wrapStyle)
              );
//***************************************************************************

//  B PUMP 
altMENU(menu, Z4PBSET21, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z4PBSThr, ":", "  HH", 0, 23, 10, 1, Z4PBsavetime, enterEvent, wrapStyle)
        , FIELD(Z4PBSTmin, "", "  MIN", 0, 59, 10, 1, Z4PBsavetime, enterEvent, wrapStyle) 
       );
       
altMENU(menu, Z4PBSET22, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z4PBSPhr, ":", "  HH", 0, 23, 10, 1, Z4PBsavedue, enterEvent, wrapStyle)
        , FIELD(Z4PBSPmin, "", "  MIN", 0, 59, 10, 1, Z4PBsavedue, enterEvent, wrapStyle)
              );
//****************************************************************

////AIRSTONE PUMP
altMENU(menu, Z4APSET23, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z4APSThr, ":", "  HH", 0, 23, 10, 1, Z4APsavetime, enterEvent, wrapStyle)
        , FIELD(Z4APSTmin, "", "  MIN", 0, 59, 10, 1, Z4APsavetime, enterEvent, wrapStyle) 
       );
       
altMENU(menu, Z4APSET24, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z4APSPhr, ":", "  HH", 0, 23, 10, 1, Z4APsavedue, enterEvent, wrapStyle)
        , FIELD(Z4APSPmin, "", "  MIN", 0, 59, 10, 1, Z4APsavedue, enterEvent, wrapStyle)
              );

              
//              // A PUMP
//altMENU(menu, Z1PASET1, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
//        , FIELD(Z1PASThr, ":", "  HH", 0, 23, 10, 1, Z1PAsavetime, enterEvent, wrapStyle)
//        , FIELD(Z1PASTmin, "", "  MIN", 0, 59, 10, 1, Z1PAsavetime, enterEvent, wrapStyle) 
//       );
//       
//altMENU(menu, Z1PASET2, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
//        , FIELD(Z1PASPhr, ":", "  HH", 0, 23, 10, 1, Z1PAsavedue, enterEvent, wrapStyle)
//        , FIELD(Z1PASPmin, "", "  MIN", 0, 59, 10, 1, Z1PAsavedue, enterEvent, wrapStyle)
//              );
////***************************************************************************
//
////  B PUMP 
//altMENU(menu, Z1PBSET1, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
//        , FIELD(Z1PBSThr, ":", "  HH", 0, 23, 10, 1, Z1PBsavetime, enterEvent, wrapStyle)
//        , FIELD(Z1PBSTmin, "", "  MIN", 0, 59, 10, 1, Z1PBsavetime, enterEvent, wrapStyle) 
//       );
//       
//altMENU(menu, Z1PBSET2, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
//        , FIELD(Z1PBSPhr, ":", "  HH", 0, 23, 10, 1, Z1PBsavedue, enterEvent, wrapStyle)
//        , FIELD(Z1PBSPmin, "", "  MIN", 0, 59, 10, 1, Z1PBsavedue, enterEvent, wrapStyle)
//              );
////****************************************************************
//
//////AIRSTONE PUMP
//altMENU(menu, Z1APSET5, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
//        , FIELD(Z1APSThr, ":", "  HH", 0, 23, 10, 1, Z1APsavetime, enterEvent, wrapStyle)
//        , FIELD(Z1APSTmin, "", "  MIN", 0, 59, 10, 1, Z1APsavetime, enterEvent, wrapStyle) 
//       );
//       
//altMENU(menu, Z1APSET6, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
//        , FIELD(Z1APSPhr, ":", "  HH", 0, 23, 10, 1, Z1APsavedue, enterEvent, wrapStyle)
//        , FIELD(Z1APSPmin, "", "  MIN", 0, 59, 10, 1, Z1APsavedue, enterEvent, wrapStyle)
//              );
//             

////**************************************************************************************
 
MENU(Z1PA, "PUMP A", showEvent, enterEvent, noStyle
     
//    , FIELD(PUMPAONtime, " ON TIME", " ", 0, 0, 0, doNothing, enterEvent, wrapStyle)
//      , FIELD(PUMPAOFFtime, " OFF TIME", " ", 0, 0, 0, doNothing, enterEvent, wrapStyle)

      , SUBMENU(Z1PASET1)
      , SUBMENU(Z1PASET2)
     , OP("       SAVE"  , EepromReadZ1AP, enterEvent)
     
     

     , EXIT("<BACK")
    );

MENU(Z1PB, "PUMP B", showEvent, enterEvent, noStyle
      , SUBMENU(Z1PBSET3)
      , SUBMENU(Z1PBSET4)
     , OP("       SAVE"  ,EepromReadZ1PB , enterEvent)

     , EXIT("<BACK")
    );  

MENU(Z1AP, "DO PUMP", showEvent, enterEvent, noStyle
//    , FIELD(airon, " ON TIME", " ", 0, 0, 0, doNothing, enterEvent, wrapStyle)

      , SUBMENU(Z1APSET5)
      , SUBMENU(Z1APSET6)
      
     , OP("       SAVE"  ,EepromReadZ1Air , enterEvent)

     , EXIT("<BACK")
    ); 

MENU(Z2PA, "PUMP A", showEvent, enterEvent, noStyle
     
//    , FIELD(PUMPAONtime, " ON TIME", " ", 0, 0, 0, doNothing, enterEvent, wrapStyle)
//      , FIELD(PUMPAOFFtime, " OFF TIME", " ", 0, 0, 0, doNothing, enterEvent, wrapStyle)

      , SUBMENU(Z2PASET7)
      , SUBMENU(Z2PASET8)
     , OP("       SAVE"  , EepromReadZ1AP, enterEvent)
     
     

     , EXIT("<BACK")
     
    );

MENU(Z2PB, "PUMP B", showEvent, enterEvent, noStyle
      , SUBMENU(Z2PBSET9)
      , SUBMENU(Z2PBSET10)
     , OP("       SAVE"  ,EepromReadZ1PB , enterEvent)

     , EXIT("<BACK")
    );  

MENU(Z2AP, "DO PUMP", showEvent, enterEvent, noStyle
//    , FIELD(airon, " ON TIME", " ", 0, 0, 0, doNothing, enterEvent, wrapStyle)

      , SUBMENU(Z2APSET11)
      , SUBMENU(Z2APSET12)
      
     , OP("       SAVE"  ,EepromReadZ1Air , enterEvent)

     , EXIT("<BACK")
    ); 

MENU(Z3PA, "PUMP A", showEvent, enterEvent, noStyle
     
//    , FIELD(PUMPAONtime, " ON TIME", " ", 0, 0, 0, doNothing, enterEvent, wrapStyle)
//      , FIELD(PUMPAOFFtime, " OFF TIME", " ", 0, 0, 0, doNothing, enterEvent, wrapStyle)

      , SUBMENU(Z3PASET13)
      , SUBMENU(Z3PASET14)
     , OP("       SAVE"  , EepromReadZ1AP, enterEvent)
     
     

     , EXIT("<BACK")
    );

MENU(Z3PB, "PUMP B", showEvent, enterEvent, noStyle
      , SUBMENU(Z3PBSET15)
      , SUBMENU(Z3PBSET16)
     , OP("       SAVE"  ,EepromReadZ1PB , enterEvent)

     , EXIT("<BACK")
    );  

MENU(Z3AP, "DO PUMP", showEvent, enterEvent, noStyle
//    , FIELD(airon, " ON TIME", " ", 0, 0, 0, doNothing, enterEvent, wrapStyle)

      , SUBMENU(Z3APSET17)
      , SUBMENU(Z3APSET18)
      
     , OP("       SAVE"  ,EepromReadZ1Air , enterEvent)

     , EXIT("<BACK")
    ); 

MENU(Z4PA, "PUMP A", showEvent, enterEvent, noStyle
     
//    , FIELD(PUMPAONtime, " ON TIME", " ", 0, 0, 0, doNothing, enterEvent, wrapStyle)
//      , FIELD(PUMPAOFFtime, " OFF TIME", " ", 0, 0, 0, doNothing, enterEvent, wrapStyle)

      , SUBMENU(Z4PASET19)
      , SUBMENU(Z4PASET20)
     , OP("       SAVE"  , EepromReadZ1AP, enterEvent)
     
     

     , EXIT("<BACK")
    );

MENU(Z4PB, "PUMP B", showEvent, enterEvent, noStyle
      , SUBMENU(Z4PBSET21)
      , SUBMENU(Z4PBSET22)
     , OP("       SAVE"  ,EepromReadZ1PB , enterEvent)

     , EXIT("<BACK")
    );  

MENU(Z4AP, "DO PUMP", showEvent, enterEvent, noStyle
//    , FIELD(airon, " ON TIME", " ", 0, 0, 0, doNothing, enterEvent, wrapStyle)

      , SUBMENU(Z4APSET23)
      , SUBMENU(Z4APSET24)
      
     , OP("       SAVE"  ,EepromReadZ1Air , enterEvent)

     , EXIT("<BACK")
    ); 

//MENU(Z1WL, "NT SOL", showEvent, enterEvent, noStyle
//      , SUBMENU(Z1SOLSET1)
//      , SUBMENU(Z1SOLSET2)
//     , OP("       CONFIRM"  , EepromReadZ1SOL, enterEvent)
//
//     , EXIT("<BACK")
//    ); 
//*************************************************************              


TOGGLE(ledRED, redLED, "       RED : ", doNothing, noEvent, noStyle
       , VALUE("On", HIGH, doNothing, noEvent)
       , VALUE("Off", LOW, doNothing, noEvent)
      );


TOGGLE(ledGREEN, greenLED, "       GREEN : ", doNothing, noEvent, noStyle
       , VALUE("On", HIGH, doNothing, noEvent)
       , VALUE("Off", LOW, doNothing, noEvent)
      );
      
TOGGLE(ledBLUE, blueLED, "       BLUE : ", doNothing, noEvent, noStyle
       , VALUE("On", HIGH, doNothing, noEvent)
       , VALUE("Off", LOW, doNothing, noEvent)
      );
      
//*****************************************************************
int selTest21=0;
SELECT(selTest21,selMenu21,"Pump A",doNothing,noEvent,noStyle
  ,VALUE("ON",0,doNothing,noEvent)
  ,VALUE("OFF",1,doNothing,noEvent)
);

int selTest31=0;
SELECT(selTest31,selMenu31,"Pump B",doNothing,noEvent,noStyle
  ,VALUE("ON",0,doNothing,noEvent)
  ,VALUE("OFF",1,doNothing,noEvent)
);

int selTest41=0;
SELECT(selTest41,selMenu41,"DO Pump",doNothing,noEvent,noStyle
  ,VALUE("ON",0,doNothing,noEvent)
  ,VALUE("OFF",1,doNothing,noEvent)
);

MENU(selMenu01,"ZONE 1",showEvent,anyEvent,noStyle
 ,SUBMENU(Z1PA)
 ,SUBMENU(Z1PB)
 ,SUBMENU(Z1AP)
 //,SUBMENU(Z1WL)
  ,EXIT("<BACK>")

  );

MENU(selMenu02,"ZONE 2",showEvent,anyEvent,noStyle
 ,SUBMENU(Z2PA)
 ,SUBMENU(Z2PB)
 ,SUBMENU(Z2AP)
 //,SUBMENU(Z1WL)
  ,EXIT("<BACK>")

  );
  MENU(selMenu03,"ZONE 3",showEvent,anyEvent,noStyle
 ,SUBMENU(Z3PA)
 ,SUBMENU(Z3PB)
 ,SUBMENU(Z3AP)
 //,SUBMENU(Z1WL)
  ,EXIT("<BACK>")

  );
  MENU(selMenu04,"ZONE 4",showEvent,anyEvent,noStyle
 ,SUBMENU(Z4PA)
 ,SUBMENU(Z4PB)
 ,SUBMENU(Z4AP)
 //,SUBMENU(Z1WL)
  ,EXIT("<BACK>")

  );
bool Autocnt;
void runer(){
if(Autocnt==true)
{
  PUMPAON.start();
  PUMPBON.start();
  AIRSTONEON.start();
   // Serial.print ("auto start");
}
}
void stoper(){
if(Autocnt==false)
{
 digitalWrite(PUMPA,LOW);
  digitalWrite(PUMPB,LOW);
  digitalWrite(AIRPUMP,LOW);
  digitalWrite(SOLVALVE,LOW);
}
}
TOGGLE(Autocnt, autoOn, "AUTOMATIC : ", doNothing, noEvent, noStyle
       , VALUE("ON", true, runer, enterEvent)
       , VALUE("OFF",false,stoper, enterEvent)
      );
        
MENU(Automatic," AUTO MODE",showEvent,anyEvent,noStyle
 ,SUBMENU(autoOn)
 ,SUBMENU(selMenu01)
 ,SUBMENU(selMenu02)
 ,SUBMENU(selMenu03)
 ,SUBMENU(selMenu04)
  ,EXIT("<BACK>")

  );

// A PUMP
altMENU(menu, Z1ZPASET1 , " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z1ZPASThr, ":", "  HH", 0, 23, 10, 1, Z1ZPAsavetime, enterEvent, wrapStyle)
        , FIELD(Z1ZPASTmin, "", "  MIN", 0, 59, 10, 1, Z1ZPAsavetime, enterEvent, wrapStyle) 
       );
       
altMENU(menu, Z1ZPASET2, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Z1ZPASPhr, ":", "  HH", 0, 23, 10, 1, Z1ZPAsavedue, enterEvent, wrapStyle)
        , FIELD(Z1ZPASPmin, "", "  MIN", 0, 59, 10, 1, Z1ZPAsavedue, enterEvent, wrapStyle)
              );
//***************************************************************************

////  B PUMP 
//altMENU(menu, Z1ZPBSET1, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
//        , FIELD(Z1ZPBSThr, ":", "  HH", 0, 23, 10, 1, Z1PBsavetime, enterEvent, wrapStyle)
//        , FIELD(Z1ZPBSTmin, "", "  MIN", 0, 59, 10, 1, Z1PBsavetime, enterEvent, wrapStyle) 
//       );
//       
//altMENU(menu, Z1ZPBSET2, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
//        , FIELD(Z1ZPBSPhr, ":", "  HH", 0, 23, 10, 1, Z1ZPBsavedue, enterEvent, wrapStyle)
//        , FIELD(Z1ZPBSPmin, "", "  MIN", 0, 59, 10, 1, Z1ZPBsavedue, enterEvent, wrapStyle)
//              );
////****************************************************************
//
//////AIRSTONE PUMP
//altMENU(menu, Z1ZAPSET1, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
//        , FIELD(Z1ZAPSThr, ":", "  HH", 0, 23, 10, 1, Z1ZAPsavetime, enterEvent, wrapStyle)
//        , FIELD(Z1ZAPSTmin, "", "  MIN", 0, 59, 10, 1, Z1ZAPsavetime, enterEvent, wrapStyle) 
//       );
//       
//altMENU(menu, Z1APSET2, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
//        , FIELD(Z1APSPhr, ":", "  HH", 0, 23, 10, 1, Z1ZAPsavedue, enterEvent, wrapStyle)
//        , FIELD(Z1APSPmin, "", "  MIN", 0, 59, 10, 1, Z1ZAPsavedue, enterEvent, wrapStyle)
//              );
MENU(UNIT_1, "SCHEDULE 1", showEvent, enterEvent, wrapStyle 
   , SUBMENU(Z1ZPASET1)  
   , SUBMENU(Z1ZPASET2)
//   ,OP("Conform",unit1read,enterEvent)
   , EXIT("<Back")
);
MENU(UNIT_2, "SCHEDULE 2", showEvent,enterEvent, wrapStyle
   , SUBMENU(Z1ZPASET1)  
   , SUBMENU(Z1ZPASET2)
//   ,OP("Conform",unit2read,enterEvent)
   , EXIT("<Back")
);
MENU(UNIT_3, "SCHEDULE 3", showEvent, enterEvent, wrapStyle 
   , SUBMENU(Z1ZPASET1)  
   , SUBMENU(Z1ZPASET2)
//   ,OP("Conform",unit3read,enterEvent)
   , EXIT("<Back")
);
MENU(UNIT_4, "SCHEDULE 4", showEvent,enterEvent, wrapStyle
   , SUBMENU(Z1ZPASET1)  
   , SUBMENU(Z1ZPASET2)
//   ,OP("Conform",unit4read,enterEvent)
   , EXIT("<Back")
);
MENU(UNIT_5, "SCHEDULE 5", showEvent,enterEvent, wrapStyle 
   , SUBMENU(Z1ZPASET1)  
   , SUBMENU(Z1ZPASET2)
//   ,OP("Conform",unit5read,enterEvent)
   , EXIT("<Back")
);
MENU(UNIT_6, "SCHEDULE 6", showEvent,enterEvent, wrapStyle 
   , SUBMENU(Z1ZPASET1)  
   , SUBMENU(Z1ZPASET2)
//   ,OP("Conform",unit6read,enterEvent)
   , EXIT("<Back")
);
MENU(UNIT_7, "SCHEDULE 7", showEvent,enterEvent, wrapStyle
   , SUBMENU(Z1ZPASET1)  
   , SUBMENU(Z1ZPASET2)
//   ,OP("Conform",unit7read,enterEvent)
   , EXIT("<Back")
);
MENU(UNIT_8,"SCHEDULE 8", showEvent,enterEvent, wrapStyle 
   , SUBMENU(Z1ZPASET1)  
   , SUBMENU(Z1ZPASET2)
//   ,OP("Conform",unit8read,enterEvent)
   , EXIT("<Back")
);

MENU(UNIT_9, "SCHEDULE 9", showEvent,enterEvent, wrapStyle 
   , SUBMENU(Z1ZPASET1)  
   , SUBMENU(Z1ZPASET2)
//   ,OP("Conform",unit5read,enterEvent)
   , EXIT("<Back")
);
MENU(UNIT_10, "SCHEDULE 10", showEvent,enterEvent, wrapStyle 
   , SUBMENU(Z1ZPASET1)  
   , SUBMENU(Z1ZPASET2)
//   ,OP("Conform",unit6read,enterEvent)
   , EXIT("<Back")
);
MENU(UNIT_11, "SCHEDULE 11", showEvent,enterEvent, wrapStyle
   , SUBMENU(Z1ZPASET1)  
   , SUBMENU(Z1ZPASET2)
//   ,OP("Conform",unit7read,enterEvent)
   , EXIT("<Back")
);
MENU(UNIT_12,"SCHEDULE 12", showEvent,enterEvent, wrapStyle 
   , SUBMENU(Z1ZPASET1)  
   , SUBMENU(Z1ZPASET2)
//   ,OP("Conform",unit8read,enterEvent)
   , EXIT("<Back")
);
MENU(Z1ZPA,"PUMP A",showEvent,anyEvent,noStyle
 ,SUBMENU(UNIT_1)
  ,SUBMENU(UNIT_2)
  ,SUBMENU(UNIT_3)
  ,SUBMENU(UNIT_4)
  ,SUBMENU(UNIT_5)
  ,SUBMENU(UNIT_6)
  ,SUBMENU(UNIT_7)
  ,SUBMENU(UNIT_8)
  ,SUBMENU(UNIT_9)
  ,SUBMENU(UNIT_10)
  ,SUBMENU(UNIT_11)
  ,SUBMENU(UNIT_12)
  //,SUBMENU(START)
//  ,OP("START",scheduling,enterEvent)
  ,EXIT("<Back") 
);


MENU(Z1ZPB,"PUMP B",showEvent,anyEvent,noStyle
 ,SUBMENU(UNIT_1)
  ,SUBMENU(UNIT_2)
  ,SUBMENU(UNIT_3)
  ,SUBMENU(UNIT_4)
  ,SUBMENU(UNIT_5)
  ,SUBMENU(UNIT_6)
  ,SUBMENU(UNIT_7)
  ,SUBMENU(UNIT_8)
   ,SUBMENU(UNIT_9)
  ,SUBMENU(UNIT_10)
  ,SUBMENU(UNIT_11)
  ,SUBMENU(UNIT_12)
  //,SUBMENU(START)
//  ,OP("START",scheduling,enterEvent)
  ,EXIT("<Back") 
);
MENU(Z1ZAP,"DO PUMP",showEvent,anyEvent,noStyle
 ,SUBMENU(UNIT_1)
  ,SUBMENU(UNIT_2)
  ,SUBMENU(UNIT_3)
  ,SUBMENU(UNIT_4)
  ,SUBMENU(UNIT_5)
  ,SUBMENU(UNIT_6)
  ,SUBMENU(UNIT_7)
  ,SUBMENU(UNIT_8)
   ,SUBMENU(UNIT_9)
  ,SUBMENU(UNIT_10)
  ,SUBMENU(UNIT_11)
  ,SUBMENU(UNIT_12)
  //,SUBMENU(START)
//  ,OP("START",scheduling,enterEvent)
  ,EXIT("<Back") 
);

MENU(sellMenu01,"ZONE 1",showEvent,anyEvent,noStyle
 ,SUBMENU(Z1ZPA)
 ,SUBMENU(Z1ZPB)
 ,SUBMENU(Z1ZAP)
 
// ,SUBMENU(Z1WL)
  ,EXIT("<BACK>")

  );

MENU(sel2Menu02,"ZONE 2",showEvent,anyEvent,noStyle
 ,SUBMENU(Z1ZPA)
 ,SUBMENU(Z1ZPB)
 ,SUBMENU(Z1ZAP)
// ,SUBMENU(Z1WL)
  ,EXIT("<BACK>")

  );
  MENU(sel3Menu03,"ZONE 3",showEvent,anyEvent,noStyle
 ,SUBMENU(Z1ZPA)
 ,SUBMENU(Z1ZPB)
 ,SUBMENU(Z1ZAP)
// ,SUBMENU(Z1WL)
  ,EXIT("<BACK>")

  );
  MENU(sel4Menu04,"ZONE 4",showEvent,anyEvent,noStyle
 ,SUBMENU(Z1ZPA)
 ,SUBMENU(Z1ZPB)
 ,SUBMENU(Z1ZAP)
// ,SUBMENU(Z1WL)
  ,EXIT("<BACK>")

  );
  bool Autocntt;
  TOGGLE(Autocntt, sche, "SCHEDULE : ", doNothing, noEvent, noStyle
       , VALUE("ON", true, doNothing, enterEvent)
       , VALUE("OFF",false,doNothing, enterEvent)
      );
  MENU(SCHEDULE," SCHEDULE MODE",showEvent,anyEvent,noStyle
 ,SUBMENU(sche)
 ,SUBMENU(sellMenu01)
 ,SUBMENU(sel2Menu02)
 ,SUBMENU(sel3Menu03)
 ,SUBMENU(sel4Menu04)
  ,EXIT("<BACK>")
);

//*********************************************************************************************************
 
int totalVolume1;
int EepromNTHset=300;
int EepromNTVSet=310;
///*********EPROM********************///
result NTHset() {
  EEPROM.write(EepromNTHset, tankHeight);  
  
//   EEPROM.read(EepromNTHset, tankHeight);  

  
  return proceed;
}
result NTVset() {
  
  EEPROM.write(EepromNTVSet, totalVolume1);
// EEPROM.read(EepromNTVSet, totalVolume);
  return proceed;
}
int EepromTSLSet = 350;
result TSLset() {
  
  EEPROM.write(EepromTSLSet, tanksetpoint);

  return proceed;
}
result EepromReadNT()
{
  tankHeight = EEPROM.read(EepromNTHset);
  totalVolume1 = EEPROM.read(EepromNTVSet);
  tanksetpoint = EEPROM.read(EepromTSLSet);

  
} 


int distanceincm;
int distanceinInch; 
MENU(tanksetting," TANK Zone 1 ",showEvent,anyEvent,noStyle
  ,SUBMENU(Tankcontrol)
  ,FIELD(distanceinInch,"    LEVEL : "," inch",0,0,0,0,doNothing,noEvent,noStyle)
  ,FIELD(tankHeight,"    HEIGHT : ","cm",0,1000,10,1,NTHset,enterEvent,wrapStyle)
  //,FIELD(totalVolume1,"    VOLUME : ","cm^3",0,5000,100,1,NTVset,enterEvent,wrapStyle)
  ,FIELD(tanksetpoint,"    SETLEVEL : ","cm",0,500,100,1,TSLset,enterEvent,wrapStyle)
  ,OP("          SAVE",EepromReadNT,enterEvent)
    
  ,EXIT("<BACK")
);

//MENU(tanksetting," TANK",showEvent,anyEvent,noStyle
//
// ,SUBMENU(Tankz1)
// ,SUBMENU(Tankz1)
// ,SUBMENU(Tankz1)
// ,SUBMENU(Tankz1)
//  ,EXIT("<BACK>")
//);

MENU(WLmenu," TANK LEVEL",showEvent,noEvent,noStyle
// ,FIELD(distanceincm,"LEVEL  "," cm",0,0,0,0,doNothing,noEvent,noStyle)
 ,FIELD(distanceinInch," LEVEL : "," inch",0,0,0,0,doNothing,noEvent,noStyle)
  ,FIELD(waterHeight,"     HEIGHT  :"," cm",0,0,0,0,doNothing,noEvent,noStyle)
  ,FIELD(waterQuantity,"   QUANTITY  :"," ml",0,0,0,0,doNothing,noEvent,noStyle)
  ,SUBMENU(tanksetting)
  ,SUBMENU(sol)
  
  
  ,EXIT("<BACK")
);

 
MENU(Settingsmenu," SETTINGS",showEvent,noEvent,noStyle
  , OP(" INFO", Cinfo, enterEvent)
  
  ,EXIT("<BACK")
);
MENU(mainMenu, "  IRRIGATION CONTROLLER", doNothing, noEvent, wrapStyle
     , OP(" MONITOR", Home, enterEvent)
     
     ,SUBMENU(Automatic)
     ,SUBMENU(SCHEDULE)
     ,SUBMENU(MANmenu)
     ,SUBMENU(tanksetting)
     ,SUBMENU(Settingsmenu)
//    , OP(" ABOUT", goHome, enterEvent)

     //  ,EXIT("<BACK")
    );




serialIn serial(Serial);

////input from the encoder + encoder button + serial
//menuIn* inputsList[]={&encStream,&encButton,&serial};
//chainStream<3> in(inputsList);//3 is the number of inputs

//fontY should now account for fontMarginY
#define fontX 3
#define fontY 10
#define MAX_DEPTH 10

//this macro replaces all the above commented lines
MENU_OUTPUTS(out, MAX_DEPTH
             // ,U8GLIB_OUT(u8g2,colors,fontX,fontY,{0,0,128/fontX,64/fontY})
             , U8G2_OUT(u8g2, colors, fontX, fontY, offsetX, offsetY, {0, 0, U8_Width / fontX, U8_Height / fontY})
             , SERIAL_OUT(Serial)
             , NONE
            );
keypadIn kpad(customKeypad);
NAVROOT(nav,mainMenu,MAX_DEPTH,kpad,out);

result doAlert(eventMask e, prompt &item) {
  nav.idleOn(alert);
  return proceed;
}

result Home(eventMask e, prompt &item) {
  nav.idleOn(Time);

}
result Time (menuOut& o,idleEvent e) {
  DateTime now = rtc.now();
  {
//

//
//
//   nhur1=now.hour();
//   nmin1=now.minute();
//    Serial.print("Time: ");
//     Serial.print(nhur1);
//     Serial.print(":");
//      
////      u8g2.setCursor(, 1);
//      u8g2.drawFrame( nhur1);
//      u8g2.drawFrame(":");
//      Serial.print( now.minute());
////      u8g2.setCursor(5,1);
//      u8g2.drawFrame( nmin1);
//      u8g2.setCursor(10,1);
////      lcd.print("Date: ");
//      //u8g2.drawStr(58, 52, "Date:")
//      u8g2.drawFrame( now.day());
//       u8g2.drawFrame("/");
//      
//      u8g2.drawFrame( now.month());
//       u8g2.drawFrame("/");
//     u8g2.drawFrame( now.year());
//    
//    }
//
  u8g2.drawFrame(0, 0, 128, 64);
   u8g2.drawFrame(0, 0, 128, 12);
     u8g2.drawFrame(0, 0, 128, 22);
   u8g2.drawFrame(12, 21, 120, 105);
 u8g2.drawFrame(41, 21, 100, 55);
  u8g2.drawFrame(70, 21, 100, 55);
 u8g2.drawFrame(99, 21, 100, 55);


//   u8g2.drawFrame(0, 0, 128, 48);

    u8g2.drawStr(16, 10, "      MONITOR    ");

      

 u8g2.drawStr(1, 20, " D/T:");
    u8g2.setCursor(28, 20);
    u8g2.print( now.day());
    u8g2.drawStr(38, 20, "/");
    u8g2.setCursor(44, 20);
    u8g2.print( now.month());
    u8g2.drawStr(49, 20, "/");
    u8g2.setCursor(55, 20);
    u8g2.print( now.year());
    //u8g2.print( ndaysOfTheWeek[now.dayOfTheWeek()]);
    
    //u8g2.drawStr(58, 52, "Date:");
    u8g2.setCursor(82 , 20); 
    u8g2.print( now.hour());
    u8g2.drawStr(92, 20, ":"); 
    u8g2.setCursor(97, 20);
    u8g2.print( now.minute());
    u8g2.drawStr(108, 20, ":"); 
    u8g2.setCursor(113, 20);
    u8g2.print( now.second());
 u8g2.drawStr(15, 30, "Zone1");
  u8g2.drawStr(26, 40, "L/M");
  u8g2.drawStr(55, 40, "L/M");
  u8g2.drawStr(85, 40, "L/M");
  u8g2.drawStr(112, 40, "L/M");

  u8g2.drawStr(26, 50, "L/M");
  u8g2.drawStr(55, 50, "L/M");
  u8g2.drawStr(85, 50, "L/M");
  u8g2.drawStr(112, 50, "L/M");


   u8g2.drawStr(32, 60, "nm");
  u8g2.drawStr(61, 60, "nm");
  u8g2.drawStr(91, 60, "nm");
  u8g2.drawStr(115, 60, "nm");
  

  
    u8g2.drawStr(2, 40, "WI");
    u8g2.drawStr(2, 50, "WO");
    u8g2.drawStr(2, 60, "R");
   
    u8g2.drawStr(44, 30, "Zone2");
     u8g2.drawStr(73, 30, "Zone3");
      u8g2.drawStr(102 , 30, "Zone4");
//      float temperature, humidity;
//  humidity = dht.readHumidity();
//  temperature = dht.readTemperature(); 
//// u8g2.clear();
// char tempF[6]; 
// char humF[6];
// dtostrf(temperature, 5, 1, tempF);
// dtostrf(humidity, 2, 0, humF);
// u8g2.drawStr(0, 41, " TEMP:");
//// u8g2.setCursor(30, 41);
//    if (dtostrf(temperature, 5, 1, tempF)){
//      u8g2.drawStr(24, 41, tempF);
//}
//else{
//    u8g2.drawStr(26, 41, NC);
//}
// u8g2.drawStr(52, 41, "C");
// u8g2.drawStr(68, 41, "HUM: ");
// 
// if (dtostrf(humidity, 2, 0, humF)){
//    u8g2.drawStr(88, 41, humF);
// }
// else{
//      u8g2.drawStr(85, 41, NC);
// }
//  u8g2.drawStr(105, 41, "%");


}
uva();
  return proceed;
}
result alert(menuOut& o, idleEvent e) {
  if (e == idling) {
    o.setCursor(0, 0);
    o.print("alert test");
    o.setCursor(0, 1);
    o.print("press [select]");
    o.setCursor(0, 2);
    o.print("to continue...");
  }
  return proceed;
}

result home(menuOut& o, idleEvent e) {
  {
    //u8g2.setFont(fontName);
    //updateScreen();

  }
  return proceed;
}
//result Home(eventMask e, prompt &item) {
//  //nav.idleOn(Time);
//
//}
result goHome(eventMask e, prompt &item) {
  nav.idleOn(home);
  return proceed;
}
//result doAlert(eventMask e, prompt &item) {
//  nav.idleOn(alert);
//  return proceed;
//}

result info(eventMask e, prompt &item) {
  nav.idleOn(onioninfo);
  return proceed;
}
result Cinfo(eventMask e, prompt &item) {
  nav.idleOn(controllerinfo);
  return proceed;
}
result controllerinfo(eventMask e, navNode& nav, prompt &item) {
  DateTime now = rtc.now();
  {
    u8g2.drawFrame(0, 0, 128, 64);
//    u8g2.drawFrame(5, 5, 118, 54);
    u8g2.drawStr(7, 12, "  CHIP ID : OOCCC6C7");
     u8g2.drawStr(12, 25,"FLASH ID : 0016301C");
    u8g2.drawStr(5, 40,"MAC : 84:F3:EB:CC:C6:C7");
    u8g2.drawStr(5, 55,"MANUFACTURE YEAR : 2021");
//    u8g2.setCursor(40, 51);

  }
  return proceed;
}

result onioninfo(eventMask e, navNode& nav, prompt &item) {

  {

    u8g2.drawStr(45, 51, "none");
  }
  return proceed;
}

///***************Read all data from memoery******************************
void readalldata() {
 EepromReadZ1AP();
  EepromReadZ1PB();
  EepromReadZ1Air();
  EepromReadNT();
      
  return proceed;
}
//when menu is suspended
result idle(menuOut& o, idleEvent e) {
  o.clear();
  switch (e) {
    case idleStart: o.println("suspending menu!"); break;
    case idling: o.println("suspended..."); break;
    case idleEnd: o.println("resuming menu."); break;
  }
  return proceed;
}



void sensorjson(){
DynamicJsonDocument doc(500);

doc["CID"] = "ZEIRRC21001";
doc["Date/Time:"] = t;

JsonObject Zone1 = doc.createNestedObject("Zone1");
Zone1["Pump A "] = 2; 
Zone1["PUMP B"] =50;
Zone1["AIR PUMP"] = 20;
Zone1["SOLENOID"] = 3;
Zone1["WO"] =50 ;
Zone1["WI"] = 20;
Zone1["WATERLEVEL"] = 80;
Zone1["RADIATION"] = 254;

serializeJsonPretty(doc, Serial3);
serializeJsonPretty(doc, Serial);

}
void dateandtime(void) {
  DateTime now = rtc.now();
  hourupg = now.hour();
  minupg = now.minute();
  secslive = now.second();
  Datelive = now.day();
  Monthlive = now.month();
  Yearlive = now.year();

}

long waterconlevel;
void getNutrientTL()
{
digitalWrite(trigPin, LOW);
delayMicroseconds (10);
digitalWrite(trigPin, HIGH);
delayMicroseconds (20);
digitalWrite(trigPin, LOW);


duration = pulseIn(echoPin, HIGH);
distanceincm = duration*0.034/2;
distanceinInch = distanceincm/2.54;
volume = pi*16*distanceincm; //change based on the volume for the tank
//waterHeight = tankHeight-distanceincm;
waterQuantity = totalVolume-volume-offset;
waterconlevel = tankHeight*2.54-waterHeight;

Serial.print("Tank is: ");
Serial.print(distanceincm);
Serial.println(" Cm");
Serial.print(distanceinInch);
//Serial.println(" Intch");
//Serial.print("Water level [cm]: ");
//Serial.println(waterHeight);
//Serial.print("Water Quantity [ml]: ");
//Serial.println(waterQuantity);

int temp;
 if(distanceincm<tanksetpoint && temp==0)
 {
     digitalWrite(SOLVALVE, LOW);
     Serial.println(" PUMP OFF");
     
     temp=1;
 }
 
  else if(distanceincm<tanksetpoint && temp==1)
 {
     digitalWrite(SOLVALVE, LOW);
     Serial.println(" PUMP OFF");
 }
 
 else if(distanceincm<tankHeight)
 {
   digitalWrite(SOLVALVE, HIGH);
   Serial.println(" PUMP ON");
   
   temp=0;
 }

 
//if(distanceincm<=200 ) 
// {
//     digitalWrite(SOLVALVE, LOW);
//    // digitalWrite(BUZZER, HIGH);
//     
//     Serial.print("Water Tank Full Motor Turned off");
////     delay(1000);
//    // digitalWrite(BUZZER, LOW);
////     delay(3000);
//     Wtemp=1;
// }
// 
//   if(distanceincm>=100 )
// { 
//     digitalWrite(SOLVALVE, HIGH); 
//     Serial.println("Water Tank Full Motor Turned ON");
////     delay(2000);
// }
 
// else if(distanceincm=<tankHeight)
// {
//   digitalWrite(SOLVALVE, HIGH);
//   Serial.println("LOW Water Level Motor Turned ON");
//   //delay(2000);
//   Wtemp=0;
// }
 return distance;

////delay(100);
//if(distanceincm==waterconlevel && duration==0)
// {
//     digitalWrite(SOLVALVE, LOW);
//    // digitalWrite(BUZZER, HIGH);
//     
//     Serial.print("Water Tank Full Motor Turned OFF");
////     delay(1000);
//    // digitalWrite(BUZZER, LOW);
//    // delay(3000);
//     Wtemp=1;
// }
// 
//  else if(distance<12 && duration==1)
// { 
//     digitalWrite(SOLVALVE, LOW); 
//     Serial.println("Water Tank Full Motor Turned OFF ");
////     delay(2000);
// }
// 
// else if(distance=<tankHeight)
// {
//   digitalWrite(SOLVALVE, HIGH);
//   Serial.println("LOW Water Level Motor Turned ON");
//   //delay(2000);
//   Wtemp=0;
// }
// return distance;
 
}
//************************************DHT11*****************************

void getDht11(){
  
  float temperature, humidity;
  humidity = dht.readHumidity();
  temperature = dht.readTemperature(); 

 char tempF[6]; 
 char humF[6];
 dtostrf(temperature, 5, 1, tempF);
 dtostrf(humidity, 2, 0, humF);

 Serial.print("T:");
 Serial.print(tempF);
 Serial.print((char)223);
 Serial.print("C ");
 Serial.println("H: ");
 Serial.print(humF);  
 Serial.println("%");
 }


//***********************************************************************


void setup() {
  Serial.begin(115200);
  pinMode(LEDPIN, OUTPUT);
  pinMode(led, OUTPUT);
   pinMode(led1, OUTPUT);
    pinMode(led2, OUTPUT);
    pinMode(led3, OUTPUT);
   while(!Serial);
  Serial.println("menu 4.x test");Serial.flush(); 
  Serial3.begin(115200);
  while (!Serial);
  Serial.println("ZEEPTOGREENS...........");
  Serial.flush();
Wire.begin();
    u8g2.begin();
  dht.begin();
   pinMode(A3 , OUTPUT);
  //WD reset
 // wdt_enable(WDTO_8S);
 
   sensorjson();
   
   DateTime now = rtc.now();
  rtc.begin();
   rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  sprintf(t, "%02d:%02d:%02d %02d/%02d/%02d",  now.hour(), now.minute(), now.second(), now.day(), now.month(), now.year());
//  Serial.print(F("Date/Time: "));
//  Serial.println(t);

  
  //mainMenu[0].enabled=disabledStatus;

  
  //change input burst for slow output devices
  //this is the number of max. processed inputs before drawing
  //nav.inputBurst=10;

  //BOOT
  nav.showTitle = true;

//  u8g2.drawStr(0, 0, "WELCOME TO ZEEPTOGREENS");

  u8g2.drawStr(10, 10, "SYSTEM BOOTING.....");
//  Timer1.initialize(1000);
// / Timer1.attachInterrupt(timerIsr);
//  SPI.begin();
//  u8g2.begin();
  //  noScreenSetup();

  readalldata();
  //TASK
//  r.init();
  Serial.println("Initialized scheduler");
  r.startNow();
  r.addTask(t1);
  Serial.println("added t1");
  t1.enable();
  t4.enable();
  r.addTask(t4);
//  pinMode(encBtn, INPUT_PULLUP);

  pinMode(BUZZER, OUTPUT);

  //***Indicator
  pinMode(LEDRED, OUTPUT);
  pinMode(LEDGREEN, OUTPUT);
  pinMode(LEDBLUE, OUTPUT);

  pinMode(PUMPA, OUTPUT);
  pinMode(PUMPB, OUTPUT);
  pinMode(AIRPUMP, OUTPUT);
  pinMode(SOLVALVE, OUTPUT);
  digitalWrite(PUMPA,LOW);
  digitalWrite(PUMPB,LOW);
  digitalWrite(AIRPUMP,LOW);
  digitalWrite(SOLVALVE,LOW);

  pinMode(UVOUT, INPUT);
  pinMode(REF_3V3, INPUT);

  ///Waterlevel 
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);


    nav.idleTask = Home;
  //***********COUNTER ****************************************
   PUMPAON.setCounter(Z1PASThr, Z1PASTmin, Z1PASTmin, PUMPAON.COUNT_UP, PUMPAONComplete);
   PUMPAON.setInterval(print_timePUMPA1, 1000);
   PUMPAOFF.setCounter(Z1PASPhr, Z1PASPmin, Z1PASPmin, PUMPAOFF.COUNT_DOWN, PUMPAOFFComplete);
   PUMPAOFF.setInterval(print_timePUMPA2, 1000);
// PUMPANone.setInterval(print_nonePUMPA, 2000);
// 
   PUMPBON.setCounter(Z1PBSThr, Z1PBSTmin, Z1PBSTmin, PUMPBON.COUNT_UP, PUMPBONComplete);
   PUMPBON.setInterval(print_timePUMPB1, 1000);
   PUMPBOFF.setCounter(Z1PBSPhr, Z1PBSPmin, Z1PBSPmin, PUMPBOFF.COUNT_DOWN, PUMPBOFFComplete);
   PUMPBOFF.setInterval(print_timePUMPB2, 1000);
 //PUMPBNone.setInterval(print_nonePUMPB, 2000);
//
   AIRSTONEON.setCounter(Z1APSThr, Z1APSTmin, Z1APSTmin, AIRSTONEON.COUNT_UP, AIRSTONEONComplete);
   AIRSTONEON.setInterval(print_timeAIRSTONE1, 1000);
   AIRSTONEOFF.setCounter(Z1APSPhr, Z1APSPmin, Z1APSPmin, AIRSTONEOFF.COUNT_DOWN, AIRSTONEOFFComplete);
   AIRSTONEOFF.setInterval(print_timeAIRSTONE2, 1000);
//   AIRSTONEone.setInterval(print_noneAIRSTONE, 2000);

   WLmenu[0].enabled=disabledStatus;
   tanksetting[1].enabled=disabledStatus;
   WLmenu[2].enabled=disabledStatus;
  //u8g.setFont(u8g_font_helvR08);
  u8g2.setFont(fontName);
  //u8g.setFont(u8g_font_04b_03r);
//  u8g2.firstPage();
  do {
    //    u8g2.setColorIndex(1);
    //    nav.out[0].setCursor(0,0);
    //    u8g2.drawStr( 2+x, 10, "RRD GLCD TEST");
//    nav.out[0].setCursor(0, 2);
//    nav.out[0].print(F("Welcome To ZEEPTOGREENS"));
    nav.out[0].setCursor(0, 3);
    nav.out[0].print(F("SYSTEM BOOTING..."));

  } while (u8g2.nextPage());
  delay(2000);
    // No counter
    // Just call print_none() method every 2s.
//  tNone.setInterval(print_none, 2000);
}


void loop() { 
    
//   getNutrientTL();
   r.execute();
  //  testfun();
//  wdt_reset();// make sure this gets called at least once every 8 seconds!
if (nav.changed(0)) {//only draw if menu changed for gfx device
    //change checking leaves more time for other tasks
    u8g2.firstPage();
    do nav.doOutput(); while(u8g2.nextPage());
  }
  DateTime now = rtc.now();
//  rtc.begin();
//  rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
//    sprintf(t, "%02d:%02d:%02d %02d/%02d/%02d",  now.hour(), now.minute(), now.second(), now.day(), now.month(), now.year());
//    Serial.print(F("Date/Time: "));
//    Serial.println(t);
  ///Switching funtion

digitalWrite(led, HIGH);


//digitalWrite(led1, HIGH);

  digitalWrite(LEDPIN, ledCtrl);
  digitalWrite (BUZZER, ledCtrl);
  digitalWrite (LEDRED, ledRED);
  digitalWrite (LEDGREEN, ledGREEN);
  digitalWrite (LEDBLUE, ledBLUE);
if(Autocnt==true){
  (Autocntt==false);
  (Autocnttt==false);
  digitalWrite(led3, HIGH);
  digitalWrite(led1, LOW);
  digitalWrite(led2, LOW);
    PUMPAON.run();
    PUMPAOFF.run();
    PUMPANone.run();

    PUMPBON.run();
    PUMPBOFF.run();
    PUMPBNone.run();

    AIRSTONEON.run();
    AIRSTONEOFF.run();
    AIRSTONEone.run(); 
}
if(Autocnt==false){
  digitalWrite(led3, LOW);

  digitalWrite (PUMPA, relayPA);
  digitalWrite (PUMPB, relayPB);
  digitalWrite (AIRPUMP, relayAir);
  digitalWrite (SOLVALVE, relaySol);
    }
  if (Autocnttt==true) {
    (Autocntt==false);
  (Autocnt==false);
    digitalWrite(led2, HIGH);
    digitalWrite(led1, LOW);
    digitalWrite(led3, LOW);
  } else {
   digitalWrite(led2, LOW);
  }

    if (Autocntt==true) {
       (Autocnt==false);
       (Autocnttt==false);
    digitalWrite(led1, HIGH);
    digitalWrite(led2, LOW);
    digitalWrite(led3, LOW );
  } else {
   digitalWrite(led1, LOW);
  }

//     }
 // sensorjson();
  
  nav.doInput();
  //nav.poll();
  if (nav.changed(1)) {//only draw if menu changed for gfx device
    //because this code clears the screen, if always called then screen will blink
//    u8g2.firstPage();
//    do nav.doOutput(); while (u8g2.nextPage());
  }
//  (100);//simulate other tasks 
}


///**********Counter Timer****************
void print_timePUMPA1()
{
  digitalWrite(PUMPA,HIGH);
  Serial.print("PUMP A ON TIME: ");
  Serial.println(PUMPAON.getCurrentTime()); 
}
void print_timePUMPA2()
{
  PUMPAON.pause();
 digitalWrite(PUMPA,LOW);
Serial.print("PUMP A OFF TIME: ");
Serial.println(PUMPAOFF.getCurrentTime());
}
void PUMPAONComplete()
{
  unsigned long currentMillis = millis();
 if (currentMillis - previousMillis >= interval) { 
  previousMillis = currentMillis;
//PUMPAOFF.restart();
PUMPAOFF.start();
}
}
void PUMPAOFFComplete()
{
  unsigned long currentMillis = millis();
 if (currentMillis - previousMillis >= interval) { 
  previousMillis = currentMillis;
PUMPAOFF.stop(); 
//PUMPAON.restart();
PUMPAON.start();
}
}
////**********************PUMP B**********
void print_timePUMPB1()
{
digitalWrite(PUMPB,HIGH);  
Serial.print("PUMP B ON TIME: ");
Serial.println(PUMPAON.getCurrentTime());
}
void print_timePUMPB2()
{
  digitalWrite(PUMPB, LOW); 
Serial.print("PUMP B OFF TIME: ");
Serial.println(PUMPAOFF.getCurrentTime());
}
void PUMPBONComplete()
{
//PUMPBON.stop();
//PUMPBOFF.restart();
PUMPBOFF.start();
}
void PUMPBOFFComplete()
{
PUMPBOFF.stop();
//PUMPBON.restart();
PUMPBON.start();
}
//////****AIR STONE PUMP**********
//char airon;
void print_timeAIRSTONE1()
{
 digitalWrite(AIRPUMP,HIGH);
Serial.print("AIRSTONE ON TIME: ");
Serial.println(AIRSTONEON.getCurrentTime());
////  airon =AIRSTONEON.getCurrentTime();
}
void print_timeAIRSTONE2()
{
  digitalWrite(AIRPUMP, LOW);
Serial.print("AIRSTONE OFF TIME: ");
Serial.println(AIRSTONEOFF.getCurrentTime());
}
void AIRSTONEONComplete()
{
 AIRSTONEON.stop();
 //AIRSTONEOFF.restart();
 AIRSTONEOFF.start();
}
void AIRSTONEOFFComplete()
{
 AIRSTONEOFF.stop();
 //AIRSTONEON.restart();
 AIRSTONEON.start();
}
//****************************************************************

void uva()
  {
  int uvLevel = averageAnalogRead(UVOUT);
  int refLevel = averageAnalogRead(REF_3V3);
  
  //Use the 3.3V power pin as a reference to get a very accurate output value from sensor
  float outputVoltage = 3.3 / refLevel * uvLevel;
  
  float uvIntensity = mapfloat(outputVoltage, 0.0, 31.0, 0.0, 99.0)*10; //Convert the voltage to a UV intensity level
 
  Serial.print("output: ");
  Serial.print(refLevel);
 
  Serial.print("ML8511 output: ");
  Serial.print(uvLevel);
 
  Serial.print(" / ML8511 voltage: ");
  Serial.print(outputVoltage);
 
  Serial.print(" / UV Intensity (mW/cm^2): ");
  Serial.print(uvIntensity);
// u8g2.drawStr(1, 24, "D/T:");
//    u8g2.setCursor(24, 24);
//    u8g2.print( now.day());
// lcd.setCursor(2, 2);
//     u8g2.print(" mW/cm^2");
  
  Serial.println();
  
//  delay(3000);
}
 
//Takes an average of readings on a given pin
//Returns the average
int averageAnalogRead(int pinToRead)
{
  byte numberOfReadings = 8;
  unsigned int runningValue = 0; 
 
  for(int x = 0 ; x < numberOfReadings ; x++)
    runningValue += analogRead(pinToRead);
  runningValue /= numberOfReadings;
 
  return(runningValue);
}
 
float mapfloat(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
