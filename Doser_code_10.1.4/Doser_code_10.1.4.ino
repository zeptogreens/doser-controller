/*/
 * DOSER Arduino MEGA with ESP8266 Board Code... 
 * Serial communication to esp8266
 * ArduinoJson
 * Auto Save
 * Mode Save
 * Audo Sheduler delay
 * TaskScheduler enable disable
 * 
 */

#include <Arduino.h>
#include <menu.h>
#include <menuIO/u8g2Out.h>
// #include <menuIO/encoderIn.h>
// #include <menuIO/keyIn.h>
#include <menuIO/chainStream.h>
#include <menuIO/serialOut.h>
#include <menuIO/serialIn.h>

#include <menuIO/altKeyIn.h>
#include <TimerOne.h>
#include <ClickEncoder.h>
#include <menuIO/clickEncoderIn.h>
#include <MemoryFree.h>
#include <SD.h>
#include <menuIO/keyIn.h>
#include <menuIO/keypadIn.h>
#include <ArduinoJson.h>
#include <TaskScheduler.h>
#include "DFRobot_PH.h"
#include "DFRobot_EC.h"
#include <Wire.h>

//RTC
#include <RTClib.h>
#include <Time.h>
#include <TimeLib.h>
char t[32];
#include <DallasTemperature.h>
#include <OneWire.h>
#include <GravityTDS.h>
using namespace Menu;

#define LEDPIN LED_BUILTIN

// rotary encoder pins
//#define encA    00
//#define encB    00
//#define encBtn  00
//define your keyboard ------------------------------------
const byte ROWS = 4; //four rows
const byte COLS = 4; //four columns
//define the cymbols on the buttons of the keypads
char hexaKeys[ROWS][COLS] = {
  {'1', '2', '3', 'A'},
  {'4', '5', '6', 'B'},
  {'7', '8', '9', '-'},
  {'/', '0', '*', '+'},
};

byte rowPins[ROWS] = {26, 27, 28, 29}; //connect to the row pinouts of the keypad 40,64,63,59//66,44,64,59
byte colPins[COLS] = {30, 31, 32, 33}; //connect to the column pinouts of the keypad 44,42,66,55//63,40,42,

//initialize an instance of class NewKeypad
Keypad customKeypad( makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS);

//********************************** ENCODER**********************

//ClickEncoder clickEncoder(encA, encB, encBtn, 2);
//ClickEncoderStream encStream(clickEncoder, 1);
//MENU_INPUTS(in, &encStream);
//void timerIsr() {
//  clickEncoder.service();
//}
//*********GLCD Settings ******************************************
#include <Wire.h>
#define fontName  u8g2_font_5x7_tf //u8g2_font_blipfest_07_tr  //u8g2_font_5x7_tf    //u8g2_font_artosserif8_8u //u8g2_font_7x13_mf //   //   //  u8g2_font_blipfest_07_tr(myt)
#define fontX 7
#define fontY 11
#define offsetX 0
#define offsetY 0
#define U8_Width 128
#define U8_Height 64
#define USE_HWI2C

U8G2_KS0108_128X64_F u8g2(U8G2_R0, 37, 38, 39, 40, 41, 42, 43, 44, /*enable=*/36, /*dc=*/ 34, /*cs0=*/ 45, /*cs1=*/ 46, /*cs2=*/ 35, /* reset=*/  47);   // Set R/W to low!

//EPROM

#include <EEPROMex.h>

//WatchDogTimer
//#include <avr/wdt.h>

//Define
#define BUZZER 13 //37
#define LEDPOWER 10
#define LEDRED 11
#define LEDGREEN 9
#define LEDBLUE 12

//RELA
#define TPUMPR A15
#define HEATERR A12
#define MIXERR A13 //14
#define RORELAYR A14 //Cooller RO WATER ON
// Debug and Test options
//#define _DEBUG_
#define _TEST_
#define DEBUG 0                                // change value to 1 to enable debuging using serial monitor  
#define SERIAL_PORT_SPEED 115200//9600, 19200, 38400, 57600, 115200
#define PH_PIN A1                        //pH meter Analog output to Arduino Analog Input 0

#define ONE_WIRE_BUS A2 // Pin 4 is used for SD card in ethernet shield
//#define STATUS_LED 13

//#define STEP_MODE 4 // (1: Full Step, 2: Half Step, 4: Quarter Step, ...)

#define PHLOW_STEP_PIN        25
#define PHLOW_DIR_PIN          24
//#define PHLOW_ENABLE_PIN      0

#define PHHIGH_STEP_PIN       23
#define PHHIGH_DIR_PIN         22
//#define PHHIGH_ENABLE_PIN      0    8885319433

#define EC_A_STEP_PIN         5
#define EC_A_DIR_PIN          6
//#define EC_A_ENABLE_PIN         0

#define EC_B_STEP_PIN          7  //26
#define EC_B_DIR_PIN          8 //28
//#define EC_B_ENABLE_PIN         00  //24

#define EC_C_STEP_PIN          3 //3  //36
#define EC_C_DIR_PIN            4 //4   //34
//#define EC_C_ENABLE_PIN         00  //30

//************SD Card **********************************************///
#define SD_DETECT_PIN   53

#define SDSS            53 //53
File sdcard_file;

struct Config {
  char hostname[64];
  int port;
};
const char *filename = "/config.txt";  // <- SD library uses 8.3 filenames
Config config;                         // <- global configuration object

///********************************************************************
GravityTDS gravityTds;

OneWire oneWireBus(ONE_WIRE_BUS);
DallasTemperature sensors (&oneWireBus);

RTC_DS3231 rtc; //for date time
char daysOfTheWeek[7][12] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};

uint16_t hourupg;
uint16_t minupg;
uint16_t secslive;
uint16_t Monthlive;
uint16_t Datelive;
uint16_t Yearlive;

float phsetvalue ;
float phSetHysteris ;
int phplus;
int phmin;
int ECA;
int ECB ;
int ECC ;
float temp1 = 0;


//**** hysteris****//
float pH ; //generates the value of pH

float phtest;
float phUp = 2;
float phDown = 2; //

float EC; //generates the value of EC
int ECUp = 2;
int ECDown = 2;


byte ECSetpoint;
byte ECHysterisMin;
byte ECHysterisPlus;


bool StopPHHys;
int autoon;
//bool autostart=autoon;
bool autostart=autoon;
bool EcCalStart;
int Scheon;
bool ScheduleAuto=Scheon;
bool Resetfun;

int Speed = 750;

# define Offsetec 0.00 //deviation compensate
# define Offset 0.00 //deviation compensate
# define LED 13
# define samplingInterval 20
# define printInterval 800
# define ArrayLenth 10 //times of collection

int pHArray[ArrayLenth]; //Store the average value of the sensor feedback
int pHArrayIndex = 0;

int ECArray[ArrayLenth]; //Store the average value of the sensor feedback
int ECArrayIndex = 0;

//float Setpoint;
float HysterisMin;
float HysterisPlus;
//float SetHysteris;


int previousMillis = 0; //
int pinHighTime = 100; //
int pinLowTime = 7500; // OLD 7500
int pinTime = 100; //

int ECpreviousMillis = 0; //             |
int ECpinHighTime = 100; //             |
int ECpinLowTime = 7500; //             |
int ECpinTime = 100; //

//*******Sensor Int***********///
//*********DO SENSOR************//

#define DO_PIN A13

String inString;
#define VREF 5000    //VREF (mv)
#define ADC_RES 1024 //ADC Resolution

//Single-point calibration Mode=0
//Two-point calibration Mode=1
#define TWO_POINT_CALIBRATION 0

//Single point calibration needs to be filled CAL1_V and CAL1_T
#define CAL1_V (1600) //mv
#define CAL1_T (25)   //℃
//Two-point calibration needs to be filled CAL2_V and CAL2_T
//CAL1 High temperature point, CAL2 Low temperature point
#define CAL2_V (1300) //mv
#define CAL2_T (15)   //℃
float DOvalue;

const uint16_t DO_Table[41] = {
  14460, 14220, 13820, 13440, 13090, 12740, 12420, 12110, 11810, 11530,
  11260, 11010, 10770, 10530, 10300, 10080, 9860, 9660, 9460, 9270,
  9080, 8900, 8730, 8570, 8410, 8250, 8110, 7960, 7820, 7690,
  7560, 7430, 7300, 7180, 7070, 6950, 6840, 6730, 6630, 6530, 6410
};

uint8_t Temperaturet;
uint16_t ADC_Raw;
uint16_t ADC_Voltage;
uint16_t DO;

int16_t readDO(uint32_t voltage_mv, uint8_t temperature_c)
{
#if TWO_POINT_CALIBRATION == 0
  uint16_t V_saturation = (uint32_t)CAL1_V + (uint32_t)35 * temperature_c - (uint32_t)CAL1_T * 35;
  return (voltage_mv * DO_Table[temperature_c] / V_saturation);
#else
  uint16_t V_saturation = (int16_t)((int8_t)temperature_c - CAL2_T) * ((uint16_t)CAL1_V - CAL2_V) / ((uint8_t)CAL1_T - CAL2_T) + CAL2_V;
  return (voltage_mv * DO_Table[temperature_c] / V_saturation);
#endif
}

//*******EC SENSOR************///

#define EC_PIN A0
#define VREF 5.0 // analog reference voltage(Volt) of the ADC
#define SCOUNT 30 // sum of sample point
int analogBuffer[SCOUNT]; // store the analog value in the array, readfrom ADC
int analogBufferTemp[SCOUNT];
int analogBufferIndex = 0, copyIndex = 0;
float averageVoltage = 0, tdsValue = 0, temperature ;
float  voltagePH, voltageEC, phValue, ecValue;
float phValueavrg;
float ECValueavrg;
DFRobot_PH ph;
DFRobot_EC ec;
float nutrientTDS = 0;
float nutrientEC = 0;
float nutrientCF = 0;
float nutrientTemperature;
#define READ_TEMP (nutrientTemperature) //Current water temperature ℃, Or temperature sensor function
float nutrientPH;
float Fahrenheit = 0;
float Status = 0;
unsigned long currentTime;
unsigned long int avgValue;  //Store the average value of the sensor feedback
int buf[10], temp;

//***EPROM***//
//*PH*//
uint16_t EeprompHSetpoint = 31;      //location of pHSetpoint in Eeprom
uint16_t EepromSetpHHysteris = 41;   //location of SetpHHysteris in Eeprom

//*EC*//
uint16_t EepromECSetpoint = 51;      //location of ECSetpoint in Eeprom
uint16_t EepromSetECHysteris = 61;   //location of SetECHysteris in Eeprom

//***Time***save eprom adress///

int Eepromhur = 200;
int Eeprommin = 203;

int Eepromhur1 = 206;
int Eeprommin1 = 209;

int Eepromhur2 = 212;
int Eeprommin2 = 215;

int Eepromhur3 = 218;
int Eeprommin3 = 221;

int Eepromhur4 = 224;
int Eeprommin4 = 227;

int Eepromhur5 = 230;
int Eeprommin5 = 233;

int Eepromhur6 = 380;
int Eeprommin6 = 383;

int Eepromhur7 = 386;
int Eeprommin7 = 389;

int Eepromhur8 = 393;
int Eeprommin8 = 396;

int EepromDate = 240;
int EepromMonth = 245;
int EepromYear = 250;

byte EeprompHs1 = 300;
byte EeprompHs2 = 305;
byte EeprompHs3 = 310;

byte EeprompHs4 = 315;
byte EeprompHs5 = 320;
byte EeprompHs6 = 399;

byte EeprompHs7 = 402;
byte EeprompHs8 = 405;


int EepromECs1 = 325;
int EepromECs2 = 330;
int EepromECs3 = 335;

int EepromECs4 = 340;
int EepromECs5 = 344;
int EepromECs6 = 408;

int EepromECs7 = 411;
int EepromECs8 = 413;
//********************** NL ***********
//int EepromNTHset = 90;
//int EepromNTVset = 91;
//
// define menu colors --------------------------------------------------------
//each color is in the format:
//  {{disabled normal,disabled selected},{enabled normal,enabled selected, enabled editing}}
// this is a monochromatic color table
const colorDef<uint8_t> colors[6] MEMMODE = {
  {{0, 0}, {0, 1, 1}}, //bgColor
  {{1, 1}, {1, 0, 0}}, //fgColor
  {{1, 1}, {1, 0, 0}}, //valColor
  {{1, 1}, {1, 0, 0}}, //unitColor
  {{0, 1}, {0, 0, 1}}, //cursorColor
  {{1, 1}, {1, 0, 0}}, //titleColor
};

// *********************************************************************
// Task Scheduler
// *********************************************************************

// objects
Scheduler r;

// Callback methods prototypes
void Task_Serial_Blink_Example();
void Task_LCDMenuLib();

// Tasks
Task t0(1000,  TASK_FOREVER, &dateandtime, &r);
Task t1(5000,  TASK_FOREVER, &getNutrientPHavg, &r, true);//getNutrientPHavg
Task t2(6000,  TASK_FOREVER, &getNutrientECavg, &r, true); //getNutrientEC //getNutrientTDS //getNutrientECavg
Task t3(7000,  TASK_FOREVER, &getNutrientTemperature, &r);
Task t4(60000,  TASK_FOREVER, &sensorjson, &r);
Task t5(50000,  TASK_FOREVER, &getSDdata, &r);
//Task t6(5000,  TASK_FOREVER, &manualdosemin, &r);
//Task t7(5500,  TASK_FOREVER, &manualdoseplus, &r);
Task t8(30000,  TASK_FOREVER, &AutoFun, &r, true);//AutoFun
Task t9(31000,  TASK_FOREVER, &scheduleFun, &r, true);//scheduleFun

//Task t10(20000EC Stable,  TASK_FOREVER, &scheduleFun, &r);
  Task t11(9000,  TASK_FOREVER, &readalldata, &r);
//  Task t12(3000,  TASK_FOREVER, &Task_CheckChangeInSettings, &r);

result doAlert(eventMask e, prompt &item);

result showEvent(eventMask e, navNode& nav, prompt& item) {
  Serial.print("event: ");
  Serial.println(e);
  return proceed;
}


result action1(eventMask e, navNode& nav, prompt &item) {
  Serial.print(e);
  Serial.println(" action1 executed, proceed menu");
  Serial.flush();
  return proceed;
}



result home(eventMask e, navNode& nav, prompt &item) {
  Serial.print(e);
  Serial.print(" home");
  return quit;
}


int ledCtrl = LOW;

result myLedOn() {
  ledCtrl = HIGH;
  digitalWrite (BUZZER, HIGH);
  digitalWrite(LEDGREEN, HIGH);
  return proceed;
}
result myLedOff() {

  ledCtrl = LOW;
  digitalWrite (BUZZER, LOW);
  digitalWrite(LEDGREEN, HIGH);
  return proceed;
}
TOGGLE(ledCtrl, setLed, "LED: ", doNothing, noEvent, noStyle
       , VALUE("On", HIGH, doNothing, noEvent)
       , VALUE("Off", LOW, doNothing, noEvent)
      );


//************************

int Eepromautostatus =415;

void autosavefun(){
  if (autoon==1){
    autostart=true;
    }
    else{
      autostart=false;
      }
  }
TOGGLE(autostart, AutoDose, " DOSING :", doNothing, enterEvent, noStyle //,doExit,enterEvent,noStyle
       , VALUE("STOP", true, autosaveon, enterEvent)
       , VALUE("START", false, autosaveon, enterEvent) //DoserautoOn
      );
result autosaveon(){
  EEPROM.write(Eepromautostatus, autostart);
  }

void EepromReadauto()
{
  autoon = EEPROM.read(Eepromautostatus);
// Serial.print ("on");
//  Serial.println (autoon);
  return proceed;
}
//*****************************************************************


byte EepromSchetatus =417;

void Schesavefun(){
  if (Scheon==1){
    ScheduleAuto=true;
    }
    else{
      ScheduleAuto=false;
      }
  }
TOGGLE(ScheduleAuto, AutoSchedule, " SCHEDULE :", doNothing, enterEvent, noStyle //,doExit,enterEvent,noStyle
       , VALUE("STOP", true, Schesaveon, enterEvent)
       , VALUE("START", false, Schesaveon, enterEvent)
      );
result Schesaveon(){
  EEPROM.write(EepromSchetatus, ScheduleAuto);
  }

void EepromReadSche()
{
  Scheon = EEPROM.read(EepromSchetatus);
// Serial.print ("Sche--on");
//  Serial.println (Scheon);
  return proceed;
}

//****************************************************************      
bool SDCARD = true;
TOGGLE(SDCARD, SDcardOT, "SD CARED :", doNothing, noEvent, noStyle //,doExit,enterEvent,noStyle
       , VALUE("ENABLE", true, doNothing, enterEvent)
       , VALUE("DISABLE", false, doExit, noEvent)
      );
      

int selTest = 0;
SELECT(selTest, selMenu, "SELECT", doNothing, noEvent, noStyle
       , VALUE("Zero", 0, doNothing, noEvent)
       , VALUE("One", 1, doNothing, noEvent)
       , VALUE("Two", 2, doNothing, noEvent)
      );


result schedautoOn() {
  ScheduleAuto = true;
  return proceed;
}
result schedautoOff() {
  ScheduleAuto = false;
  return proceed;
}

result SDON() {
  SDCARD = true;
  return proceed;
}
result SDOFF() {
  SDCARD = false;
  return proceed;
}

//********************************PH CAL*************************************************************************
float SetpHHysteris;
float pHSetpoint ;


///*********EPROM*******************************************************************************************///

result phsetsave() {
  EEPROM.writeFloat(EeprompHSetpoint, pHSetpoint);  // 6.5 pH for tomato growth

  return proceed;
}

result phsetHyssave() {

  EEPROM.writeFloat(EepromSetpHHysteris, phSetHysteris);
  return proceed;
}

void EepromReadPHCal()
{
  pHSetpoint = EEPROM.readFloat(EeprompHSetpoint);
  phSetHysteris = EEPROM.readFloat(EepromSetpHHysteris);
}
//**************************EC cal**************************************************************************************

float ECSetHysteris;
float ECsetvalue;
///*********EPROM********************///
result ECsetsave() {
  EEPROM.writeFloat(EepromECSetpoint, ECsetvalue);

  return proceed;
}
result ECsethyssave() {

  EEPROM.writeFloat(EepromSetECHysteris, ECSetHysteris);
  return proceed;
}
void EepromReadEC()
{
  ECsetvalue = EEPROM.readFloat(EepromECSetpoint);
  ECSetHysteris = EEPROM.readFloat(EepromSetECHysteris);
}
//************************************************************************************************************
int PHUPR = 1;
SELECT(PHUPR, PHUPRatio, "PH UP :", doNothing, noEvent, noStyle
       , VALUE("1", 1, doNothing, noEvent)
       , VALUE("2", 2, doNothing, noEvent)
       , VALUE("3", 3, doNothing, noEvent)
       , VALUE("4", 4, doNothing, noEvent)
       , VALUE("5", 5, doNothing, noEvent)

      );
int PHDOWNR = 1;
SELECT(PHDOWNR, PHDOWNRatio, "PH DOWN :", doNothing, noEvent, noStyle
       , VALUE("1", 1, doNothing, noEvent)
       , VALUE("2", 2, doNothing, noEvent)
       , VALUE("3", 3, doNothing, noEvent)
       , VALUE("4", 4, doNothing, noEvent)
       , VALUE("5", 5, doNothing, noEvent)

      );


int ECRatioA = 1;
SELECT(ECRatioA, ECARatio, "EC A :", doNothing, noEvent, noStyle
       , VALUE("1", 1, doNothing, noEvent)
       , VALUE("2", 2,
       
       doNothing, noEvent)
       , VALUE("3", 3, doNothing, noEvent)
       , VALUE("4", 4, doNothing, noEvent)
       , VALUE("5", 5, doNothing, noEvent)

      );

int ECRatioB = 1;
SELECT(ECRatioB, ECBRatio, "EC B :", doNothing, noEvent, noStyle
       , VALUE("1", 1, doNothing, noEvent)
       , VALUE("2", 2, doNothing, noEvent)
       , VALUE("3", 3, doNothing, noEvent)
       , VALUE("4", 4, doNothing, noEvent)
       , VALUE("5", 5, doNothing, noEvent)

      );

int ECRatioC = 1;
SELECT(ECRatioC, ECCRatio, "EC C :", doNothing, noEvent, noStyle
       , VALUE("1", 1, doNothing, noEvent)
       , VALUE("2", 2, doNothing, noEvent)
       , VALUE("3", 3, doNothing, noEvent)
       , VALUE("4", 4, doNothing, noEvent)
       , VALUE("5", 5, doNothing, noEvent)

      );

MENU(ECRatio, " MIXING RATIO", showEvent, noEvent, noStyle

     , SUBMENU(PHUPRatio)
     , SUBMENU(PHDOWNRatio)
     , SUBMENU(ECARatio)
     , SUBMENU(ECBRatio)
     , SUBMENU(ECCRatio)
     , EXIT("<Back")
    );
//******************************  MANUAL DOSING ******************************************************************************

MENU(MDoseMenu, " MANUAL ", showEvent, noEvent, noStyle

     , FIELD(phmin, " pH DOWN", "  ml", 0, 2000, 100, 10, manualdose, enterEvent, wrapStyle)
     , OP("       START DOSING ", manualdoseplus, enterEvent)

     , FIELD(phplus, " pH UP  ", "  ml", 0, 2000, 100, 10, manualdose, enterEvent, wrapStyle)
     // , OP("        save", manualdose, enterEvent)
     , OP("        START DOSING ",  manualdosemin, enterEvent)

     , FIELD(ECA, " NUTRIENT A ", "  ml", 0, 2000, 100, 10, doNothing, enterEvent, wrapStyle)
     , OP("       START DOSING ", manualdoseEcA, enterEvent)

     , FIELD(ECB, " NUTRIENT B  ", "  ml", 0, 2000, 100, 10, doNothing, enterEvent, wrapStyle)
     , OP("       START DOSING ", manualdoseEcB, enterEvent)

     , FIELD(ECC, " NUTRIENT C  ", "  ml", 0, 2000, 100, 10, doNothing, enterEvent, wrapStyle)
     , OP("       START DOSING ", manualdoseEcC, enterEvent)

     //, OP("       RESET", MDreset, enterEvent)
     //, OP("       START DOSING", Mdose, enterEvent)
     , EXIT("<Back")
    );

///*********EPROM********************///

int Eepromphplus = 350;
int Eepromphmin = 360;
int EepromECA = 365;
int EepromECB = 370;
int EepromECC = 375;

result manualdose() {
  EEPROM.writeFloat(Eepromphplus, phplus);
  EEPROM.writeFloat(Eepromphmin, phmin);
  //  EEPROM.writeFloat(EepromECA, ECA);
  //  EEPROM.writeFloat(EepromECB, ECB);
  //  EEPROM.writeFloat(EepromECC, ECC);

  EepromReadMD();
  return proceed;
}

void EepromReadMD()
{
  phplus = EEPROM.readFloat(Eepromphplus);
  phmin = EEPROM.readFloat(Eepromphmin);
  //  ECA = EEPROM.readFloat(EepromECA);
  //  ECB = EEPROM.readFloat(EepromECB);
  //  ECC = EEPROM.readFloat(EepromECC);
}


result MDreset() {

  {
    float phplus, phmin, ECA, ECB, ECC = 0;
  }
  return ;

}

//int oneml = 125;
//int onemlphdown = 320; //320
//int onemlphup = 340;  //370
//int onemlA = 320; //315
//int onemlB = 320; // 380
//int onemlC = 320; //315

result manualdosemin() {
  digitalWrite(LEDBLUE, HIGH);
  int RPHUP =  PHUPR * 230;
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis > pinTime) {
    previousMillis = currentMillis;
    digitalWrite(PHHIGH_DIR_PIN, LOW);
    // Motor rotate with ml input
    for ( int y = 0; y < phplus; y++) {
      for ( int x = 0; x < RPHUP; x++) //(onemlphdown * phmin) //420
      {
        digitalWrite(PHHIGH_STEP_PIN, HIGH);
        delayMicroseconds(Speed);
        digitalWrite(PHHIGH_STEP_PIN, LOW);
        delayMicroseconds(Speed);

      }
    }
    pinTime = pinLowTime;
  }
}

result manualdoseplus() {
  digitalWrite(LEDBLUE, HIGH);
  int RPHDOWN =  PHDOWNR * 230;
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis > pinTime) {
    previousMillis = currentMillis;
    digitalWrite(PHLOW_DIR_PIN, LOW);
    for ( int y = 0; y < phmin ; y++) {
      for (int x = 0; x < RPHDOWN ; x++) //(onemlphup * phplus)390
      {
        digitalWrite(PHLOW_STEP_PIN, HIGH);
        delayMicroseconds(Speed);
        digitalWrite(PHLOW_STEP_PIN, LOW);
        delayMicroseconds(Speed);
      }

    }
    pinTime = pinLowTime;
  }
}
int ECRA;
result manualdoseEcA() {
  digitalWrite(LEDBLUE, HIGH);
  int ECRA =  ECRatioA * 230;
  digitalWrite(EC_A_DIR_PIN, LOW);

  // Motor rotate with ml input
  for ( int z = 0; z <  ECA; z++) {
    for (int x = 0; x < ECRA; x++)//375
    {
      digitalWrite(EC_A_STEP_PIN, HIGH);
      delayMicroseconds(Speed);

      digitalWrite(EC_A_STEP_PIN, LOW);
      delayMicroseconds(Speed);

    }
  }
}
int ECRB;
result manualdoseEcB() {
  digitalWrite(LEDBLUE, HIGH);
  int ECRB =  ECRatioB * 240;
  digitalWrite(EC_B_DIR_PIN, LOW);
  for ( int b = 0; b <  ECB; b++) {
    // Spin motor slowly
    for (int x = 0; x < ECRB; x++) //375
    {
      digitalWrite(EC_B_STEP_PIN, HIGH);
      delayMicroseconds(Speed);
      digitalWrite(EC_B_STEP_PIN, LOW);
      delayMicroseconds(Speed);
    }
    //delay(1000);
    //  return proceed;
  }
}
int ECRC;
result manualdoseEcC() {
  digitalWrite(LEDBLUE, HIGH);
  int ECRC =  ECRatioC * 230;
  digitalWrite(EC_C_DIR_PIN, LOW);
  for ( int y = 0; y <  ECC; y++) {
    // Spin motor slowly
    for (int x = 0; x < ECRC ; x++) //385
    {
      digitalWrite(EC_C_STEP_PIN, HIGH);
      delayMicroseconds(Speed);
      digitalWrite(EC_C_STEP_PIN, LOW);
      delayMicroseconds(Speed);
    }
    //delay(1000);
    //  return proceed;
  }
}

//*********************************************************************************************************

int ledRED = LOW;
int ledGREEN = LOW;
int ledBLUE = LOW;

int TPRelay = LOW ;
int HRelay = LOW ;
int MRelay = LOW;
int RRelay = LOW ;

bool probeEc;
bool probeTemp;
bool probeDo;

//**********LED-*******************
TOGGLE(ledRED, redLED, "       RED : ", doNothing, noEvent, noStyle
       , VALUE("On", HIGH, doNothing, noEvent)
       , VALUE("Off", LOW, doNothing, noEvent)
      );


TOGGLE(ledGREEN, greenLED, "       GREEN : ", doNothing, noEvent, noStyle
       , VALUE("On", HIGH, doNothing, noEvent)
       , VALUE("Off", LOW, doNothing, noEvent)
      );

TOGGLE(ledBLUE, blueLED, "       BLUE : ", doNothing, noEvent, noStyle
       , VALUE("On", HIGH, doNothing, noEvent)
       , VALUE("Off", LOW, doNothing, noEvent)
      );

//*************************Probe****************************************
bool probePh;
TOGGLE(probePh, pHline, "     pH   : ", doNothing, noEvent, noStyle
       , VALUE("Enable", true, doNothing, noEvent)
       , VALUE("Disable", false, doNothing, noEvent)
      );

TOGGLE(probeEc, Ecline, "     EC   : ", doNothing, noEvent, noStyle
       , VALUE("Enable", true, doNothing, noEvent)
       , VALUE("Disable", false, doNothing, noEvent)
      );

TOGGLE(probeTemp, Templine, "     TEMP : ", doNothing, noEvent, noStyle
       , VALUE("Enable", true, doNothing, noEvent)
       , VALUE("Disable", false, doNothing, noEvent)
      );

TOGGLE(probeDo, Doline, "     DO   : ", doNothing, noEvent, noStyle
       , VALUE("Enable", true, doNothing, noEvent)
       , VALUE("Disable", false, doNothing, noEvent)
      );

//***********RELAY***********************
TOGGLE(TPRelay, tpump, "     SUB PUMP : ", doNothing, noEvent, noStyle
       , VALUE("On", HIGH, doNothing, noEvent)
       , VALUE("Off", LOW, doNothing, noEvent)
      );


TOGGLE(HRelay, heater, "     HEATER : ", doNothing, noEvent, noStyle
       , VALUE("On", HIGH, doNothing, noEvent)
       , VALUE("Off", LOW, doNothing, noEvent)
      );

TOGGLE(MRelay, mixer, "     MIXER : ", doNothing, noEvent, noStyle
       , VALUE("On", HIGH, doNothing, noEvent)
       , VALUE("Off", LOW, doNothing, noEvent)
      );

TOGGLE(RRelay, relay, "     RELAY : ", doNothing, noEvent, noStyle
       , VALUE("On", HIGH, doNothing, noEvent)
       , VALUE("Off", LOW, doNothing, noEvent)
      );
//*********************************************************************************************************

bool Autosave= HIGH;
TOGGLE(Autosave, Asave, " AUTO SAVE : ", doNothing, noEvent, noStyle
       , VALUE("YES", HIGH, doNothing, noEvent)
       , VALUE("NO", LOW, doNothing, noEvent)
      );


//****************TIME AND DATE**************************************************************************************************
uint16_t S1Hr;
uint16_t S1Min;
//unsigned long secs;

uint16_t S2Hr;
uint16_t S2Min;
//unsigned long secs0;

uint16_t S3Hr;
uint16_t S3Min;
//unsigned long secs1;

uint16_t S4Hr;
uint16_t S4Min;
//unsigned long secs1;

uint16_t S5Hr;
uint16_t S5Min;
//unsigned long secs1;

uint16_t S6Hr;
uint16_t S6Min;

uint16_t S7Hr;
uint16_t S7Min;

uint16_t S8Hr;
uint16_t S8Min;


//define a pad style menu (single line menu)
//here with a set of fields to enter a date in YYYY/MM/DD format
altMENU(menu, timeMenu0, "EDIT", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(S1Hr, ":", " (HH)", 0, 23, 1, 0, Shift1Clockhr, enterEvent, wrapStyle)
        , FIELD(S1Min, "", " (MM)", 0, 59, 10, 1, Shift1Clockmin, enterEvent, wrapStyle)

        //,FIELD(secs,"","",0,59,10,1,doNothing,noEvent,wrapStyle)
       );

altMENU(menu, timeMenu1, "EDIT", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(S2Hr, ":", " (HH)", 0, 23, 1, 0, Shift2Clock, enterEvent, wrapStyle)
        , FIELD(S2Min, "", " (MM)", 0, 59, 10, 1, Shift2Clock, enterEvent, wrapStyle)
        //  ,FIELD(secs,"","",0,59,10,1,doNothing,enterEvent,wrapStyle)
       );
altMENU(menu, timeMenu2, "EDIT", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(S3Hr, ":", " (HH)", 0, 23, 1, 0, Shift3Clock, enterEvent, wrapStyle)
        , FIELD(S3Min, "", " (MM)", 0, 59, 10, 1, Shift3Clock, enterEvent, wrapStyle)
        //  ,FIELD(secs,"","",0,59,10,1,doNothing,noEvent,wrapStyle)
       );
altMENU(menu, timeMenu3, "EDIT", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(S4Hr, ":", " (HH)", 0, 23, 1, 0, Shift4Clock, enterEvent, wrapStyle)
        , FIELD(S4Min, "", " (MM)", 0, 59, 10, 1, Shift4Clock, enterEvent, wrapStyle)
        //  ,FIELD(secs,"","",0,59,10,1,doNothing,noEvent,wrapStyle)
       );
altMENU(menu, timeMenu4, "EDIT", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(S5Hr, ":", " (HH)", 0, 23, 1, 0, Shift5Clock, enterEvent, wrapStyle)
        , FIELD(S5Min, "", " (MM)", 0, 59, 10, 1, Shift5Clock, enterEvent, wrapStyle)
        //  ,FIELD(secs,"","",0,59,10,1,doNothing,noEvent,wrapStyle)
       );

altMENU(menu, timeMenu5, "EDIT", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(S6Hr, ":", " (HH)", 0, 23, 1, 0, Shift6Clock, enterEvent, wrapStyle)
        , FIELD(S6Min, "", " (MM)", 0, 59, 10, 1, Shift6Clock, enterEvent, wrapStyle)
        //  ,FIELD(secs,"","",0,59,10,1,doNothing,noEvent,wrapStyle)
       );

altMENU(menu, timeMenu6, "EDIT", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(S7Hr, ":", " (HH)", 0, 23, 1, 0, Shift7Clock, enterEvent, wrapStyle)
        , FIELD(S7Min, "", " (MM)", 0, 59, 10, 1, Shift7Clock, enterEvent, wrapStyle)
        //  ,FIELD(secs,"","",0,59,10,1,doNothing,noEvent,wrapStyle)
       );

altMENU(menu, timeMenu7, "EDIT", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(S8Hr, ":", " (HH)", 0, 23, 1, 0, Shift8Clock, enterEvent, wrapStyle)
        , FIELD(S8Min, "", " (MM)", 0, 59, 10, 1, Shift8Clock, enterEvent, wrapStyle)
        //  ,FIELD(secs,"","",0,59,10,1,doNothing,noEvent,wrapStyle)
       );
//******************************************Shift Time Setting Save ********************************************************

result Shift1Clockhr() {
  EEPROM.write(Eepromhur1, S1Hr);

  return proceed;
}
result Shift1Clockmin() {

  EEPROM.write(Eeprommin1, S1Min);
  return proceed;
}
void EepromReadTime1()
{
  S1Hr = EEPROM.read(Eepromhur1);
  S1Min = EEPROM.read(Eeprommin1);
  return proceed;
}
//=================================================
//**********************SCHEDULER ALARM*******************
result Shift2Clock() {
  EEPROM.write(Eepromhur2, S2Hr);
  EEPROM.write(Eeprommin2, S2Min);
  return proceed;
}

void EepromReadTime2()
{
  S2Hr = EEPROM.read(Eepromhur2);
  S2Min = EEPROM.read(Eeprommin2);
  return proceed;
}
//=================================================
//**********************SCHEDULER ALARM*******************
result Shift3Clock() {
  EEPROM.write(Eepromhur3, S3Hr);
  EEPROM.write (Eeprommin3, S3Min);
  return proceed;
}
void EepromReadTime3()
{
  S3Hr = EEPROM.read(Eepromhur3);
  S3Min = EEPROM.read(Eeprommin3);
  return proceed;
}

//**********************SCHEDULER ALARM*******************
result Shift4Clock() {
  EEPROM.write(Eepromhur4, S4Hr);
  EEPROM.write (Eeprommin4, S4Min);
  return proceed;
}
void EepromReadTime4()
{
  S4Hr = EEPROM.read(Eepromhur4);
  S4Min = EEPROM.read(Eeprommin4);
  return proceed;
}

//**********************SCHEDULER ALARM*******************
result Shift5Clock() {
  EEPROM.write(Eepromhur5, S5Hr);
  EEPROM.write(Eeprommin5, S5Min);
  return proceed;
}
void EepromReadTime5()
{
  S5Hr = EEPROM.read(Eepromhur5);
  S5Min = EEPROM.read(Eeprommin5);
  return proceed;
}

//**********************SCHEDULER ALARM*******************
result Shift6Clock() {
  EEPROM.write(Eepromhur6, S6Hr);
  EEPROM.write(Eeprommin6, S6Min);
  return proceed;
}
void EepromReadTime6()
{
  S6Hr = EEPROM.read(Eepromhur6);
  S6Min = EEPROM.read(Eeprommin6);
  return proceed;
}

//**********************SCHEDULER ALARM*******************
result Shift7Clock() {
  EEPROM.write(Eepromhur7, S7Hr);
  EEPROM.write(Eeprommin7, S7Min);
  return proceed;
}
void EepromReadTime7()
{
  S7Hr = EEPROM.read(Eepromhur7);
  S7Min = EEPROM.read(Eeprommin7);
  return proceed;
}

//**********************SCHEDULER ALARM*******************
result Shift8Clock() {
  EEPROM.write(Eepromhur8, S8Hr);
  EEPROM.write(Eeprommin8, S8Min);
  return proceed;
}
void EepromReadTime8()
{
  S8Hr = EEPROM.read(Eepromhur8);
  S8Min = EEPROM.read(Eeprommin8);
  return proceed;
}
//*********************************************************************
void allReadTime() {
  EepromReadTime1();
  EepromReadTime2();
  EepromReadTime3();
  EepromReadTime4();
  EepromReadTime5();
  EepromReadTime6();
  EepromReadTime7();
  EepromReadTime8();


}

//******************************************Time Date Setting**************************************************************

altMENU(menu, timeMenu, " TIME ", doNothing, anyEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(hourupg, "", ":", 0, 23, 1, 0, doNothing, anyEvent, noStyle)
        , FIELD(minupg, "", ":", 0, 59, 10, 1, doNothing, anyEvent, wrapStyle)
        , FIELD(secslive, "", "", 0, 59, 10, 1, doNothing, noEvent, wrapStyle)
       );


altMENU(menu, dateMenu, " DATE ", doNothing, anyEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Datelive, "", ":", 0, 31, 1, 0, doNothing, anyEvent, noStyle)
        , FIELD(Monthlive, "", ":", 0, 12, 10, 1, doNothing, anyEvent, wrapStyle)
        , FIELD(Yearlive, "", "", 0, 2050, 10, 1, doNothing, anyEvent, wrapStyle)
       );


MENU(S1St, " SLOT 1 TIME SET ", showEvent, enterEvent, noStyle

     , SUBMENU(timeMenu0)
     , OP("      SAVE"  , EepromReadTime1, enterEvent)

     , EXIT("<Back")
    );

MENU(S2St, " SLOT 2 TIME SET ", showEvent, enterEvent, noStyle

     , SUBMENU(timeMenu1)
     , OP("       SAVE"  , EepromReadTime2, enterEvent)

     , EXIT("<Back")
    );
MENU(S3St, " SLOT 3 TIME SET ", showEvent, enterEvent, noStyle

     , SUBMENU(timeMenu2)
     , OP("      SAVE"  , EepromReadTime3, enterEvent)

     , EXIT("<Back")
    );

MENU(S4St, " SLOT 4 TIME SET ", showEvent, enterEvent, noStyle

     , SUBMENU(timeMenu3)
     , OP("       SAVE"  , EepromReadTime4, enterEvent)

     , EXIT("<Back")
    );
MENU(S5St, " SLOT 5 TIME SET", showEvent, enterEvent, noStyle

     , SUBMENU(timeMenu4)
     , OP("       SAVE"  , EepromReadTime5, enterEvent)

     , EXIT("<Back")
    );

MENU(S6St, " SLOT 6 TIME SET", showEvent, enterEvent, noStyle

     , SUBMENU(timeMenu5)
     , OP("       SAVE"  , EepromReadTime6, enterEvent)

     , EXIT("<Back")
    );

MENU(S7St, " SLOT 7 TIME SET", showEvent, enterEvent, noStyle

     , SUBMENU(timeMenu6)
     , OP("       SAVE"  , EepromReadTime7, enterEvent)

     , EXIT("<Back")
    );

MENU(S8St, " SLOT 8 TIME SET", showEvent, enterEvent, noStyle

     , SUBMENU(timeMenu7)
     , OP("       SAVE"  , EepromReadTime8, enterEvent)

     , EXIT("<Back")
    );

float pHs1;
float ECs1;
float temps;
float dos1;

float pHs2;
float ECs2;
float temps2;
float dos2;

float pHs3;
float ECs3;
float temps3;
float dos3;

float pHs4;
float ECs4;
float dos4;

float pHs5;
float ECs5;
float dos5;

float pHs6;
float ECs6;
float dos6;

float pHs7;
float ECs7;
float dos7;

float pHs8;
float ECs8;
float dos8;

MENU(slot1, " SLOT 1", showEvent, anyEvent, wrapStyle
     , SUBMENU(S1St)
     , FIELD(pHs1, "  pH :    ", " ", 0, 14, 1, 0.10, saveslot1, enterEvent, wrapStyle)
     , FIELD(ECs1, "  EC :    ", " mS/cm", 0, 5, 1, 0.01, saveslot1, enterEvent, wrapStyle)
     , FIELD(dos1, "  DO :    ", " PPM", 0, 1000, 10, 100, doNothing, noEvent, wrapStyle)
     , OP("         SAVE", S1Read, enterEvent)
     , EXIT("<Back")
    );

MENU(slot2, " SLOT 2", showEvent, anyEvent, wrapStyle
     , SUBMENU(S2St)
     , FIELD(pHs2, "  pH  :    ", " ", 0, 14, 1, 0.10, saveslot2, enterEvent, wrapStyle)
     , FIELD(ECs2, "  EC  :    ", " mS/cm", 0, 5, 1, 0.01, saveslot2, enterEvent, wrapStyle)
     , FIELD(dos2, "  DO  :    ", " PPM", 0, 1000, 10, 100, doNothing, noEvent, wrapStyle)
     , OP("       SAVE", S2Read, enterEvent)
     , EXIT("<Back")
    );

MENU(slot3, " SLOT 3", showEvent, anyEvent, wrapStyle
     , SUBMENU(S3St)
     , FIELD(pHs3, "  pH  :    ", " ", 0, 14, 1, 0.10, saveslot3, enterEvent, wrapStyle)
     , FIELD(ECs3, "  EC  :    ", " mS/cm", 0, 5, 1, 0.01, saveslot3, enterEvent, wrapStyle)
     , FIELD(dos3, "  DO  :    ", " PPM", 0, 1000, 10, 100, doNothing, noEvent, wrapStyle)
     , OP("       SAVE", S3Read, enterEvent)
     , EXIT("<Back")
    );
MENU(slot4, " SLOT 4", showEvent, anyEvent, wrapStyle
     , SUBMENU(S4St)
     , FIELD(pHs4, "  pH  :    ", " ", 0, 14, 1, 0.10, saveslot4, enterEvent, wrapStyle)
     , FIELD(ECs4, "  EC  :    ", " mS/cm", 0, 5, 1, 0.01, saveslot4, enterEvent, wrapStyle)
     , FIELD(dos4, "  DO  :    ", " PPM", 0, 1000, 10, 100, doNothing, noEvent, wrapStyle)
     , OP("       SAVE", S4Read, enterEvent)
     , EXIT("<Back")
    );
MENU(slot5, " SLOT 5", showEvent, anyEvent, wrapStyle
     , SUBMENU(S5St)
     , FIELD(pHs5, "  pH  :    ", " ", 0, 14, 1, 0.10, saveslot5, enterEvent, wrapStyle)
     , FIELD(ECs5, "  EC  :    ", "  mS/cm", 0, 5, 1, 0.01, saveslot5, enterEvent, wrapStyle)
     , FIELD(dos5, "  DO  :     ", " PPM", 0, 1000, 10, 100, doNothing, noEvent, wrapStyle)
     , OP("       SAVE", S5Read, enterEvent)
     , EXIT("<Back")
    );

MENU(slot6, " SLOT 6", showEvent, anyEvent, wrapStyle
     , SUBMENU(S6St)
     , FIELD(pHs6, "  pH  :    ", " ", 0, 14, 1, 0.20, savepHs6, enterEvent, wrapStyle)
     , FIELD(ECs6, "  EC  :    ", "  mS/cm", 0, 5, 1, 0.01, saveECs6, enterEvent, wrapStyle)
     , FIELD(dos6, "  DO  :     ", " PPM", 0, 1000, 10, 100, doNothing, noEvent, wrapStyle)
     , OP("       SAVE", S6Read, enterEvent)
     , EXIT("<Back")
    );

MENU(slot7, " SLOT 7", showEvent, anyEvent, wrapStyle
     , SUBMENU(S7St)
    , FIELD(pHs7, "  pH  :    ", " ", 0, 14, 1, 0.20, savepHs7, enterEvent, wrapStyle)
     , FIELD(ECs7, "  EC  :    ", "  mS/cm", 0, 5, 1, 0.01, saveECs7, enterEvent, wrapStyle)
     , FIELD(dos7, "  DO  :     ", " PPM", 0, 1000, 10, 100, doNothing, noEvent, wrapStyle)
     , OP("       SAVE", S7Read, enterEvent)
     , EXIT("<Back")
    );

MENU(slot8, " SLOT 8", showEvent, anyEvent, wrapStyle
     , SUBMENU(S8St)
     , FIELD(pHs8, "  pH  :    ", " ", 0, 14, 0.20, 0.20, savepHs8, enterEvent, wrapStyle)
     , FIELD(ECs8, "  EC  :    ", "  mS/cm", 0, 5, 1, 0.01, saveECs8, enterEvent, wrapStyle)
     , FIELD(dos8, "  DO  :     ", " PPM", 0, 1000, 10, 100, doNothing, noEvent, wrapStyle)
     , OP("       SAVE", S8Read, enterEvent)
     , EXIT("<Back")
    );

//****Save Slot1*****************************************************
result saveslot1() {
  EEPROM.updateFloat(EeprompHs1, pHs1);
  EEPROM.updateFloat(EepromECs1, ECs1);
  return proceed;
}

void S1Read()
{
  pHs1 = EEPROM.readFloat(EeprompHs1);
  ECs1 = EEPROM.readFloat(EepromECs1);
  
}
//****Save Slot2*****************************************************
result saveslot2() {
  EEPROM.updateFloat(EeprompHs2, pHs2);
  EEPROM.updateFloat(EepromECs2, ECs2);
  return proceed;
}

void S2Read()
{
  pHs2 = EEPROM.readFloat(EeprompHs2);
  ECs2 = EEPROM.readFloat(EepromECs2);
}

//****Save Slot3*****************************************************
result saveslot3() {
  EEPROM.updateFloat(EeprompHs3, pHs3);
  EEPROM.writeFloat(EepromECs3, ECs3);
  return proceed;
}

void S3Read()
{
  pHs3 = EEPROM.readFloat(EeprompHs3);
  ECs3 = EEPROM.readFloat(EepromECs3);
}

//****Save Slot4*****************************************************
result saveslot4() {
  EEPROM.writeFloat(EeprompHs4, pHs4);
  EEPROM.writeFloat(EepromECs4, ECs4);
  return proceed;
}

void S4Read()
{
  pHs4 = EEPROM.readFloat(EeprompHs4);
  ECs4 = EEPROM.readFloat(EepromECs4);
}

//****Save Slot5************************************
result saveslot5() {
  EEPROM.writeFloat(EeprompHs5, pHs5);
  EEPROM.writeFloat(EepromECs5, ECs5);
  return proceed;
}


void S5Read()
{
  pHs5 = EEPROM.readFloat(EeprompHs5);
  ECs5 = EEPROM.readFloat(EepromECs5);
}
//****Save Slot6********************************************************
result savepHs6() {
  EEPROM.writeFloat(EeprompHs6, pHs6);

  return proceed;
}

result saveECs6() {

  EEPROM.writeFloat(EepromECs6, ECs6);
  return proceed;
}
void S6Read()
{
  pHs6 = EEPROM.readFloat(EeprompHs6);
  ECs6 = EEPROM.readFloat(EepromECs6);
}

//****Save Slot7***************************************************
result savepHs7() {
  EEPROM.writeFloat(EeprompHs7, pHs7);

  return proceed;
}

result saveECs7() {

  EEPROM.writeFloat(EepromECs7, ECs7);
  return proceed;
}
void S7Read()
{
  pHs7 = EEPROM.readFloat(EeprompHs7);
  ECs7 = EEPROM.readFloat(EepromECs7);
}
//****Save Slot8******************************************
result savepHs8() {
  EEPROM.writeFloat(EeprompHs8, pHs8);

  return proceed;
}

result saveECs8() {

  EEPROM.writeFloat(EepromECs8, ECs8);
  return proceed;
}
void S8Read()
{
  pHs8 = EEPROM.readFloat(EeprompHs8);
  ECs8 = EEPROM.readFloat(EepromECs8);

 Serial.print(pHs8);
}
//*****************************************************************************
void Readall()
{
  pHs1 = EEPROM.readFloat(EeprompHs1);
  ECs1 = EEPROM.readFloat(EepromECs1);
  pHs2 = EEPROM.readFloat(EeprompHs2);
  ECs2 = EEPROM.readFloat(EepromECs2);
  pHs3 = EEPROM.readFloat(EeprompHs3);
  ECs3 = EEPROM.readFloat(EepromECs3);
  pHs4 = EEPROM.readFloat(EeprompHs4);
  ECs4 = EEPROM.readFloat(EepromECs4);
  pHs5 = EEPROM.readFloat(EeprompHs5);
  ECs5 = EEPROM.readFloat(EepromECs5);
  pHs6 = EEPROM.readFloat(EeprompHs6);
  ECs6 = EEPROM.readFloat(EepromECs6);
  pHs7 = EEPROM.readFloat(EeprompHs7);
  ECs7 = EEPROM.readFloat(EepromECs7);
  pHs8 = EEPROM.readFloat(EeprompHs8);
  ECs8 = EEPROM.readFloat(EepromECs8);
}
//***********************************************************************************

MENU(Shift, " ROSTER", showEvent, enterEvent, wrapStyle
     , SUBMENU(S1St)
     , SUBMENU(S2St)
     , SUBMENU(S3St)
     , SUBMENU(S4St)
     , SUBMENU(S5St)
     , SUBMENU(S6St)
     , SUBMENU(S7St)
     , SUBMENU(S8St)
     , EXIT("<Back")
    );

MENU(schedule, " SCHEDULED", showEvent, anyEvent, wrapStyle
     , SUBMENU(AutoSchedule)
     , SUBMENU(slot1)
     , SUBMENU(slot2)
     , SUBMENU(slot3)
     , SUBMENU(slot4)
     , SUBMENU(slot5)
     , SUBMENU(slot6)
     , SUBMENU(slot7)
     , SUBMENU(slot8)

     , EXIT("<Back")
    );


int FDate;
int FMonth;
int FYear;
int TDate;
int TMonth;
int TYear;
altMENU(menu, From, " From Date", doNothing, anyEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(FDate, "", "/", 0, 31, 1, 0, SPFD, anyEvent, noStyle)
        , FIELD(FMonth, "", "/", 0, 12, 10, 1, SPFM, anyEvent, wrapStyle)
        , FIELD(FYear, "", "", 2020, 2022, 0, 1, doNothing, anyEvent, wrapStyle)
       );
altMENU(menu, To, " To Date  ", doNothing, anyEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(TDate, "", "/", 0, 31, 1, 0, SPTD, anyEvent, noStyle)
        , FIELD(TMonth, "", "/", 0, 12, 10, 1, SPTM, anyEvent, wrapStyle)
        , FIELD(TYear, "", "", 2020, 2022, 0, 1, doNothing, anyEvent, wrapStyle)
       );

int slotslect = 0;
SELECT(slotslect, slots, " Select Slot :", doNothing, noEvent, noStyle
       , VALUE("All Slot", 0, doNothing, enterEvent)
       , VALUE("Slot 1", 1, doNothing, enterEvent)
       , VALUE("Slot 2 ", 2, doNothing, enterEvent)
       , VALUE("Slot 3", 3, doNothing, enterEvent)

      );

//***************Eprom Save SP****************//

int EepromSPFD = 260;
int EepromSPFM = 265;
int EepromSPTD = 270;
int EepromSPTM = 275;

result SPFD() {
  EEPROM.write(EepromSPFD, FDate);

  return proceed;
}
result SPFM() {
  EEPROM.write(EepromSPFM, FMonth);

  return proceed;
}
result SPTD() {
  EEPROM.write(EepromSPTD, TDate);

  return proceed;
}
result SPTM() {
  EEPROM.write(EepromSPTM, TMonth);

  return proceed;
}


float pHsp;
float ECsp;
float tempsp;
float dosp;
float nullval;
float phRefV;
float EcRefV;
int EepromPhRV =0x00;
int EepromECRV =0x00;
result RefVRead() {
  phRefV = EEPROM.read(EepromPhRV);
  EcRefV = EEPROM.read(EepromECRV);
  return proceed;
}
MENU(SetPoint, " AUTOMATIC ", showEvent, anyEvent, wrapStyle
     , FIELD(phValueavrg, "       pH ", " ", 0, 0, 0, 0, doNothing, enterEvent, wrapStyle)
     , FIELD(ECValueavrg, "       EC ", " mS/cm", 0, 0, 0, 0, doNothing, enterEvent, wrapStyle)
     , FIELD(phRefV, "       pH Ref V ", " ", 0, 0, 0, 0, doNothing, enterEvent, wrapStyle)
     , FIELD(EcRefV, "       EC Ref V", "", 0, 0, 0, 0, doNothing, enterEvent, wrapStyle)
     //     , OP("           ", doNothing, enterEvent)
     , FIELD(phSetHysteris, " PH HYSTERIS: ", " ", 0, 4, 1, 0.01, phsetHyssave, enterEvent, wrapStyle)
     , FIELD(ECSetHysteris, " EC HYSTERIS: ", " mS/cm", 0, 5, 1.0, 0.01, ECsethyssave, enterEvent, wrapStyle)
     , FIELD(pHsp, "  pH  :    ", " ", 0, 14, 1, 0.1, SPph, enterEvent, wrapStyle)
     , FIELD(ECsp, "  EC  :    ", " mS/cm", 0, 5, 1, 0.01, SPec, enterEvent, wrapStyle)
     //,FIELD(tempsp,"  TEMP:    "," 'C",0,100,1,5,SPtemp,noEvent,wrapStyle)
     , FIELD(dosp, "  DO  :    ", " PPM", 0, 1000, 10, 100, SPdosp, enterEvent, wrapStyle)
     , OP("           SAVE", SPFTRead, enterEvent)
     , SUBMENU(AutoDose)
     , EXIT("<Back")
    );

result RSTSETPOINT() {
  phSetHysteris = 0.5;
  ECSetHysteris = 0.2;
  pHsp = 6;
  ECsp = 1.2;
  tempsp = 25;
  dosp = 500;
  return proceed;
}
result SPUpdate() {
  //  setpointjson();
  // sensorjson();

  return proceed;
}

MENU(DSetpoint, " SETPOINTS ", showEvent, anyEvent, noStyle
     , SUBMENU(Asave)
     , FIELD(phSetHysteris, " PH HYSTERIS: ", " ", 0, 4, 1, 0.01, phsetHyssave, enterEvent, wrapStyle)
     , FIELD(ECSetHysteris, " EC HYSTERIS: ", " mS/cm", 0, 5, 1, 0.01, ECsethyssave, enterEvent, wrapStyle)
     , FIELD(pHsp, "  pH  :    ", " ", 0, 14, 1, 0.1, SPph, enterEvent, wrapStyle)
     , FIELD(ECsp, "  EC  :    ", " mS/cm", 0, 5, 1, 0.1, SPec, enterEvent, wrapStyle)
     , FIELD(tempsp, "  TEMP:    ", " 'C", 0, 100, 1, 5, SPtemp, noEvent, wrapStyle)
     , FIELD(dosp, "  DO  :    ", " PPM", 0, 1000, 10, 100, SPdosp, enterEvent, wrapStyle)
     , OP("      RESET SETPOINT", RSTSETPOINT, enterEvent)
     , OP("         UPDATE", SPUpdate, enterEvent)

     , EXIT("<Back")
    );
MENU(AutoDoser, " MODE ", showEvent, anyEvent, noStyle
     , SUBMENU(SetPoint) //Automatic
     , SUBMENU(schedule) //schedule
     , SUBMENU(MDoseMenu) //Manual Dose
     , EXIT("<Back")
    );
//***************Eprom Save SP****************//

int EeprompHsp = 280;
int EepromECsp = 285;
int Eepromtempsp = 290;
int Eepromdosp = 295;

result SPph() {
  EEPROM.writeFloat(EeprompHsp, pHsp);

  return proceed;
}
result SPec() {
  EEPROM.writeFloat(EepromECsp, ECsp);

  return proceed;
}
result SPtemp() {
  EEPROM.writeFloat(Eepromtempsp, tempsp);

  return proceed;
}
result SPdosp() {
  EEPROM.writeFloat(Eepromdosp, dosp);

  return proceed;
}


result SPFTRead() {
  FDate = EEPROM.read(EepromSPFD);
  TDate = EEPROM.read(EepromSPFM);
  FMonth = EEPROM.read(EepromSPTD);
  TMonth = EEPROM.read(EepromSPTM);
  pHsp = EEPROM.readFloat(EeprompHsp);
  ECsp = EEPROM.readFloat(EepromECsp);
  tempsp = EEPROM.readFloat(Eepromtempsp);
  dosp = EEPROM.readFloat(Eepromdosp);
  return proceed;
}

MENU(timedateset, " DATE & TIME", showEvent, anyEvent, noStyle

     , SUBMENU(timeMenu)
     , SUBMENU(dateMenu)
     , EXIT("<Back")
    );


CHOOSE(Speed, StepSpeed, " FLOW RATE :", doNothing, noEvent, noStyle


       , VALUE(" LOW", 1000, doNothing, enterEvent)
       , VALUE(" MEDIUM ", 750, doNothing, enterEvent)
       , VALUE(" HIGH", 500, doNothing, enterEvent)
      );

int DoserVr = 2;
CHOOSE(DoserVr, DoserMotor, " DOSER VERSION :", doNothing, noEvent, noStyle


       , VALUE("  v1.0", 1, doNothing, enterEvent)
       , VALUE("  v2.0 ", 2, doNothing, enterEvent)
       , VALUE("  v3.0", 3, doNothing, enterEvent)
      );

int ECUnite = 1;
CHOOSE(ECUnite, ecsetunite, " EC :", doNothing, noEvent, noStyle
       , VALUE("TDS ppm (mS/cm)", 0, doNothing, enterEvent)
       , VALUE("EC (ms/cm)", 1, doNothing, enterEvent)
       , VALUE("CF (μS/cm)", 2, doNothing, enterEvent)


      );
int tempUnite = 0;
CHOOSE(tempUnite, tempsetunite, " Temp :", doNothing, noEvent, noStyle
       , VALUE(" CENTIGRATE", 0, doNothing, enterEvent)
       , VALUE(" FARENHEIT", 1, doNothing, enterEvent)

      );


MENU(Units, " UNITS", showEvent, anyEvent, noStyle
     , SUBMENU(ecsetunite)
     , SUBMENU(tempsetunite)

     , EXIT("<Back")
    );

MENU(SDCard, " SD CARD", showEvent, enterEvent, noStyle

     , SUBMENU(SDcardOT)
     , EXIT("<Back")
    );

MENU(Doser, " SPEED & RATIO", showEvent, enterEvent, noStyle
     , SUBMENU(ECRatio)
     , SUBMENU(DoserMotor)
     , SUBMENU(StepSpeed)

     , EXIT("<Back")
    );
MENU(CPmenu, " DEBUG ", showEvent, enterEvent, noStyle
     , OP(" PROBE's", doNothing, noEvent)
     , SUBMENU(pHline)
     , SUBMENU(Ecline)
     , SUBMENU(Templine)
     , SUBMENU(Doline)
     , OP(" ACTUATOR's", doNothing, noEvent)
     , SUBMENU(tpump)
     , SUBMENU(heater)
     , SUBMENU(mixer)
     , SUBMENU(relay)
     , OP(" INDICATOR's", doNothing, noEvent)
     , SUBMENU(redLED)
     , SUBMENU(greenLED)
     , SUBMENU(blueLED)
     , EXIT("<Back")
    );
MENU(settings, " SETTINGS", showEvent, anyEvent, noStyle
//     , SUBMENU(CalMenu)
     , SUBMENU(DSetpoint)
     , SUBMENU(Units)
     , SUBMENU(timedateset)
     , SUBMENU(Shift)
     , SUBMENU(Doser)
     , SUBMENU(CPmenu)
     //, SUBMENU(SDCard)
     , OP(" INFO", Cinfo, enterEvent)
     , EXIT("<Back")
    );



MENU(mainMenu, "    DOSING CONTROLLER", doNothing, noEvent, wrapStyle
     , OP(" MONITOR", goHome, enterEvent)
     , SUBMENU(AutoDoser)
     //     , SUBMENU(CalMenu)
     , SUBMENU(settings)
     //  ,EXIT("<Back")
    );

serialIn serial(Serial);
////input from the encoder + encoder button + serial
//menuIn* inputsList[]={&encStream,&encBtn,&serial};
//menuIn* inputsList[]={&joystickBtns,&serial};
//chainStream<3> in(inputsList);//3 is the number of inputs

#define MAX_DEPTH 10

//this macro replaces all the above commented lines
MENU_OUTPUTS(out, MAX_DEPTH
             // ,U8GLIB_OUT(u8g2,colors,fontX,fontY,{0,0,128/fontX,64/fontY})
             //             , U8G2_OUT(u8g2, colors, fontX, fontY, offsetX, offsetY, {0, 0, U8_Width / fontX, U8_Height / fontY})
             , U8G2_OUT(u8g2, colors, fontX, fontY, offsetX, offsetY, {0, 0, U8_Width / fontX, U8_Height / fontY})
             , SERIAL_OUT(Serial)
             , NONE
            );



//NAVROOT(nav, mainMenu, MAX_DEPTH, in, out);
keypadIn kpad(customKeypad);
NAVROOT(nav, mainMenu, MAX_DEPTH, kpad, out);
result alert(menuOut& o, idleEvent e) {
  if (e == idling) {
    o.setCursor(0, 0);
    o.print("alert test");
    o.setCursor(0, 1);
    o.print("press [select]");
    o.setCursor(0, 2);
    o.print("to continue...");
  }
  return proceed;
}
void updateScreen() {
  DateTime now = rtc.now();
  //getNutrientPH();
  //EepromReadTime();
  //EepromReadDate();

  {
    u8g2.drawFrame(0, 0, 128, 64);
    u8g2.drawFrame(0, 0, 128, 12);
    u8g2.drawFrame(0, 0, 128, 53);
    u8g2.drawFrame(0, 0, 128, 43);
    u8g2.drawFrame(0, 0, 128, 33);
    //u8g2.drawFrame(53, 42, 128, 11);
    u8g2.drawFrame(64, 11, 128, 32);

    u8g2.drawStr(16, 10, " DOSING CONTROLLER");
    u8g2.drawStr(5, 21, "pH 1:");
    u8g2.setCursor(32, 21);
//    u8g2.print(float( phValue)); //phValueavrg
    u8g2.print(float( phValueavrg)); 

    u8g2.drawStr(5, 30, "pH 2:");
    u8g2.setCursor(32, 30);
    u8g2.print("  NC");

    u8g2.setCursor(67, 21);
    switch (ECUnite) {
      case 0: {
          u8g2.print("TDS 1:");

          break;
        }
      case 1: {
          u8g2.print("EC 1:");
          break;
        }
      case 2: {
          u8g2.print("CF 1:");
          break;
        }
      default :
        break;
    }
    u8g2.setCursor(92, 21);
    switch (ECUnite) {
      case 0: {
          u8g2.print(float( tdsValue));
          break;
        }
      case 1: {
//          u8g2.print(float(ecValue));  //ECValueavrg
          u8g2.print(float(ECValueavrg));
          break;
        }
      case 2: {
          u8g2.print(float( nutrientCF));
          break;
        }

      default :
        break;
    }
    u8g2.drawStr(67, 30, "EC 2:");
    u8g2.setCursor(95, 30);
    u8g2.print(" NC");


    u8g2.drawStr(5, 41, "TEMP:");
    u8g2.setCursor(32, 41);
    switch (tempUnite) {
      case 0: {
          u8g2.print((nutrientTemperature));
          u8g2.print("C");
          break;
        }
      case 1: {
          u8g2.print(( Fahrenheit));
          u8g2.print(" F");
          break;
        }
      default :
        break;
    }

    u8g2.drawStr(67, 41, "DO  :");
    u8g2.setCursor(93, 41);
    u8g2.print( "NC");
    //u8g2.print( DOvalue);

    u8g2.drawStr(2, 62, "STATUS:");
    u8g2.drawStr(38, 62, "System Ready");

    //     u8g2.setCursor(20, 62);
    //    switch (Status) {
    //      case 0: {
    //          u8g2.print("Mode0");
    //
    //          break;
    //        }
    //      case 1: {
    //          u8g2.print( Mode1);
    //
    //          break;
    //        }
    //        case 2: {
    //          u8g2.print(Mode2);
    //          break;
    //        }
    //
    //      default :
    //        break;
    //    }

    u8g2.drawStr(102, 62, "<Back");

    //      u8g2.setCursor(92,40);
    //      u8g2.print( " --");

    u8g2.drawStr(3, 51, "D/T:");
    u8g2.setCursor(24, 51);
    u8g2.print( now.day());
    u8g2.drawStr(35, 51, "/");
    u8g2.setCursor(42, 51);
    u8g2.print( now.month());
    u8g2.drawStr(47, 51, "/");
    u8g2.setCursor(53, 51);
    u8g2.print( now.year());
    //u8g2.print( ndaysOfTheWeek[now.dayOfTheWeek()]);

    //u8g2.drawStr(58, 52, "Date:");
    u8g2.setCursor(82, 51);
    u8g2.print( now.hour());
    u8g2.drawStr(92, 51, ":");
    u8g2.setCursor(98, 51);
    u8g2.print( now.minute());
    u8g2.drawStr(108, 51, ":");
    u8g2.setCursor(113, 51);
    u8g2.print( now.second());

    //    u8g2.drawFrame(0,0,128,64);
    //    u8g2.drawFrame(0,0,128,12);
    //    u8g2.setFont(fontName);

    //u8g2.setFont(u8g2_font_5x7_tr);
  }
}
result home(menuOut& o, idleEvent e) {
  {
    //u8g2.setFont(fontName);
    updateScreen();

  }
  return proceed;
}
result goHome(eventMask e, prompt &item) {
  nav.idleOn(home);
  return proceed;
}
result doAlert(eventMask e, prompt &item) {
  nav.idleOn(alert);
  return proceed;
}

result info(eventMask e, prompt &item) {
  nav.idleOn(onioninfo);
  return proceed;
}
result Cinfo(eventMask e, prompt &item) {
  nav.idleOn(controllerinfo);
  return proceed;
}
result controllerinfo(eventMask e, navNode& nav, prompt &item) {
  DateTime now = rtc.now();
  {
    u8g2.drawFrame(0, 0, 128, 64);
    u8g2.drawFrame(5, 5, 118, 54);
    u8g2.drawStr(9, 15, "    Name: ZG02OTB0");
    // u8g2.drawStr(40, 25, (Crop));
    u8g2.drawStr(25, 35, "    YoM:2021");
    u8g2.drawStr(20, 48, "Vr 2.0.1");
    u8g2.drawStr(20, 48, "DOSER Vr 2.0");
    u8g2.setCursor(40, 51);

  }
  return proceed;
}

result onioninfo(eventMask e, navNode& nav, prompt &item) {

  {

    u8g2.drawStr(45, 51, "none");
  }
  return proceed;
}

///***************Read all data from memoery******************************
void readalldata() {

//  Serial.println("SAVE DATA TO EEPROM");
  //  EepromRead();
  EepromReadEC();
  EepromReadPHCal();
  SPFTRead();
  
  
  autosavefun();//auto on Power
//  Serial.println(autoon);
  EepromReadauto();
  RefVRead();
  Schesavefun();
  EepromReadSche();
  allReadTime();

  Readall();
}
//when menu is suspended
result idle(menuOut& o, idleEvent e) {
  o.clear();
  switch (e) {
    case idleStart: o.println("suspending menu!"); break;
    case idling: o.println("suspended..."); break;
    case idleEnd: o.println("resuming menu."); break;
  }
  return proceed;
}
//
void getSDdata() {
  
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis > pinTime) {
    previousMillis = currentMillis;
    getNutrientTemperature();

    DateTime now = rtc.now();
    rtc.begin();
    sprintf(t, "%02d:%02d:%02d %02d/%02d/%02d",  now.hour(), now.minute(), now.second(), now.day(), now.month(), now.year());
    //Serial.print(F("Date/Time: "));
//    Serial.println("     ");
//    Serial.print(t);
//    Serial.print("      ");
//    Serial.print(nutrientTemperature);
//    Serial.print("      ");
//    Serial.print(nutrientPH);
//    Serial.print("      ");
//    Serial.print(tdsValue);

    sdcard_file = SD.open("data.txt", FILE_WRITE);
    if (sdcard_file) {
      
      sdcard_file.println("DATE ");
      sdcard_file.println(t);
      sdcard_file.print("      ");
      sdcard_file.print(t);
      sdcard_file.print("      ");
      sdcard_file.print(nutrientTemperature);
      sdcard_file.print("      ");
      sdcard_file.println(pH);
      sdcard_file.println("      ");
      sdcard_file.println(EC);  
      sdcard_file.println("      ");
      sdcard_file.println(phRefV);
      sdcard_file.println("      ");
      sdcard_file.println(EcRefV);  
      Serial.println(phRefV);
      Serial.println(EcRefV);
      
       
      
//      serializeJsonPretty(doc, Serial3);
      sdcard_file.close(); // close the file

    }
    // if the file didn't open, print an error:
    else {
      Serial.println(F("error opening test.txt"));
    }


  }

}
//

//!******************************************************************************
//! Name: getNutrientTemperature()                               *
//! Description: For Doser Shedule                     *
//! Param : Temperature                                  *
//! Returns: void                                     *
//! Example:          *
//!******************************************************************************

// Read water temperature
float getNutrientTemperature() {
  // Read DALLAS
  // call sensors.requestTemperatures() to issue a global temperature
  // request to all devices on the bus
  sensors.requestTemperatures(); // Send the command to get temperatures
  float ntValue = (sensors.getTempCByIndex(0)); // Why "byIndex"?
  // You can have more than one IC on the same bus.
  // 0 refers to the first IC on the wire
  ntValue = roundDecimalPoint(ntValue, 1);
  Fahrenheit = sensors.toFahrenheit(ntValue);

  nutrientTemperature = ntValue;
  //  Serial.print(nutrientTemperature, 1);
  //    Serial.println("C");
  //    Serial.print(Fahrenheit, 1);
  //    Serial.println("F");
  return ntValue;
}
//!******************************************************************************
//! Name: getNutrientPHavg()                               *
//! Description: For Doser Shedule                     *
//! Param : pH                                  *
//! Returns: void                                     *
//! Example:          *
//!******************************************************************************

bool pHAvg;
float getNutrientPHavg() {
 
  static unsigned long samplingTime = millis();
  static unsigned long printTime = millis();
  static unsigned long timepoint = millis();
  static float voltagePHavrg, phtest;

  if (millis() - samplingTime > samplingInterval) {
    pHArray[pHArrayIndex++] = analogRead(PH_PIN);
    if (pHArrayIndex == ArrayLenth)
      pHArrayIndex = 0;
    voltagePH = analogRead(PH_PIN) / 1024.0 * 5000;      // read the ph voltage
    phValue    = (ph.readPH(voltagePH, nutrientTemperature)) + Offset;

    voltagePHavrg = avergearray(pHArray, ArrayLenth) / 1024.0 * 5000;
    phValueavrg    = (ph.readPH(voltagePHavrg, nutrientTemperature)) + Offset;
//    Serial.print("pH :");
//    Serial.println(phValue, 2);
    phtest = roundDecimalPoint(phValue, 2);
    int phRv = phtest;
//    Serial.print(phRv);
//    Serial.println("                ");
//    Serial.print("pH Arg:");
//    Serial.println(phValueavrg, 2);
    int phValueavrgO = roundDecimalPoint(phValueavrg, 2);
    samplingTime = millis();

    if (phRv == phValueavrgO) {
//      Serial.println("PH Stable");
      pHAvg = true;
    }
    else {
//      Serial.println("PH Not Stable");
      pHAvg = false;
    }
  }

  return phValueavrg;
  pH = roundDecimalPoint(phValueavrg, 2);
//  Serial.print("PH raed SENSOR : ");
//    Serial.println(pH);
}
//Read ph value from water

//!******************************************************************************
//! Name: getNutrientPH()                               *
//! Description: For Doser Shedule                     *
//! Param : pH                                  *
//! Returns: void                                     *
//! Example:          *
//!******************************************************************************

float getNutrientPH() {
  static unsigned long samplingTime = millis();
  static unsigned long printTime = millis();
  static unsigned long timepoint = millis();
  static float pHValue;
  if (millis() - timepoint > 1000U) {                      //time interval: 1s
    timepoint = millis();
    //temperature = readTemperature();                   // read your temperature sensor to execute temperature compensation
    voltagePH = analogRead(PH_PIN) / 1024.0 * 5000;      // read the ph voltage
    phValue    = (ph.readPH(voltagePH, nutrientTemperature)) + Offset;     // convert voltage to pH with temperature compensation
//    Serial.print("pH:");
    Serial.println(phValue, 2);
  }
  return phValue;

  //  pH = roundDecimalPoint(pHValue, 2);
}

//!******************************************************************************
//! Name: getNutrientECavg()                               *
//! Description: For Doser Shedule                     *
//! Param : EC AVRAGE                                 *
//! Returns: void                                     *
//! Example:          *
//!******************************************************************************

bool ECAvg;
float getNutrientECavg() {
  static unsigned long samplingTime = millis();
  static unsigned long printTime = millis();
  static unsigned long timepoint = millis();
  static float voltageECavrg, ECtest;

  if (millis() - samplingTime > samplingInterval) {
    ECArray[ECArrayIndex++] = analogRead(EC_PIN);
    if (ECArrayIndex == ArrayLenth)
      ECArrayIndex = 0;
    voltageEC = analogRead(EC_PIN) / 1024.0 * 5000;      // read the ph voltage
    
    ecValue    = (ec.readEC(voltageEC, nutrientTemperature)) + Offsetec; 
    voltageECavrg = avergearrayec
    (ECArray, ArrayLenth) / 1024.0 * 5000;
    ECValueavrg    = (ec.readEC(voltageECavrg, nutrientTemperature)) + Offsetec;
//    Serial.print("_________________");
//    Serial.print("EC :");
//    Serial.println(ecValue, 2);
    
    ECtest = roundDecimalPoint(ecValue, 2);
    int ECRv = ECtest;
//    Serial.print(ECRv);
//    Serial.println("                ");
//    Serial.print("EC Arg:");
//    Serial.println(ECValueavrg, 2);
    int ECValueavrgO = roundDecimalPoint(ECValueavrg, 2);
    samplingTime = millis();

    if (ECRv == ECValueavrgO) {
//      Serial.println("EC Stable");
      ECAvg = true;
    }
    else {
//      Serial.println("EC Not Stable");
      ECAvg = false;
    }
  }
//  Serial.print("_________________");
  return ECValueavrg;
  EC = roundDecimalPoint(ECValueavrg, 2);
}
//!******************************************************************************
//! Name: getNutrientEC()                               *
//! Description: For Doser Shedule                     *
//! Param : EC                                     *
//! Returns: void                                     *
//! Example:          *
//!******************************************************************************

float getNutrientEC() {

  static unsigned long timepoint = millis();
  if (millis() - timepoint > 1000U) {
    timepoint = millis();
    voltageEC = analogRead(EC_PIN) / 1024.0 * 5000;
    ecValue    = (ec.readEC(voltageEC, nutrientTemperature)) + 0.25;    // convert voltage to EC with temperature compensation
    //    Serial.print(", EC:");
    //    Serial.print(ecValue, 2);
    //    Serial.print("ms/cm");
  }
}

//!******************************************************************************
//! Name: getNutrientTDS()                               *
//! Description: For Doser Shedule                     *
//! Param : EC- TDS                                  *
//! Returns: void                                     *
//! Example:          *
//!******************************************************************************

float getNutrientTDS() {
  float tdsValue = ecValue * 500;
  tdsValue = roundDecimalPoint(tdsValue, 2);

  nutrientTDS = tdsValue ;
  //  Serial.print("EC Value:");
  //  Serial.print(nutrientEC,2);
  return tdsValue;
}
//!******************************************************************************
//! Name: getNutrientCF()                               *
//! Description: For Doser Shedule                     *
//! Param : EC- CF                                     *
//! Returns: void                                     *
//! Example:          *
//!******************************************************************************

float getNutrientCF() {
  float cfValue = ecValue * 10;
  nutrientCF = cfValue;
  //  Serial.print("CF Value:");
  //  Serial.println(nutrientCF,0);
  return cfValue;
}

//!******************************************************************************
//! Name: getNutrientDO()                               *
//! Description: For Doser Shedule                     *
//! Param : DO Sensor                                    *
//! Returns: void                                     *
//! Example:          *
//!******************************************************************************

float getNutrientDO() {
  Temperaturet = (uint8_t)READ_TEMP;
  ADC_Raw = analogRead(DO_PIN);
  ADC_Voltage = uint32_t(VREF) * ADC_Raw / ADC_RES;

  // Serial.print("Temperaturet:\t" + String(Temperaturet) + "\t");
  // Serial.print("ADC RAW:\t" + String(ADC_Raw) + "\t");
  //Serial.print("ADC Voltage:\t" + String(ADC_Voltage) + "\t");
  //Serial.println("DO:\t" + String(readDO(ADC_Voltage, Temperaturet)) + "\t");
  float DO = (readDO(ADC_Voltage, Temperaturet));
  float DOvalue = DO;
  //Serial.print(DOvalue);


  return DOvalue;

}
bool phAutoon;
//***************************************AUTO DOSAER *******************************************
//!******************************************************************************
//! Name: DoserAuto()                               *
//! Description: For Doser Shedule                     *
//! Param :                                   *
//! Returns: void                                     *
//! Example:DoserAuto(float pHAutoSP, float pHcalibration, float ECAutoSP, float ECHys )             *
//!******************************************************************************

void DoserAuto(float pHAutoSP, float pHcalibration, float ECAutoSP, float ECHys ) {
  static unsigned long samplingTime = millis();
  static unsigned long printTime = millis();

  digitalWrite(LEDGREEN, HIGH);
//    Serial.println(pHcalibration);
//    Serial.println("pH automatic start");
//    Serial.print("pH SETPOINT : ");
//    Serial.println(pHAutoSP);
//    Serial.print("EC SETPOINT : ");
//    Serial.println(ECAutoSP);
//    Serial.println(pHcalibration);
   //Sampler pump and mixer relay ON...
    digitalWrite(TPUMPR, HIGH);
    digitalWrite(MIXERR, HIGH);

  pHSetpoint = pHAutoSP ;
  phSetHysteris = pHcalibration;
   pH = roundDecimalPoint(phValueavrg, 2);
//    Serial.print("PH SENSOR : ");
//    Serial.println(pH);
//    Serial.print("EC HYS");
//    Serial.println(ECHys);

  ECSetpoint = ECAutoSP;
    ECSetHysteris=ECHys;
  EC = roundDecimalPoint(ecValue, 2);
 
//    Serial.print("EC SENSOR : ");
//    Serial.println(EC);

  float HysterisMin = (pHSetpoint - phSetHysteris);
  float HysterisPlus = (pHSetpoint + phSetHysteris);
//    Serial.print("PH HYS MIN : ");
//    Serial.println(HysterisMin);
//    Serial.print("PH HYS MAX : ");
//    Serial.println(HysterisPlus);

  float ECHysterisMin = (ECSetpoint - ECSetHysteris);
  float ECHysterisPlus = (ECSetpoint + ECSetHysteris);
//  Serial.print("EC HYS MIN : ");
//  Serial.println(ECHysterisMin);
//  Serial.print("EC HYS MAX : ");
//  Serial.println(ECHysterisPlus);

  if (StopPHHys == false) {
    if (pH == pHSetpoint) {
      unsigned long currentMillis = millis();
      if (currentMillis - previousMillis > pinTime) {
        previousMillis = currentMillis;

        digitalWrite(PHHIGH_DIR_PIN, LOW);
        //digitalWrite(PHLOW_DIR_PIN, LOW);
        for (int X = 0; X < 1000; X++) {
          //digitalWrite(PHLOW_STEP_PIN, HIGH);
          digitalWrite(PHHIGH_STEP_PIN, LOW);
          delayMicroseconds(Speed);
          digitalWrite(PHHIGH_STEP_PIN, LOW);
          delayMicroseconds(Speed);
        }

        //          Serial.println("ph Max pump and Min pump are OFF");
        phDown = 0;
        phUp = 0;
        pinTime = pinLowTime;
      }
    }

    if (pH < HysterisMin) {
      digitalWrite(TPUMPR, HIGH);
      digitalWrite(MIXERR, HIGH);
      unsigned long currentMillis = millis();
      if (currentMillis - previousMillis > pinTime) {
        previousMillis = currentMillis;

        digitalWrite(PHHIGH_DIR_PIN, LOW);
        //digitalWrite(PHLOW_DIR_PIN, LOW);
        for (int X = 0; X < 1000; X++) {
          //digitalWrite(PHLOW_STEP_PIN, HIGH);
          digitalWrite(PHHIGH_STEP_PIN, HIGH);
          delayMicroseconds(Speed);
          digitalWrite(PHHIGH_STEP_PIN, LOW);
          delayMicroseconds(Speed);
        }
        //      Serial.println("ph Max pump is HIGH");
      }
    }
    if (pH >= HysterisMin && pH < pHSetpoint) {
      unsigned long currentMillis = millis();
      if (currentMillis - previousMillis > pinTime) {
        previousMillis = currentMillis;
        digitalWrite(PHHIGH_DIR_PIN, LOW);
        //digitalWrite(PHLOW_DIR_PIN, LOW);
        for (int X = 0; X < 1000; X++) {
          //digitalWrite(PHLOW_STEP_PIN, HIGH);
          digitalWrite(PHHIGH_STEP_PIN, HIGH);
          delayMicroseconds(Speed);
          digitalWrite(PHHIGH_STEP_PIN, LOW);
          delayMicroseconds(Speed);
          //            Serial.println("ph Max pump is HIGH");
          phUp = 1;
          phDown = 0;
          pinTime = pinHighTime;
        }
      }
    }
    if (pH > HysterisPlus) {
      digitalWrite(TPUMPR, HIGH);
      digitalWrite(MIXERR, HIGH);
      unsigned long currentMillis = millis();
      if (currentMillis - previousMillis > pinTime) {
        previousMillis = currentMillis;

        digitalWrite(PHLOW_DIR_PIN, LOW);
        //digitalWrite(PHLOW_DIR_PIN, LOW);
        for (int X = 0; X < 1000; X++) {
          digitalWrite(PHLOW_STEP_PIN, HIGH);
          //digitalWrite(PHLOW_STEP_PIN, LOW);
          delayMicroseconds(Speed);
          digitalWrite(PHLOW_STEP_PIN, LOW);
          delayMicroseconds(Speed);

          phDown = 1;
          phUp = 0;
          pinTime = pinLowTime;
        }
        //      Serial.println("ph Min pump is HIGH");

      }
    }

    if (pH <= HysterisPlus && pH > pHSetpoint) {
      unsigned long currentMillis = millis();
      if (currentMillis - previousMillis > pinTime) {
        previousMillis = currentMillis;

        digitalWrite(PHLOW_DIR_PIN, LOW);
        //digitalWrite(PHLOW_DIR_PIN, LOW);
        for (int X = 0; X < 1000; X++) {
          digitalWrite(PHLOW_STEP_PIN, HIGH);
          //digitalWrite(PHLOW_STEP_PIN, LOW);
          delayMicroseconds(Speed);
          digitalWrite(PHLOW_STEP_PIN, LOW);
          delayMicroseconds(Speed);

          //            Serial.println("ph Min pump is HIGH");
          phUp = 0;
          phDown = 1;
          pinTime = pinLowTime;
        }
      }
    }

    //********************EC DOSER ******************************
    if ((pH >= HysterisMin && pH <= HysterisPlus)) {
      //           digitalWrite(TPUMPR, HIGH);
      //           digitalWrite(MIXERR, HIGH);
      //      StopPHHys == true;
      Serial.println("PH EQL & EC Correction START");
      // if(StopPHHys == true){

      //EC FUNtion
      if (EC == ECSetpoint) {
        unsigned long currentMillis = millis();
        if (currentMillis - previousMillis > pinTime) {
          previousMillis = currentMillis;

          ///ec ABC low

          Serial.println("EC + and - are OFF");
          phDown = 0;
          phUp = 0;
          pinTime = pinLowTime;
        }
      }
    }
    if (EC >= ECHysterisMin && EC <= ECHysterisPlus) {
      unsigned long currentMillis = millis();
      if (currentMillis - previousMillis > pinTime) {
        previousMillis = currentMillis;
        //LOW

        int ECRA =  ECRatioA * 230;
        digitalWrite(EC_A_DIR_PIN, LOW);
        for (int X = 0; X < ECRA; X++) {
          digitalWrite(EC_A_STEP_PIN, LOW);
          delayMicroseconds(Speed);
          digitalWrite(EC_A_STEP_PIN, LOW);
          delayMicroseconds(Speed);
        }
        int ECRB =  ECRatioB * 230;
        digitalWrite(EC_B_DIR_PIN, LOW);

        for (int X = 0; X < ECRB; X++) {
          digitalWrite(EC_B_STEP_PIN, LOW);
          delayMicroseconds(Speed);
          digitalWrite(EC_B_STEP_PIN, LOW);
          delayMicroseconds(Speed);
        }
        //          int ECRC =  ECRatioC * 230;
        //          digitalWrite(EC_C_DIR_PIN, LOW);
        //          for (int X = 0; X < ECRC; X++) {
        //            digitalWrite(EC_C_STEP_PIN, LOW);
        //            delayMicroseconds(Speed);
        //            digitalWrite(EC_C_STEP_PIN, LOW);
        //            delayMicroseconds(Speed);
        //
        //          }


        Serial.println("EC MIN and MAX are LOW (hysteria correction)");
        phUp = 0;
        phDown = 0;
        pinTime = pinLowTime;
      }
    }
    if (EC >= ECHysterisMin && EC < ECSetpoint) {
      unsigned long currentMillis = millis();
      if (currentMillis - previousMillis > pinTime) {
        previousMillis = currentMillis;

        int ECRA =  ECRatioA * 230;
        digitalWrite(EC_A_DIR_PIN, LOW);
        for (int X = 0; X < ECRA; X++) {
          digitalWrite(EC_A_STEP_PIN, LOW);
          delayMicroseconds(Speed);
          digitalWrite(EC_A_STEP_PIN, LOW);
          delayMicroseconds(Speed);
        }
        int ECRB =  ECRatioB * 230;
        digitalWrite(EC_B_DIR_PIN, LOW);

        for (int X = 0; X < ECRB; X++) {
          digitalWrite(EC_B_STEP_PIN, LOW);
          delayMicroseconds(Speed);
          digitalWrite(EC_B_STEP_PIN, LOW);
          delayMicroseconds(Speed);
        }
        //          int ECRC =  ECRatioC * 230;
        //          digitalWrite(EC_C_DIR_PIN, LOW);
        //          for (int X = 0; X < ECRC; X++) {
        //            digitalWrite(EC_C_STEP_PIN, LOW);
        //            delayMicroseconds(Speed);
        //            digitalWrite(EC_C_STEP_PIN, LOW);
        //            delayMicroseconds(Speed);
        //
        //          }

        Serial.println("EC MAX pin is HIGH");
        phUp = 1;
        phDown = 0;
        pinTime = pinHighTime;
      }
    }

    if (EC < ECHysterisMin) {
      unsigned long currentMillis = millis();
      if (currentMillis - previousMillis > pinTime) {
        previousMillis = currentMillis;
        int ECRA =  ECRatioA * 230;
        digitalWrite(EC_A_DIR_PIN, LOW);
        for (int X = 0; X < ECRA; X++) {
          digitalWrite(EC_A_STEP_PIN, HIGH);
          delayMicroseconds(Speed);
          digitalWrite(EC_A_STEP_PIN, LOW);
          delayMicroseconds(Speed);
        }
        int ECRB =  ECRatioB * 230;
        digitalWrite(EC_B_DIR_PIN, LOW);

        for (int X = 0; X < ECRB; X++) {
          digitalWrite(EC_B_STEP_PIN, HIGH);
          delayMicroseconds(Speed);
          digitalWrite(EC_B_STEP_PIN, LOW);
          delayMicroseconds(Speed);
        }
        //          int ECRC =  ECRatioC * 230;
        //          digitalWrite(EC_C_DIR_PIN, LOW);
        //          for (int X = 0; X < ECRC; X++) {
        //            digitalWrite(EC_C_STEP_PIN, HIGH);
        //            delayMicroseconds(Speed);
        //            digitalWrite(EC_C_STEP_PIN, LOW);
        //            delayMicroseconds(Speed);
        //
        //          }
      }
      //          Serial.println("EC doser  HIGH");

    }


    if (EC > ECHysterisPlus) {
      unsigned long currentMillis = millis();
      if (currentMillis - previousMillis > pinTime) {
        previousMillis = currentMillis;
        digitalWrite(LEDRED, HIGH);
        //RO WATER ON
        digitalWrite( RORELAYR, HIGH);

        Serial.println("ph - pin is HIGH");
        phDown = 1;
        phUp = 0;
        pinTime = pinLowTime;

      }
    }

    if (EC <= ECHysterisPlus && EC > ECSetpoint) {
      unsigned long currentMillis = millis();
      if (currentMillis - previousMillis > pinTime) {
        previousMillis = currentMillis;
        digitalWrite(LEDRED, HIGH);

        //RO WATER ON
        digitalWrite( RORELAYR, HIGH);

        Serial.println("ph - pin is HIGH");
        phUp = 0;
        phDown = 1;
        pinTime = pinLowTime;
      }
    }
  }


}

///*****************************************SHEDULER PH AND EC *****************************************************
//!******************************************************************************
//! Name: SheduleDoser()                               *
//! Description: For Doser Shedule                     *
//! Param : DATE /TIME                                      *
//! Returns: void                                     *
//! Example:SheduleDoser(float pHAutoSetValue, float pHHys, float ECAutoSetValue, float ECHys )               *
//!******************************************************************************

void SheduleDoser(float pHAutoSetValue, float pHHys, float ECAutoSetValue, float ECHys ) {
  digitalWrite(LEDGREEN, HIGH);
  //  Serial.println(pHcalibration);
  //  Serial.println("pH Sheduler start");
  //  Serial.print("pH SETPOINT : ");
  //  Serial.println(pHAutoSetValue);
  //  Serial.print("EC SETPOINT : ");
  //  Serial.println(ECAutoSetValue);

  //  digitalWrite(TPUMPR, HIGH);
  //  digitalWrite(MIXERR, HIGH);

  pHSetpoint = pHAutoSetValue ;
  phSetHysteris = pHHys;
  pH = roundDecimalPoint(phValue, 2);
  //  Serial.print("PH SENSOR : ");
  //  Serial.println(pH);

  ECSetpoint = ECAutoSetValue;
  ECHys = ECSetHysteris;
  EC = roundDecimalPoint(ecValue, 2);
  //  Serial.print("EC SENSOR : ");
  //  Serial.println(EC);

  float HysterisMin = (pHSetpoint - phSetHysteris);
  float HysterisPlus = (pHSetpoint + phSetHysteris);
  //  Serial.print("PH HYS MIN : ");
  //  Serial.println(HysterisMin);
  //  Serial.print("PH HYS MAX : ");
  //  Serial.println(HysterisPlus);

  ECHysterisMin = (ECSetpoint - ECSetHysteris);
  ECHysterisPlus = (ECSetpoint + ECSetHysteris);
  //  Serial.print("EC HYS MIN : ");
  //  Serial.println(ECHysterisMin);
  //  Serial.print("EC HYS MAX : ");
  //  Serial.println(ECHysterisPlus);

  if (StopPHHys == false) {
    if (pH == pHSetpoint) {
      unsigned long currentMillis = millis();
      if (currentMillis - previousMillis > pinTime) {
        previousMillis = currentMillis;

        digitalWrite(PHHIGH_DIR_PIN, LOW);
        //digitalWrite(PHLOW_DIR_PIN, LOW);
        for (int X = 0; X < 1000; X++) {
          //digitalWrite(PHLOW_STEP_PIN, HIGH);
          digitalWrite(PHHIGH_STEP_PIN, LOW);
          delayMicroseconds(Speed);
          digitalWrite(PHHIGH_STEP_PIN, LOW);
          delayMicroseconds(Speed);
        }

        //          Serial.println("ph Max pump and Min pump are OFF");
        phDown = 0;
        phUp = 0;
        pinTime = pinLowTime;
      }
    }

    if (pH < HysterisMin) {
      digitalWrite(TPUMPR, HIGH);
      digitalWrite(MIXERR, HIGH);
      unsigned long currentMillis = millis();
      if (currentMillis - previousMillis > pinTime) {
        previousMillis = currentMillis;

        digitalWrite(PHHIGH_DIR_PIN, LOW);
        //digitalWrite(PHLOW_DIR_PIN, LOW);
        for (int X = 0; X < 1000; X++) {
          //digitalWrite(PHLOW_STEP_PIN, HIGH);
          digitalWrite(PHHIGH_STEP_PIN, HIGH);
          delayMicroseconds(Speed);
          digitalWrite(PHHIGH_STEP_PIN, LOW);
          delayMicroseconds(Speed);
        }
        //      Serial.println("ph Max pump is HIGH");
      }
    }
    if (pH >= HysterisMin && pH < pHSetpoint) {
      unsigned long currentMillis = millis();
      if (currentMillis - previousMillis > pinTime) {
        previousMillis = currentMillis;
        digitalWrite(PHHIGH_DIR_PIN, LOW);
        //digitalWrite(PHLOW_DIR_PIN, LOW);
        for (int X = 0; X < 1000; X++) {
          //digitalWrite(PHLOW_STEP_PIN, HIGH);
          digitalWrite(PHHIGH_STEP_PIN, HIGH);
          delayMicroseconds(Speed);
          digitalWrite(PHHIGH_STEP_PIN, LOW);
          delayMicroseconds(Speed);
          //            Serial.println("ph Max pump is HIGH");
          phUp = 1;
          phDown = 0;
          pinTime = pinHighTime;
        }
      }
    }
    if (pH > HysterisPlus) {
      digitalWrite(TPUMPR, HIGH);
      digitalWrite(MIXERR, HIGH);
      unsigned long currentMillis = millis();
      if (currentMillis - previousMillis > pinTime) {
        previousMillis = currentMillis;

        digitalWrite(PHLOW_DIR_PIN, LOW);
        //digitalWrite(PHLOW_DIR_PIN, LOW);
        for (int X = 0; X < 1000; X++) {
          digitalWrite(PHLOW_STEP_PIN, HIGH);
          //digitalWrite(PHLOW_STEP_PIN, LOW);
          delayMicroseconds(Speed);
          digitalWrite(PHLOW_STEP_PIN, LOW);
          delayMicroseconds(Speed);

          phDown = 1;
          phUp = 0;
          pinTime = pinLowTime;
        }
        //      Serial.println("ph Min pump is HIGH");

      }
    }

    if (pH <= HysterisPlus && pH > pHSetpoint) {
      unsigned long currentMillis = millis();
      if (currentMillis - previousMillis > pinTime) {
        previousMillis = currentMillis;

        digitalWrite(PHLOW_DIR_PIN, LOW);
        //digitalWrite(PHLOW_DIR_PIN, LOW);
        for (int X = 0; X < 1000; X++) {
          digitalWrite(PHLOW_STEP_PIN, HIGH);
          //digitalWrite(PHLOW_STEP_PIN, LOW);
          delayMicroseconds(Speed);
          digitalWrite(PHLOW_STEP_PIN, LOW);
          delayMicroseconds(Speed);

          //            Serial.println("ph Min pump is HIGH");
          phUp = 0;
          phDown = 1;
          pinTime = pinLowTime;
        }
      }
    }

    //********************EC DOSER ******************************
    if ((pH >= HysterisMin && pH <= HysterisPlus)) {
      //           digitalWrite(TPUMPR, HIGH);
      //           digitalWrite(MIXERR, HIGH);
      //      StopPHHys == true;
      Serial.println("PH EQL & EC Correction START");
      // if(StopPHHys == true){

      //EC FUNtion
      if (EC == ECSetpoint) {
        unsigned long currentMillis = millis();
        if (currentMillis - previousMillis > pinTime) {
          previousMillis = currentMillis;

          ///ec ABC low

          Serial.println("EC + and - are OFF");
          phDown = 0;
          phUp = 0;
          pinTime = pinLowTime;
        }
      }
    }
    if (EC >= ECHysterisMin && EC <= ECHysterisPlus) {
      unsigned long currentMillis = millis();
      if (currentMillis - previousMillis > pinTime) {
        previousMillis = currentMillis;
        //LOW

        int ECRA =  ECRatioA * 230;
        digitalWrite(EC_A_DIR_PIN, LOW);
        for (int X = 0; X < ECRA; X++) {
          digitalWrite(EC_A_STEP_PIN, LOW);
          delayMicroseconds(Speed);
          digitalWrite(EC_A_STEP_PIN, LOW);
          delayMicroseconds(Speed);
        }
        int ECRB =  ECRatioB * 230;
        digitalWrite(EC_B_DIR_PIN, LOW);

        for (int X = 0; X < ECRB; X++) {
          digitalWrite(EC_B_STEP_PIN, LOW);
          delayMicroseconds(Speed);
          digitalWrite(EC_B_STEP_PIN, LOW);
          delayMicroseconds(Speed);
        }
        //          int ECRC =  ECRatioC * 230;
        //          digitalWrite(EC_C_DIR_PIN, LOW);
        //          for (int X = 0; X < ECRC; X++) {
        //            digitalWrite(EC_C_STEP_PIN, LOW);
        //            delayMicroseconds(Speed);
        //            digitalWrite(EC_C_STEP_PIN, LOW);
        //            delayMicroseconds(Speed);
        //
        //          }


        Serial.println("EC MIN and MAX are LOW (hysteria correction)");
        phUp = 0;
        phDown = 0;
        pinTime = pinLowTime;
      }
    }
    if (EC >= ECHysterisMin && EC < ECSetpoint) {
      unsigned long currentMillis = millis();
      if (currentMillis - previousMillis > pinTime) {
        previousMillis = currentMillis;

        int ECRA =  ECRatioA * 230;
        digitalWrite(EC_A_DIR_PIN, LOW);
        for (int X = 0; X < ECRA; X++) {
          digitalWrite(EC_A_STEP_PIN, LOW);
          delayMicroseconds(Speed);
          digitalWrite(EC_A_STEP_PIN, LOW);
          delayMicroseconds(Speed);
        }
        int ECRB =  ECRatioB * 230;
        digitalWrite(EC_B_DIR_PIN, LOW);

        for (int X = 0; X < ECRB; X++) {
          digitalWrite(EC_B_STEP_PIN, LOW);
          delayMicroseconds(Speed);
          digitalWrite(EC_B_STEP_PIN, LOW);
          delayMicroseconds(Speed);
        }
        //          int ECRC =  ECRatioC * 230;
        //          digitalWrite(EC_C_DIR_PIN, LOW);
        //          for (int X = 0; X < ECRC; X++) {
        //            digitalWrite(EC_C_STEP_PIN, LOW);
        //            delayMicroseconds(Speed);
        //            digitalWrite(EC_C_STEP_PIN, LOW);
        //            delayMicroseconds(Speed);
        //
        //          }

        Serial.println("EC MAX pin is HIGH");
        phUp = 1;
        phDown = 0;
        pinTime = pinHighTime;
      }
    }

    if (EC < ECHysterisMin) {
      unsigned long currentMillis = millis();
      if (currentMillis - previousMillis > pinTime) {
        previousMillis = currentMillis;
        int ECRA =  ECRatioA * 230;
        digitalWrite(EC_A_DIR_PIN, LOW);
        for (int X = 0; X < ECRA; X++) {
          digitalWrite(EC_A_STEP_PIN, HIGH);
          delayMicroseconds(Speed);
          digitalWrite(EC_A_STEP_PIN, LOW);
          delayMicroseconds(Speed);
        }
        int ECRB =  ECRatioB * 230;
        digitalWrite(EC_B_DIR_PIN, LOW);

        for (int X = 0; X < ECRB; X++) {
          digitalWrite(EC_B_STEP_PIN, HIGH);
          delayMicroseconds(Speed);
          digitalWrite(EC_B_STEP_PIN, LOW);
          delayMicroseconds(Speed);
        }
        //          int ECRC =  ECRatioC * 230;
        //          digitalWrite(EC_C_DIR_PIN, LOW);
        //          for (int X = 0; X < ECRC; X++) {
        //            digitalWrite(EC_C_STEP_PIN, HIGH);
        //            delayMicroseconds(Speed);
        //            digitalWrite(EC_C_STEP_PIN, LOW);
        //            delayMicroseconds(Speed);
        //
        //          }
      }
      //          Serial.println("EC doser  HIGH");

    }


    if (EC > ECHysterisPlus) {
      unsigned long currentMillis = millis();
      if (currentMillis - previousMillis > pinTime) {
        previousMillis = currentMillis;
        digitalWrite(LEDRED, HIGH);
        //RO WATER ON
        digitalWrite( RORELAYR, HIGH);

        Serial.println("ph - pin is HIGH");
        phDown = 1;
        phUp = 0;
        pinTime = pinLowTime;

      }
    }

    if (EC <= ECHysterisPlus && EC > ECSetpoint) {
      unsigned long currentMillis = millis();
      if (currentMillis - previousMillis > pinTime) {
        previousMillis = currentMillis;
        digitalWrite(LEDRED, HIGH);

        //RO WATER ON
        digitalWrite( RORELAYR, HIGH);

        Serial.println("ph - pin is HIGH");
        phUp = 0;
        phDown = 1;
        pinTime = pinLowTime;
      }
    }
  }

 
}

//*************************************  TIME SHEDULER   ***********************************************
//!******************************************************************************
//! Name: scheduleFun()                               *
//! Description: For Doser Shedule                     *
//! Param : DATE /TIME                                      *
//! Returns: void                                     *
//! Example:               *
//!******************************************************************************


void scheduleFun() {

  
  //  digitalWrite(LEDGREEN, HIGH);
  //  digitalWrite(TPUMPR, HIGH);
  //  digitalWrite(MIXERR, HIGH);
  if (pHAvg == true && ECAvg == true){
  static byte trigger = 1;
  dateandtime();
  if (hourupg <= S1Hr && minupg <= S1Min && Resetfun == false) {

    digitalWrite(LEDGREEN, HIGH);
    digitalWrite(LEDBLUE, HIGH);
    float pHAutoSetValue =  pHs1;
    float pHHys =  phSetHysteris;
    float ECAutoSetValue =  ECs1;
    float ECHys =  ECSetHysteris;

    SheduleDoser(pHAutoSetValue, pHHys, ECAutoSetValue, ECHys );
    //      Serial.println("PH SETPOINTS");
    //      Serial.println(pHAutoSetValue);
    //      Serial.println("PH HYS  SETPOINTS");
    //      Serial.println(pHHys);
    //      Serial.println("EC SETPOINTS");
    //      Serial.println(ECAutoSetValue);
    //      Serial.println("EC HYS  SETPOINTS");
    //      Serial.println(ECHys);

    Serial.println("Shift 1 Progressing");



  }

  if (hourupg <= S2Hr && minupg <= S2Min && Resetfun == false) {

    digitalWrite(LEDGREEN, HIGH);
    digitalWrite(LEDBLUE, HIGH);
    float pHAutoSetValue =  pHs2;
    float pHHys =  phSetHysteris;
    float ECAutoSetValue =  ECs2;
    float ECHys =  ECSetHysteris;

    SheduleDoser(pHAutoSetValue, pHHys, ECAutoSetValue, ECHys );
    //      Serial.println("PH SHIFT 2 SETPOINTS");
    //      Serial.println(pHAutoSetValue);
    //      Serial.println("PH  HYS  SETPOINTS");
    //      Serial.println(pHHys);
    //      Serial.println("EC SHIFT 2 SETPOINTS");
    //      Serial.println(ECAutoSetValue);
    //      Serial.println("EC HYS  SETPOINTS");
    //      Serial.println(ECHys);

    Serial.println("Shift 2 Progressing");

  }
  if (hourupg <= S3Hr && minupg <= S3Min && Resetfun == false) {
    digitalWrite(LEDGREEN, HIGH);
    digitalWrite(LEDBLUE, HIGH);
    float pHAutoSetValue =  pHs3;
    float pHHys =  phSetHysteris;
    float ECAutoSetValue =  ECs3;
    float ECHys =  ECSetHysteris;

    SheduleDoser(pHAutoSetValue, pHHys, ECAutoSetValue, ECHys );
    //      Serial.println("PH SHIFT 2 SETPOINTS");
    //      Serial.println(pHAutoSetValue);
    //      Serial.println("PH  HYS  SETPOINTS");
    //      Serial.println(pHHys);
    //      Serial.println("EC SHIFT 2 SETPOINTS");
    //      Serial.println(ECAutoSetValue);
    //      Serial.println("EC HYS  SETPOINTS");
    //      Serial.println(ECHys);

    Serial.println("Shift 3 Progressing");
  }

  if (hourupg <= S4Hr && minupg <= S4Min && Resetfun == false) {

    digitalWrite(LEDGREEN, HIGH);
    digitalWrite(LEDBLUE, HIGH);
    float pHAutoSetValue =  pHs4;
    float pHHys =  phSetHysteris;
    float ECAutoSetValue =  ECs4;
    float ECHys =  ECSetHysteris;

    SheduleDoser(pHAutoSetValue, pHHys, ECAutoSetValue, ECHys );
    //      Serial.println("PH SHIFT 2 SETPOINTS");
    //      Serial.println(pHAutoSetValue);
    //      Serial.println("PH  HYS  SETPOINTS");
    //      Serial.println(pHHys);
    //      Serial.println("EC SHIFT 2 SETPOINTS");
    //      Serial.println(ECAutoSetValue);
    //      Serial.println("EC HYS  SETPOINTS");
    //      Serial.println(ECHys);

    Serial.println("Shift 4 Progressing");
  }
  if (hourupg == S5Hr && minupg == S5Min && Resetfun == false) {

    digitalWrite(LEDGREEN, HIGH);
    digitalWrite(LEDBLUE, HIGH);
    float pHAutoSetValue =  pHs5;
    float pHHys =  phSetHysteris;
    float ECAutoSetValue =  ECs5;
    float ECHys =  ECSetHysteris;

    SheduleDoser(pHAutoSetValue, pHHys, ECAutoSetValue, ECHys );
    //      Serial.println("PH SHIFT 5 SETPOINTS");
    //      Serial.println(pHAutoSetValue);
    //      Serial.println("PH  HYS  SETPOINTS");
    //      Serial.println(pHHys);
    //      Serial.println("EC SHIFT 5 SETPOINTS");
    //      Serial.println(ECAutoSetValue);
    //      Serial.println("EC HYS  SETPOINTS");
    //      Serial.println(ECHys);

    Serial.println("Shift 5 Progressing");
  }

  if (hourupg == S6Hr && minupg == S6Min && Resetfun == false) {

    digitalWrite(LEDGREEN, HIGH);
    digitalWrite(LEDBLUE, HIGH);
    float pHAutoSetValue =  pHs6;
    float pHHys =  phSetHysteris;
    float ECAutoSetValue =  ECs6;
    float ECHys =  ECSetHysteris;

    SheduleDoser(pHAutoSetValue, pHHys, ECAutoSetValue, ECHys );
    //      Serial.println("PH SHIFT 6 SETPOINTS");
    //      Serial.println(pHAutoSetValue);
    //      Serial.println("PH  HYS  SETPOINTS");
    //      Serial.println(pHHys);
    //      Serial.println("EC SHIFT 6 SETPOINTS");
    //      Serial.println(ECAutoSetValue);
    //      Serial.println("EC HYS  SETPOINTS");
    //      Serial.println(ECHys);

    Serial.println("Shift 6 Progressing");
  }
  if (hourupg == S7Hr && minupg == S7Min && Resetfun == false) {

    digitalWrite(LEDGREEN, HIGH);
    digitalWrite(LEDBLUE, HIGH);
    float pHAutoSetValue =  pHs7;
    float pHHys =  phSetHysteris;
    float ECAutoSetValue =  ECs7;
    float ECHys =  ECSetHysteris;

    SheduleDoser(pHAutoSetValue, pHHys, ECAutoSetValue, ECHys );
    //      Serial.println("PH SHIFT 7 SETPOINTS");
    //      Serial.println(pHAutoSetValue);
    //      Serial.println("PH  HYS  SETPOINTS");
    //      Serial.println(pHHys);
    //      Serial.println("EC SHIFT 7 SETPOINTS");
    //      Serial.println(ECAutoSetValue);
    //      Serial.println("EC HYS  SETPOINTS");
    //      Serial.println(ECHys);

    Serial.println("Shift 7 Progressing");
  }
  if (hourupg == S8Hr && minupg == S8Min && Resetfun == false) {

    digitalWrite(LEDGREEN, HIGH);
    digitalWrite(LEDBLUE, HIGH);
    float pHAutoSetValue =  pHs8;
    float pHHys =  phSetHysteris;
    float ECAutoSetValue =  ECs8;
    float ECHys =  ECSetHysteris;

    SheduleDoser(pHAutoSetValue, pHHys, ECAutoSetValue, ECHys );
    //      Serial.println("PH SHIFT 8 SETPOINTS");
    //      Serial.println(pHAutoSetValue);
    //      Serial.println("PH  HYS  SETPOINTS");
    //      Serial.println(pHHys);
    //      Serial.println("EC SHIFT 8 SETPOINTS");
    //      Serial.println(ECAutoSetValue);
    //      Serial.println("EC HYS  SETPOINTS");
    //      Serial.println(ECHys);

    Serial.println("Shift 8 Progressing");
  }
}
}




//*************************************************************************************************
//!******************************************************************************
//! Name: dateandtime()                                  *
//! Description:                      *
//! Param : DATE /TIME                                      *
//! Returns: void                                     *
//! Example: TIME              *
//!******************************************************************************

boolean Reset = false;
void dateandtime() {
  DateTime now = rtc.now();
  hourupg = now.hour();
  minupg = now.minute();
  secslive = now.second();
  Datelive = now.day();
  Monthlive = now.month();
  Yearlive = now.year();

}


float roundDecimalPoint( float in_value, int decimal_place )
{
  float multiplier = powf( 10.0f, decimal_place );
  in_value = roundf( in_value * multiplier ) / multiplier;
  return in_value;
}

double avergearrayec(int * arr, int number) {
  int i;
  int max,
      min;
  double avg;
  long amount = 0;
  if (number <= 0) {
    Serial.println("Error number for the array to avraging!/n");
    return 0;
  }
  if (number < 5) { //less than 5, calculated directly statistics
    for (i = 0; i < number; i++) {
      amount += arr[i];
    }
    avg = amount / number;
    return avg;
  } else {
    if (arr[0] < arr[1]) {
      min = arr[0];
      max = arr[1];
    } else {
      min = arr[1];
      max = arr[0];
    }
    for (i = 2; i < number; i++) {
      if (arr[i] < min) {
        amount += min; //arr<min
        min = arr[i];
      } else {
        if (arr[i] > max) {
          amount += max; //arr>max
          max = arr[i];
        } else {
          amount += arr[i]; //min<=arr<=max
        }
      } //if
    } //for
    avg = (double)amount / (number - 2);
  } //if
  return avg;
}
double avergearray(int * arr, int number) {
  int i;
  int max,
      min;
  double avg;
  long amount = 0;
  if (number <= 0) {
    Serial.println("Error number for the array to avraging!/n");
    return 0;
  }
  if (number < 5) { //less than 5, calculated directly statistics
    for (i = 0; i < number; i++) {
      amount += arr[i];
    }
    avg = amount / number;
    return avg;
  } else {
    if (arr[0] < arr[1]) {
      min = arr[0];
      max = arr[1];
    } else {
      min = arr[1];
      max = arr[0];
    }
    for (i = 2; i < number; i++) {
      if (arr[i] < min) {
        amount += min; //arr<min
        min = arr[i];
      } else {
        if (arr[i] > max) {
          amount += max; //arr>max
          max = arr[i];
        } else {
          amount += arr[i]; //min<=arr<=max
        }
      } //if
    } //for
    avg = (double)amount / (number - 2);
  } //if
  return avg;
}
//!******************************************************************************
//! Name: regulateNutrient()                                  *
//! Description:                      *
//! Param : void                                      *
//! Returns: void                                     *
//! Example: regulateNutrient(float temperature);              *
//!******************************************************************************

void regulateNutrient(float temperature)
{

  // Serial.println("------------------------");
  // Serial.println("Tank Status");


  //Regulator Control - Regulator in socket 5
  if (temperature < 25 && temperature > 0) { //>0 to avoid -999.00*C error
    //    digitalWrite(HEATERR, HIGH);

    //    Serial.println("Heaating...");
  }

  if (temperature > 26 || temperature < 0) { //<0 to avoid -999.00*C error
    //    digitalWrite(HEATERR, LOW);

    //    Serial.println("Heater standby");
  }

  // //Cooling Fan Control - Fan in socket 4
  if (temperature > 27) {
    //    digitalWrite(COOLER_OPIN, HIGH);

    //    Serial.println("Cooling...");
  }

  if (temperature < 26) {
    //    digitalWrite(COOLER_OPIN, LOW);

    //    Serial.println("Cooler standby");
  }

  // Alarm if temperature is too high
  if (temperature > 30) {
    digitalWrite (BUZZER, HIGH);
    delay(100);
    digitalWrite (BUZZER, LOW);
    delay(100);
    digitalWrite (BUZZER, HIGH);
    delay(100);
    digitalWrite (BUZZER  , LOW);
    delay(100);
  }

}


void sensorjson() {
  //  Serial.print("Json");
  DynamicJsonDocument doc(150);
   EC = roundDecimalPoint(ecValue, 2);
   pH = roundDecimalPoint(phValueavrg, 2);
  doc["CID"] = "ZEDC8266";
  doc["Date/Time:"] = t;


  JsonObject Sensor = doc.createNestedObject("Sensor");
  Sensor["pH"] =  (pH);
  Sensor["EC"] = (EC);
  Sensor["temp C"] =( nutrientTemperature);
  Sensor["DO"] = (DOvalue);
  Sensor["pHASP"] =( pHsp);
  Sensor["ECASP"] = ( ECsp);
  Sensor["PHHYS"] = ( phSetHysteris);
  Sensor["ECHYS"] = (ECSetHysteris);

  serializeJsonPretty(doc, Serial3);
  serializeJsonPretty(doc, Serial);


}


//void setpointjson() {
//  DynamicJsonDocument setpointdoc(300);
//
//  setpointdoc["CI0D"] = "ZEDC8266";
//  setpointdoc["Date/Time:"] = t;
//  JsonObject setpoint = setpointdoc.createNestedObject("Setpoint");
//  //SLOT 1
//  setpoint["AUTOMATIC SETPOINT"] = int( "AUTOMATIC");
//  setpoint["pH"] = float( phSetHysteris);
//  setpoint["EC"] = float( );
//  setpoint["pH"] = float( pHsp);
//  setpoint["EC"] = float( ECsp);
//  //setpoint["temp C"] = float( tempsp);
//  setpoint["DO"] = float(dosp);
//
//  //speed
//  setpoint["DOSING SPEED"] = float(Speed);
//  //Automatic setpoint
//  setpoint["DEF SETPOINT"] = int( "DEF AUTOMATIC");
//  setpoint["pH"] = float( pHsp);
//  setpoint["EC"] = float( ECsp);
//  //setpoint["temp C"] = float( nutrientTemperature);
//  setpoint["DO"] = float(dosp);
//  //SLOT 1
//  setpoint["SLOT 1"] = float( 1);
//  setpoint["pH"] = float( pHs1);
//  setpoint["EC"] = float( ECs1);
//  //setpoint["temp C"] = float( nutrientTemperature);
//  setpoint["DO"] = float(dos1);
//  //SLOT 2
//  setpoint["SLOT 2"] = float( 2);
//  setpoint["pH"] = float( pHs2);
//  setpoint["EC"] = float( ECs2);
//  //setpoint["temp C"] = float( nutrientTemperature);
//  setpoint["DO"] = float(dos2);
//    //SLOT
//  setpoint["SLOT 3"] = float( 3);
//  setpoint["pH"] = float( pHs3);
//  setpoint["EC"] = float( ECs3);
//  //setpoint["temp C"] = float( nutrientTemperature);
//  setpoint["DO"] = float(dos3);
//  setpoint["SLOT 4"] = float( 4);
//  setpoint["pH"] = float( pHs4);
//  setpoint["EC"] = float( ECs4);
//  //setpoint["temp C"] = float( nutrientTemperature);
//  setpoint["DO"] = float(dos4);
//   setpoint["SLOT 5"] = float( 5);
//  setpoint["pH"] = float( pHs5);
//  setpoint["EC"] = float( ECs5);
//  //setpoint["temp C"] = float( nutrientTemperature);
//  setpoint["DO"] = float(dos5);
//
//  serializeJsonPretty(setpointdoc, Serial3);
//  serializeJsonPretty(setpointdoc, Serial);
//  delay(1000);
//}

void autotask(){
  if (autostart == true) {
      t8.enable(); 
    }
    else if(autostart == false)
    { t8.disable();}
  
  }

void shedtask(){
  if (ScheduleAuto == true) {
      t9.enable();
      Serial.println("Sheduler dose ON"); 
    }
    else if(ScheduleAuto == false)
    { t9.disable();}
  
  }
    
void setup() {

  #if defined(_DEBUG_) || defined(_TEST_)
  Serial.begin(115200);
  delay(2000);
#endif

 
  Serial3.begin(115200);
  while (!Serial);
  Serial.println("ZEEPTOGREENS...........");
  Serial.flush();
  rtc.begin();
  sensors.begin(); // DALLAS
  ph.begin();
  ec.begin();
//  r.init(); 
   r.startNow();
  //  wdt_disable();
  //  wdt_enable(WDTO_8S);

  //TIME UPDATE

  rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  //  setTime(now.hour(), now.minute(), now.second(), now.day(), now.month(),now.year());

#if defined(USE_HWSPI)
  SPI.begin();
  u8g2.begin();
#elif defined(USE_HWI2C)
  Wire.begin();
  u8g2.begin();
#else
#error "please choose your interface (I2c,SPI)"
#endif
  u8g2.setFont(fontName);
  nav.showTitle = true;
  // u8g2.setBitmapMode(0);


  // disable option
  //  mainMenu[0].enabled=disabledStatus;
  SetPoint[0].enabled = disabledStatus;
  SetPoint[1].enabled = disabledStatus;
  //  SetPoint[2].enabled = disabledStatus;
  //  nav.idleTask=idle;//point a function to be used when menu is suspended
  nav.idleTask = home;
  Serial.println("setup done."); Serial.flush();
  //u8g2.drawStr(0, 0, "WELCOME TO ZEEPTOGREENS");

  u8g2.drawStr(10, 10, "BOOTING.....");
  Timer1.initialize(1000);
  //  Timer1.attachInterrupt(timerIsr);

  //  pinMode(encBtn, INPUT_PULLUP);

  // Stepper configer
  pinMode(PHLOW_STEP_PIN  , OUTPUT);
  pinMode(PHLOW_DIR_PIN    , OUTPUT);
  //  pinMode(PHLOW_ENABLE_PIN    , OUTPUT);

  pinMode(PHHIGH_STEP_PIN  , OUTPUT);
  pinMode(PHHIGH_DIR_PIN    , OUTPUT);

  pinMode(EC_A_STEP_PIN, OUTPUT);
  pinMode(EC_B_STEP_PIN, OUTPUT);
  pinMode(EC_C_STEP_PIN, OUTPUT);

  pinMode(EC_A_DIR_PIN, OUTPUT);
  pinMode(EC_B_DIR_PIN, OUTPUT);
  pinMode(EC_C_DIR_PIN, OUTPUT);

  //  pinMode(EC_A_ENABLE_PIN, OUTPUT);
  //  pinMode(EC_B_ENABLE_PIN, OUTPUT);
  //  pinMode(EC_C_ENABLE_PIN, OUTPUT);

  digitalWrite(EC_A_DIR_PIN, LOW);
  digitalWrite(EC_B_DIR_PIN, LOW);

  digitalWrite(EC_C_DIR_PIN, LOW);

  //  digitalWrite(EC_A_ENABLE_PIN, LOW);
  //  digitalWrite(EC_B_ENABLE_PIN, LOW);
  //  digitalWrite(EC_C_ENABLE_PIN, LOW);


  pinMode(BUZZER, OUTPUT);

  //***Indicator
  pinMode(LEDRED, OUTPUT);
  pinMode(LEDGREEN, OUTPUT);
  pinMode(LEDBLUE, OUTPUT);
  pinMode(LEDPOWER, OUTPUT);
  digitalWrite(LEDPOWER, HIGH);
  pinMode(TPUMPR, OUTPUT);
  pinMode(HEATERR, OUTPUT);
  pinMode(MIXERR, OUTPUT);
  pinMode(RORELAYR, OUTPUT);

  // Enable  Read time Task
  t0.enable(); //Time

  t4.enable();//jsondata send to Esp8266
  
//t8.enable();
//    if (autostart == true) {
//      t8.enable(); //PH Reading
//    }
//    else if(autostart == false)
//    { t8.disable();}
  

  
  //Sensor Reading
    readalldata();
//  if (probePh == false) {
//        t1.enable(); //PH Reading
//  }
//
//  if (probeEc == false) {
//    t2.enable();  // Ec Reading
//  }

  if (probeDo == false) {
    getNutrientDO();
  }
  if (probeTemp == false) {

    t3.enable(); //Temperature
  }
t11.enable();
  if(Autosave==HIGH){
    t11.enable();//t4.disable();
    }
    else{
      t11.disable();
      }
  if (SDCARD == true) {
    Serial.print(F("  Store data to SD Card  "));
            t5.enable(); //SD CARD
  }
  t5.enable(); //SD CARD
  //****SDCard Funtion Code**---
   pinMode(SD_DETECT_PIN, OUTPUT);
  // SD Card Initialization
  if (SD.begin(SDSS))
    //  filePickMenu.begin();//need this after sd begin
    //  Serial.println("initialization done.");
    //  nav.useAccel=false;



    Serial.print(F("  Date  "));
  Serial.print(F("      "));
  Serial.print(F("   Time  "));
  Serial.print("     ");
  Serial.print(F("   Temperature  "));
  Serial.print("     ");
  Serial.print(F("   PH  "));
  Serial.print("     ");
  Serial.print(F("   EC  "));
  Serial.print("     ");
    Serial.print("phRefV");
      Serial.print("     ");
    Serial.print("EcRefV");

  sdcard_file = SD.open("data.txt", FILE_WRITE);
  if (sdcard_file) {
    sdcard_file.print(F("Date  "));
    sdcard_file.print("      ");
    sdcard_file.print("   Time  ");
    sdcard_file.print("     ");
    sdcard_file.print("   Temperature   ");
    sdcard_file.print("     ");
    sdcard_file.print("   PH   ");
    sdcard_file.print("     ");
    sdcard_file.print("   EC   ");
    sdcard_file.print("     ");
  sdcard_file.print("phRefV");
  sdcard_file.print("     ");
  sdcard_file.print("EcRefV");

    
    sdcard_file.close(); // close the file
  }
  // if the file didn't open, print an error:
  else {
    Serial.println("error opening test.txt");
  }

  u8g2.firstPage();
  do {
    nav.out[0].setCursor(0, 2);
    //nav.out[0].print(F("DOSING CONTROLLER"));
    nav.out[0].setCursor(0, 3);
    nav.out[0].print(F("SYSTEM BOOTING ..."));

  } while (u8g2.nextPage());
  delay(1000);
}


void AutoFun(){
  
    //Serial.print("AUTO DOSER PROGRESSING...");
    //    digitalWrite(TPUMPR, HIGH);
   if (pHAvg == true && ECAvg == true){
    float pHAutoSP =  pHsp;
    float pHHys =  phSetHysteris;
          Serial.println(pHAutoSP);
          Serial.println(pHHys);
    float ECAutoSP =  ECsp;
    float ECHys =  ECSetHysteris;
          Serial.println(ECAutoSP);
          Serial.println(ECHys);
    DoserAuto(pHAutoSP, pHHys, ECAutoSP, ECHys);
   }
  
  }
void loop() {
  //  testfun();
  // getNutrientPHavg();
  //    wdt_reset();// make sure this gets called at least once every 8 seconds!
  DateTime now = rtc.now();
  rtc.begin();

  sprintf(t, "%02d:%02d:%02d %02d/%02d/%02d",  now.hour(), now.minute(), now.second(), now.day(), now.month(), now.year());
  //  Serial.print(F("Date/Time: "));
  //  Serial.println(t);
  ///Switching funtion
  //    sensorjson();
autotask();
shedtask();
  //setpointjson();

  digitalWrite(LEDPIN, ledCtrl);
  digitalWrite (BUZZER, ledCtrl);
  digitalWrite (LEDRED, ledRED);
  digitalWrite (LEDGREEN, ledGREEN);
  digitalWrite (LEDBLUE, ledBLUE);
  digitalWrite (TPUMPR, TPRelay);
  digitalWrite (HEATERR, HRelay);
  digitalWrite (MIXERR, MRelay);
  digitalWrite (RORELAYR, RRelay);
  //getSDdata();
  r.execute();
  nav.doInput();
  //nav.poll();
  if (nav.changed(0)) {//only draw if menu changed for gfx device
    //because this code clears the screen, if always called then screen will blink
    u8g2.firstPage();
    do nav.doOutput(); while (u8g2.nextPage());
  }
  delay(100);//simulate other tasks delay
}
